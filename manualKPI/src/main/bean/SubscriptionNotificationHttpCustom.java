package main.bean;



import java.util.HashMap;
import java.util.Map;


/* EXAMPLE OF SUBSCRIPTION - httpCustom

		"httpCustom": {
			"url": "http://217.172.12.224/dme/orion/device/delete",
			"headers": {
				"contextService": "cedus",
				"contextServicePath": "/trento/parking",
				"typeOut": "ParkingSpot"
			}
		}
	
*/

public class SubscriptionNotificationHttpCustom extends SubscriptionNotificationHttp {

	private Map<String, String> headers;

	public Map<String, String> getHeaders() {
		return headers;
	}

	public void setHeaders(Map<String, String> headers) {
		this.headers = headers;
	}
	
	public SubscriptionNotificationHttpCustom(String url, Map<String, String> headers) {
		super(url);
		this.headers = headers;
	}
	
	public SubscriptionNotificationHttpCustom() {
		super("");
		headers = new HashMap<String, String>();
	}
	
	public void putHeader(String key, String value){
		this.headers.put(key, value);
	}
	
}



