package main.bean;



/* EXAMPLE OF SUBSCRIPTION

{
	"id": "595cf6f84f2dfeaa4f6bb171",
	"description": "onDelete Device Subscription",
	"status": "active",
	"subject": {
		"entities": [{
			"idPattern": ".*", // or id 
			"type": "Device"
		}],
		"condition": {
			"attrs": [
				"opType"
			],
			"expression": {
				"q": "opType==deleted"
			}
		}
	},
	"notification": {
		"timesSent": 2,
		"lastNotification": "2017-07-06T14:15:29.00Z",
		"attrs": [
			"status",
			"location",
			"dateCreated",
			"dateModified"
		],
		"attrsFormat": "normalized",
		"httpCustom": {
			"url": "http://217.172.12.224/dme/orion/device/delete",
			"headers": {
				"contextService": "cedus",
				"contextServicePath": "/trento/parking",
				"typeOut": "ParkingSpot"
			}
		}
	}
}

	
*/


public class SubcriptionSubjectEntity {
	
	String idPattern = ""; 
	String id = "";
	String type = "";	
	
	public String getIdPattern() {
		return idPattern;
	}
	public void setIdPattern(String idPattern) {
		this.idPattern = idPattern;
		this.id = null;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
		this.idPattern = null;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	
	
	
	
	public SubcriptionSubjectEntity() {
		idPattern = "";
		id = "";
		type = "";
	}
	
	

}


