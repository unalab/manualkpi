package main.bean;



/* EXAMPLE OF SUBSCRIPTION - http

		"http": {
			"url": "http://217.172.12.224/dme/orion/device/delete",
		}
*/

public class SubscriptionNotificationHttp {

	private String url;
	
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public SubscriptionNotificationHttp(String url) {
		this.url = url;
	}
	
	public SubscriptionNotificationHttp() {
		url = "http://localhost:8080/cpm";
	}
	
}



