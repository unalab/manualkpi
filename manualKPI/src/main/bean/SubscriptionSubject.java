package main.bean;



import java.util.HashSet;
import java.util.Set;

/* EXAMPLE OF SUBSCRIPTION Subject
	"subject": {
		"entities": [{
			"idPattern": ".*",
			"type": "Device"
		}],
		"condition": {
			"attrs": [
				"opType"
			],
			"expression": {
				"q": "opType==deleted"
			}
		}
	}
*/

public class SubscriptionSubject {

	private Set<SubcriptionSubjectEntity> entities;
	private SubscriptionSubjectCondition condition;

	public Set<SubcriptionSubjectEntity> getEntities() {
		return entities;
	}

	public void setEntities(Set<SubcriptionSubjectEntity> entities) {
		this.entities = entities;
	}

	public SubscriptionSubjectCondition getCondition() {
		return condition;
	}

	public void setCondition(SubscriptionSubjectCondition condition) {
		this.condition = condition;
	}

	public SubscriptionSubject(Set<SubcriptionSubjectEntity> entities,
			SubscriptionSubjectCondition condition) {
		this.entities = entities;
		this.condition = condition;
	}

	public SubscriptionSubject() {
		entities = new HashSet<SubcriptionSubjectEntity>();
		condition = new SubscriptionSubjectCondition();
	}

}
