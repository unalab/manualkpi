package main.bean;



import java.util.HashSet;

/* EXAMPLE OF SUBSCRIPTION
	"notification": {
		"timesSent": 2,
		"lastNotification": "2017-07-06T14:15:29.00Z",
		"attrs": [
			"status",
			"location",
			"dateCreated",
			"dateModified"
		],
		"attrsFormat": "normalized",
		"http": {
			"url": "http://217.172.12.224/dme/orion/device/delete"
		}
	}
	
*/


public class SubscriptionNotification {

	int timesSent;
	String lastNotification;
	HashSet<String> attrs;
	String attrsFormat;
	SubscriptionNotificationHttpCustom httpCustom;
	SubscriptionNotificationHttp http;
	
	
	public int getTimesSent() {
		return timesSent;
	}

	public void setTimesSent(int timesSent) {
		this.timesSent = timesSent;
	}

	public String getLastNotification() {
		return lastNotification;
	}

	public void setLastNotification(String lastNotification) {
		this.lastNotification = lastNotification;
	}

	
	public String getAttrsFormat() {
		return attrsFormat;
	}

	public void setAttrsFormat(String attrsFormat) {
		this.attrsFormat = attrsFormat;
	}

	
	public HashSet<String> getAttrs() {
		return attrs;
	}

	public void setAttrs(HashSet<String> attrs) {
		this.attrs = attrs;
	}

	public SubscriptionNotificationHttpCustom getHttpCustom() {
		return httpCustom;
	}

	public void setHttpCustom(SubscriptionNotificationHttpCustom httpCustom) {
		this.httpCustom = httpCustom;
		this.http = null;
	}
	
	public SubscriptionNotificationHttp getHttp() {
		return http;
	}

	public void setHttp(SubscriptionNotificationHttp http) {
		this.http = http;
		this.httpCustom = null;
	}
		
	public SubscriptionNotification(int timesSent, String lastNotification, HashSet<String> attrs, String attrsFormat, SubscriptionNotificationHttpCustom httpCustom, SubscriptionNotificationHttp http) {
		this.timesSent = timesSent;
		this.lastNotification = lastNotification;
		this.attrs = attrs;
		this.attrsFormat = attrsFormat;
		this.httpCustom = httpCustom;
		this.http = http;
		
	}
	
	
	public SubscriptionNotification() {
		timesSent = 2;
		lastNotification = "";
		attrsFormat = "";
		attrs = new HashSet<String>();
		httpCustom = new SubscriptionNotificationHttpCustom();
		http = new SubscriptionNotificationHttp();
		
	}
	
		
	
}

