package main.bean;


/*
 * "condition": {
			"attrs": [
				"opType"
			],
			"expression": {
				"q": "opType==deleted"
			}
		}
		
 */
public class SubscriptionSubjectConditionExpression {

	String q = null;
	String mq = null;
	String geometry = null;
	String coords = null;
	String georel = null;
	
	
	public String getQ() {
		return q;
	}

	public void setQ(String q) {
		this.q = q;
		this.mq = null;
		this.geometry = null;
		this.coords = null;
		this.georel = null;
	}

	public String getMq() {
		return mq;
	}

	public void setMq(String mq) {
		this.q = null;
		this.mq = mq;
		this.geometry = null;
		this.coords = null;
		this.georel = null;
	}

	public String getGeometry() {
		return geometry;
	}

	public void setGeometry(String geometry) {
		this.q = null;
		this.mq = null;
		this.geometry = geometry;
		this.coords = null;
		this.georel = null;
	}

	public String getCoords() {
		return coords;
	}

	public void setCoords(String coords) {
		this.q = null;
		this.mq = null;
		this.geometry = null;
		this.coords = coords;
		this.georel = null;
		
	}

	public String getGeorel() {
		return georel;
	}

	public void setGeorel(String georel) {
		this.q = null;
		this.mq = null;
		this.geometry = null;
		this.coords = null;
		this.georel = georel;
	}


	public SubscriptionSubjectConditionExpression(String q) {
		this.q = q;
	}

	public SubscriptionSubjectConditionExpression() {
		q=null;
		mq = null;
		geometry = null;
		coords = null;
		georel = null;
	}
	
}
