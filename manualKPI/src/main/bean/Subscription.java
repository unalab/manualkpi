package main.bean;



import java.util.HashSet;
import java.util.Set;

/* EXAMPLE OF SUBSCRIPTION
{
	"description": "Subscription",
	"subject": {
		"entities": [{
			"idPattern": ".*",
			"type": "air"
		}],
		"condition": {
			"attrs": ["CO2"]
		}
	},
	"notification": {
		"http": {
			"url": "http://192.168.150.15:5050/notify"
		},
		"attrsFormat": "legacy",
		"attrs": [
			"CO2",
			"status",
			"location"
		]
	}
}
	
*/

/* Subscription Structure
{
	"id": "595cf6f84f2dfeaa4f6bb171",
	"description": "onDelete Device Subscription",
	"status": "active",
	"subject": {
		"entities": [{
			"idPattern": ".*",
			"type": "Device"
		}],
		"condition": {
			"attrs": [
				"opType"
			],
			"expression": {
				"q": "opType==deleted"
			}
		}
	},
	"notification": {
		"timesSent": 2,
		"lastNotification": "2017-07-06T14:15:29.00Z",
		"attrs": [
			"status",
			"location",
			"dateCreated",
			"dateModified"
		],
		"attrsFormat": "normalized",
		"httpCustom": {
			"url": "http://217.172.12.224/dme/orion/device/delete",
			"headers": {
				"contextService": "cedus",
				"contextServicePath": "/trento/parking",
				"typeOut": "ParkingSpot"
			}
		}
	}
}
 * */

public class Subscription {

	private String id = "";
	private String description = "";
	private String status = "";
	private String expires = "";
	private int throttling ;

	private SubscriptionSubject subject;
	private SubscriptionNotification notification;
	private String attrsFormat;
	

	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getExpires() {
		return expires;
	}

	public void setExpires(String expires) {
		this.expires = expires;
	}

	public int getThrottling() {
		return throttling;
	}

	public void setThrottling(int throttling) {
		this.throttling = throttling;
	}

	public SubscriptionSubject getSubject() {
		return subject;
	}

	public void setSubject(SubscriptionSubject subject) {
		this.subject = subject;
	}

	public SubscriptionNotification getNotification() {
		return notification;
	}

	public void setNotification(SubscriptionNotification notification) {
		this.notification = notification;
	}

	public String getAttrsFormat() {
		return attrsFormat;
	}

	public void setAttrsFormat(String attrsFormat) {
		this.attrsFormat = attrsFormat;
	}


	public Subscription() {
		//id = "";
		description = "";
		status = "";
		expires = "";
		//throttling = 5;

		subject = new SubscriptionSubject();
		notification = new SubscriptionNotification();
	}
	
	public Subscription(String id, String description, String status, String expires, int throttling,
			SubscriptionSubject subscriptionSubjectBean, SubscriptionNotification subscriptionNotificationBean) {
		//this.id = id;
		this.description = description;
		this.status = status;
		this.expires = expires;
		this.throttling = throttling;
		this.subject = subscriptionSubjectBean;
		this.notification = subscriptionNotificationBean;
		
	}

}
