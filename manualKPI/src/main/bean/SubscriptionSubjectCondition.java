package main.bean;


import java.util.HashSet;

import org.apache.jasper.tagplugins.jstl.core.Set;

/* EXAMPLE OF SUBSCRIPTION

		"condition": {
			"attrs": [
				"opType"
			],
			"expression": {
				"q": "opType==deleted"
			}
		}

	
*/


public class SubscriptionSubjectCondition {

	private HashSet<String> attrs = new HashSet<String>();
	private SubscriptionSubjectConditionExpression expression = null;
	
	public SubscriptionSubjectConditionExpression getExpression() {
		return expression;
	}

	public void setExpression(SubscriptionSubjectConditionExpression expression) {
		this.expression = expression;
	}

	public HashSet<String> getAttrs() {
		return attrs;
	}

	public void setAttrs(HashSet<String> attrs) {
		this.attrs = attrs;
	}

	
	
	public SubscriptionSubjectCondition() {
		this.attrs = new HashSet<String>();
		this.expression = new SubscriptionSubjectConditionExpression();	
	}

	public SubscriptionSubjectCondition(HashSet<String> attrs, SubscriptionSubjectConditionExpression expression) {
		this.attrs = attrs;
		this.expression = expression;
	}

	
	
}
