package main.tools;

import java.io.IOException;

import javax.servlet.GenericServlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;
 
public class JobScheduler extends GenericServlet {
 
   
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void init(ServletConfig config) throws ServletException {
		 
        super.init(config);


      try {
 
         // specify the job' s details..
         JobDetail job = JobBuilder.newJob(MashupJob.class)
                                   .withIdentity("mashupJob")
                                   .build();
 
         // specify the running period of the job
         Trigger trigger = TriggerBuilder.newTrigger()
                                         .withSchedule(SimpleScheduleBuilder.simpleSchedule()
                                                                            .withIntervalInSeconds(86400)
                                                                            .repeatForever())
                                          .build();
 
         //schedule the job
         SchedulerFactory schFactory = new StdSchedulerFactory();
         Scheduler sch = schFactory.getScheduler();
         sch.start();
         sch.scheduleJob(job, trigger);
 
      } catch (SchedulerException e) {
         e.printStackTrace();
      }
 }

@Override
public void service(ServletRequest arg0, ServletResponse arg1) throws ServletException, IOException {
	// TODO Auto-generated method stub
	
}
 
}