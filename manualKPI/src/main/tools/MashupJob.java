package main.tools;

import org.apache.log4j.Logger;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

 
public class MashupJob implements Job {
 
   private Logger log = Logger.getLogger(MashupJob.class);
 
   public void execute(JobExecutionContext jExeCtx) throws JobExecutionException {
      log.debug("Job run successfully...");
      InvokeMashup mashup;
	try {
		mashup = new InvokeMashup();
	    mashup.invokeDme(1);
	    mashup.invokeDme(2);
	    mashup.invokeDme(3);
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		
	}
      
   }
 
}