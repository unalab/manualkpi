package main.tools;


import java.nio.charset.Charset;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import javax.ws.rs.core.MediaType;

import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.sun.jersey.api.client.ClientResponse;



import main.utils.RestUtils;
import main.tools.ContextBroker;
import main.java.PropertyManager;
import main.java.manualKPIProperties;
import main.bean.CBEntity;
import main.bean.EntityAttribute;



public class Orion extends ContextBroker {
	
	private static final Logger LOGGER = Logger.getLogger(Orion.class.getName() );
	
	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.00'Z'");
	
	private String path_entities = PropertyManager.getProperty(manualKPIProperties.OrionEnt);
	private String path_subscribe = PropertyManager.getProperty(manualKPIProperties.OrionSub);
			
	
	public Orion() throws Exception{
		super(PropertyManager.getProperty(manualKPIProperties.OrionProtocol) + PropertyManager.getProperty(manualKPIProperties.OrionHost));
	}

	public String getPathEntities(){
		return this.path_entities;
	}
	
	public String getPathSubscribe(){
		return this.path_subscribe;
	}
	
	public static String dateFormat(Date date){
		return sdf.format(date);
	}
	
	public static Date dateParse(String date) throws ParseException{
		return sdf.parse(date);
	}
	
	@Override
	public String getEntity(String fiwareservice, String fiwareservicepath, String entityid) {
		
		String servicepath = fiwareservicepath.startsWith("/") ? fiwareservicepath : "/"+fiwareservicepath;
		
		String resp = "";
		String url = getBaseUrl()+this.path_entities + "/" + entityid;
		
		Map<String, String> headers = new HashMap<String, String>();
						headers.put("fiware-service", fiwareservice);
						headers.put("fiware-servicepath", servicepath);
		
		try{ resp = RestUtils.consumeGet(url, headers); }
		catch(Exception e){
			e.printStackTrace();
		}
						
		return resp;
	}

	@Override
	public String getEntities(String fiwareservice, String fiwareservicepath) {
		
		String servicepath = fiwareservicepath.startsWith("/") ? fiwareservicepath : "/"+fiwareservicepath;
		
		String resp = "";
		String url = getBaseUrl()+this.path_entities+"?limit=1000";
		
		Map<String, String> headers = new HashMap<String, String>();
						headers.put("fiware-service", fiwareservice);
						headers.put("fiware-servicepath", servicepath);
		
		try{ resp = RestUtils.consumeGet(url, headers); }
		catch(Exception e){
			e.printStackTrace();
		}
						
		return resp;
	}
	
	public String getEntities(String fiwareservice, String fiwareservicepath, Set<BasicNameValuePair> queryParams) {
		
		BasicNameValuePair limitParam = new BasicNameValuePair("limit", "1000");
		queryParams.add(limitParam);
		String queryString = "?"+  URLEncodedUtils.format(queryParams, Charset.defaultCharset());
		
		String servicepath = fiwareservicepath.startsWith("/") ? fiwareservicepath : "/"+fiwareservicepath;
		
		String resp = "";
		String url = getBaseUrl()+this.path_entities + queryString;
		//LOGGER.log(Level.INFO, "Invoking url: " + url);
		Map<String, String> headers = new HashMap<String, String>();
						headers.put("fiware-service", fiwareservice);
						headers.put("fiware-servicepath", servicepath);
		
		try{ resp = RestUtils.consumeGet(url, headers); }
		catch(Exception e){
			e.printStackTrace();
		}
						
		return resp;
	}
	
	@Override
	public JSONObject postEntities(String fiwareservice, String fiwareservicepath, Set<? extends CBEntity> entities){
		
		JSONObject out = new JSONObject();
		
		for(CBEntity entity : entities){
			boolean esito = postEntity(fiwareservice, fiwareservicepath, entity);
			try {
				out.put(entity.getId(), esito);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return out;
	}
	
	private boolean postEntity(String fiwareservice, String fiwareservicepath, CBEntity entity){
		
		String servicepath = fiwareservicepath.startsWith("/") ? fiwareservicepath : "/"+fiwareservicepath;
		
		String url = getBaseUrl()+this.path_entities;
		Map<String, String> configHeaders = new HashMap<String, String>();
						configHeaders.put("fiware-service", fiwareservice);
						configHeaders.put("fiware-servicepath", servicepath);
		boolean out = true;
		try{ RestUtils.consumePost(url, getGson().toJson(entity), configHeaders); }
		catch(Exception e){
			e.printStackTrace();
			out = false;
		}
		
		return out;
	}
	
	public JSONObject postEntities(String fiwareservice, String fiwareservicepath, JSONArray entities){
		
		JSONObject out = new JSONObject();
		for(int i=0; i< entities.length(); i++){
			JSONObject j = null;
			try {
				j = entities.getJSONObject(i);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			boolean esito = postEntity(fiwareservice, fiwareservicepath, j);
			try {
				out.put(j.getString("id"), esito);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return out;
	}
	
	public boolean postEntity(String fiwareservice, String fiwareservicepath, JSONObject entity){
		
		String servicepath = fiwareservicepath.startsWith("/") ? fiwareservicepath : "/"+fiwareservicepath;
		
		String url = getBaseUrl()+this.path_entities;
		Map<String, String> configHeaders = new HashMap<String, String>();
						configHeaders.put("fiware-service", fiwareservice);
						configHeaders.put("fiware-servicepath", servicepath);
		boolean out = true;
		try{ RestUtils.consumePost(url, entity.toString(), configHeaders); }
		catch(Exception e){
			e.printStackTrace();
			out = false;
		}
		
		return out;
	}
	
	public boolean updateEntityAttribute(String fiwareservice, String fiwareservicepath, String entityid, String attr, Object newval){

		String servicepath = fiwareservicepath.startsWith("/") ? fiwareservicepath : "/"+fiwareservicepath;

		String url = getBaseUrl()+this.path_entities+"/"+entityid+"/attrs";
		Map<String, String> configHeaders = new HashMap<String, String>();
		configHeaders.put("fiware-service", fiwareservice);
		configHeaders.put("fiware-servicepath", servicepath);
		boolean out = true;
		try{
			JSONObject body = new JSONObject();
			
			JSONObject val = new JSONObject();
				val.put("value", newval.toString());
				
			body.put(attr, val);
			
			JSONObject dateModified = new JSONObject();
				dateModified.put("value", Orion.dateFormat(GregorianCalendar.getInstance().getTime()));
				
			body.put("dateModified", dateModified);
			
			MediaType mediatype = MediaType.APPLICATION_JSON_TYPE;

			RestUtils.consumePost(url, body, mediatype, configHeaders); 
		}
		catch(Exception e){
			e.printStackTrace();
			out = false;
		}

		return out;
	}
	
	public boolean updateEntityAttributeMetadata(String fiwareservice, String fiwareservicepath, String entityid, String attr, String jsonSchema){

		String servicepath = fiwareservicepath.startsWith("/") ? fiwareservicepath : "/"+fiwareservicepath;

		String url = getBaseUrl()+this.path_entities+"/"+entityid+"/attrs";
		Map<String, String> configHeaders = new HashMap<String, String>();
		configHeaders.put("fiware-service", fiwareservice);
		configHeaders.put("fiware-servicepath", servicepath);
		boolean out = true;
		try{
			JSONObject entity = new JSONObject();
			JSONObject jsonSchemaObj = new JSONObject();
			JSONObject metavalue = new JSONObject();
			try {
				metavalue = new JSONObject(jsonSchema);
			} catch (JSONException je){
				je.printStackTrace();
			}
			/*
			"jsonschema": {
				"type": "string",
          		"value": "value"
			}
			*/
			jsonSchemaObj.put("type", "object");
			jsonSchemaObj.put("value", metavalue);
			
			JSONObject jsonschema = new JSONObject();
			jsonschema.put("jsonschema", jsonSchemaObj);
			
			JSONObject metadata = new JSONObject();
			metadata.put("metadata", jsonschema);
			entity.put(attr, metadata);
			
			MediaType mediatype = MediaType.APPLICATION_JSON_TYPE;

			RestUtils.consumePost(url, entity, mediatype, configHeaders);
			
		} catch(Exception e){
			e.printStackTrace();
			out = false;
		}

		return out;
	}

	public boolean subscribe(String fiwareservice, String fiwareservicepath, String body) {
		
		String servicepath = fiwareservicepath.startsWith("/") ? fiwareservicepath : "/"+fiwareservicepath;
		
		String url = getBaseUrl() + path_subscribe;
		Map<String, String> headers = new HashMap<String, String>();
			headers.put("fiware-service", fiwareservice);
			headers.put("fiware-servicepath", servicepath);
		boolean out = true;
		try{ 
			//LOGGER.log(Level.INFO, "in: "+body);
			String serviceResponse = RestUtils.consumePost(url, body, headers);
			//LOGGER.log(Level.INFO, "out: "+serviceResponse);
		}
		catch(Exception e){
			e.printStackTrace();
			out = false;
		}
		return out;
	}
	
	public boolean deleteEntity(String fiwareservice, String fiwareservicepath, String entity_id) 
			throws Exception {
		
		String servicepath = fiwareservicepath.startsWith("/") ? fiwareservicepath : "/"+fiwareservicepath;
		
		String url = getBaseUrl() + path_entities + "/" + entity_id;
		
		Map<String, String> headers = new HashMap<String, String>();
							headers.put("fiware-service", fiwareservice);
							headers.put("fiware-servicepath", servicepath);

		ClientResponse cr = RestUtils.consumeDelete(url, headers);
		if(cr.getStatus() > 299)
			return false;
		
		return true;
	}
	
	public String getEntityAttributes(String fiwareservice, String fiwareservicepath, String entityid) {
		
		String servicepath = fiwareservicepath.startsWith("/") ? fiwareservicepath : "/"+fiwareservicepath;
		
		String resp = "";
		String url = getBaseUrl()+this.path_entities + "/" + entityid + "/attrs";
		
		Map<String, String> headers = new HashMap<String, String>();
						headers.put("fiware-service", fiwareservice);
						headers.put("fiware-servicepath", servicepath);
		
		try{ resp = RestUtils.consumeGet(url, headers); }
		catch(Exception e){
			e.printStackTrace();
		}
						
		return resp;
	}
	
	public boolean deleteEntityAttribute(String fiwareservice, String fiwareservicepath, String entityid, String attributename)
			throws Exception {
		
		String servicepath = fiwareservicepath.startsWith("/") ? fiwareservicepath : "/"+fiwareservicepath;
		
		String url = getBaseUrl() + path_entities + "/" + entityid + "/attrs/" + attributename;
		
		Map<String, String> headers = new HashMap<String, String>();
			headers.put("fiware-service", fiwareservice);
			headers.put("fiware-servicepath", servicepath);

			ClientResponse cr = RestUtils.consumeDelete(url, headers);
		
//			LOGGER.log(Level.INFO, "\t" + cr.getStatus());
//			LOGGER.log(Level.INFO, "===================\n");
//		
		return true;
	}
	

}
