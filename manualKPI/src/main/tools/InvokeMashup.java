package main.tools;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import main.java.CalculatedKPIDao;
import main.java.GetKPIList;
import main.java.KPIBean;
import main.java.KPIDao;
import main.java.PropertyManager;
import main.java.manualKPIProperties;
import main.tools.Dme;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import main.utils.GetCommonCategory;
import main.utils.RestUtils;

@WebListener
public class InvokeMashup extends Dme implements ServletContextListener{
	Logger logger = Logger.getLogger(InvokeMashup.class.getName());
	private String path_mashup_eindhoven = PropertyManager.getProperty(manualKPIProperties.DmeMashupEindhoven);
	private String path_mashup_tampere = PropertyManager.getProperty(manualKPIProperties.DmeMashupTampere);
	private String path_mashup_genova = PropertyManager.getProperty(manualKPIProperties.DmeMashupGenova);
	
	public InvokeMashup() throws Exception{
		super(PropertyManager.getProperty(manualKPIProperties.DmeProtocol) + PropertyManager.getProperty(manualKPIProperties.DmeHost));
		
	}

	public String getPathMashupEindhoven(){
		return this.path_mashup_eindhoven;
	}
	
	public String getPathMashupTampere(){
		return this.path_mashup_tampere;
	}
	
	public String getPathMashupGenova(){
		return this.path_mashup_genova;
	}
	
	
	//costruisco un JSONArray per ogni città
	public static JSONArray buildInput(Integer id_city) throws JSONException { 
		JSONObject kpi = null;
		JSONArray json = new JSONArray();
		ArrayList<KPIBean> kpis = KPIDao.getKpi4Mashup();
		JSONArray category = new JSONArray();
		category.put("quantitative");
		category.put("qualitative");
		String cityId = id_city.toString();
		float value = 0;
		Integer valueComputed = 0;
		String city = GetCommonCategory.getCity(cityId);
		Integer categoryCity;
		Integer idKnowage;
		boolean isPresent4City= false;
		
		 for (int i = 0; i<kpis.size(); i++) {
			
			if(kpis.get(i).getIsManual().equalsIgnoreCase("manual")) {
				//prendi l'ultimo valore da kpilist per ciascuna città se c'è
				
				value = KPIDao.getManualValue(kpis.get(i).getIdKPI(), id_city);
				kpi = new JSONObject();
				
					kpi.put("id", kpis.get(i).getIdKPI());
					kpi.put("name", kpis.get(i).getName());
					kpi.put("description", kpis.get(i).getDescription());
					kpi.put("source", "City Performance Monitor");
					kpi.put("organization", "UNaLab");
					kpi.put("calculationFrequency", "daily");
					kpi.put("category", category);
					kpi.put("value", value);
					json.put(kpi);
			}
			else {
				categoryCity = CalculatedKPIDao.getCategoryId(city, "KPI_KPI_CATEGORY");
				idKnowage = KPIDao.getIdKnowageByidKpi(kpis.get(i).getIdKPI());
				isPresent4City = CalculatedKPIDao.isPresent4City(idKnowage, categoryCity);
				if(isPresent4City) {
					//prendi l'ultimo valore dal db di knowage per ciascuna città se c'è
					valueComputed = CalculatedKPIDao.getLastValue(idKnowage);
					value = valueComputed.floatValue();
					kpi = new JSONObject();
					
						kpi.put("id", kpis.get(i).getIdKPI());
						kpi.put("name", kpis.get(i).getName());
						kpi.put("description", kpis.get(i).getDescription());
						kpi.put("source", "City Performance Monitor");
						kpi.put("organization", "31a56233270c46eb8585e2ee41411c89");
						kpi.put("calculationFrequency", "daily");
						kpi.put("category", category);
						kpi.put("value", value);
				        json.put(kpi);
			}
			
			}
		
		}
		
	    
		return json;
	}
	
	
	public boolean invokeDme(Integer id_city) throws JSONException {
		//invoko l'api per il mashup
		String id = id_city.toString();
		String city = GetCommonCategory.getCity(id); 
		city = city.toLowerCase();
		//String path = "getPathMashup" + city.toLowerCase();
		String url; 
		
		switch(city) {
		case "eindhoven":
			url = getBaseUrl() + getPathMashupEindhoven();
			break;
		case "genova":
			url= getBaseUrl() + getPathMashupGenova();
			break;
		case "tampere":
		default:
			url = getBaseUrl() + getPathMashupTampere();
		}
		
		JSONArray input = buildInput(id_city);
		//logger.info("INPUT: " + input);
		boolean out = true;
		try{ RestUtils.consumePost(url, input.toString());
			}
		catch(Exception e){
			e.printStackTrace();
			out = false;
		}
		
		return out;
		
		
		
		
	}

	
	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	
	public void contextInitialized(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
		 
//		 public static void main (String args[]) throws JSONException {
//			 
//			 JSONArray input = buildInput(1);
//			 System.out.println(input);
//		 }

		

}
