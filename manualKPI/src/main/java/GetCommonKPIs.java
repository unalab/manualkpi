package main.java;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import main.utils.GetCommonCategory;

/**
 * Servlet implementation class GetCommonKPIs
 */
@WebServlet("/GetCommonKPIs")
public class GetCommonKPIs extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger(GetCommonKPIs.class.getName());
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetCommonKPIs() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ArrayList<KPIBean> commonKpi = null;
		ArrayList<KPIBean> intermediate = null;
		ArrayList<KPIBean> kpicomputedList = null;
		ArrayList<KPIBean> cityKpi= null;
		ArrayList<KPIBean> results = new ArrayList<KPIBean>();
		ArrayList<Integer> ids = null;
		String city = request.getParameter("city");
		logger.debug("selected pilot: " + city);
		Integer id_city = Integer.parseInt(city);
		commonKpi = KPIDao.getCommon(id_city);
		
		String cityS = GetCommonCategory.getCity(city);
		Integer category_city = CalculatedKPIDao.getCategoryId(cityS.toUpperCase(), "KPI_KPI_CATEGORY");
		intermediate = CalculatedKPIDao.getComputedKPI("task", "all",id_city);
		ids = CalculatedKPIDao.getIds(intermediate, category_city);
		
		kpicomputedList = CalculatedKPIDao.getComputedValues(intermediate, ids);
		
		
		
		cityKpi = KPIDao.getCityKPI("task", "all",id_city);
		
		
		logger.debug("kpilist");
		results.addAll(commonKpi);
		if(kpicomputedList!= null) {
			results.addAll(kpicomputedList);
			}
		if(cityKpi!=null) {
			results.addAll(cityKpi);
		}
		request.setAttribute("kpiList", results);
		String nextJsp= "commonKPI.jsp?city="+id_city;
		logger.debug("nextJSP: " + nextJsp);
		request.getRequestDispatcher(nextJsp).forward(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
