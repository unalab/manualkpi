package main.java;
 
import java.sql.Connection;
import java.sql.DriverManager;

import org.apache.log4j.Logger;


 
public class DBConnectionCalculated {
	
	static Logger logger = Logger.getLogger(DBConnectionCalculated.class.getName());
 
public static Connection createConnection()
{
Connection con = null;

String url = PropertyManager.getProperty(manualKPIProperties.DB_HOST_CALCULATED);
String username = PropertyManager.getProperty(manualKPIProperties.DB_USERNAME);
String password = PropertyManager.getProperty(manualKPIProperties.DB_PASSWORD);
 
//logger.debug("URL DB: " + url);

try
{
	try
	{
		Class.forName("com.mysql.jdbc.Driver");
	}
	catch (ClassNotFoundException e)
	{
		e.printStackTrace();
	}
con = DriverManager.getConnection(url, username, password);
//logger.info("db connection for calculated KPIs");
//System.out.println("Post establishing a DB connection - "+con);
}
catch (Exception e)
{
e.printStackTrace();
}
 
return con;
}
}