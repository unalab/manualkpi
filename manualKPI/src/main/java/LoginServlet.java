package main.java;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import main.bean.*;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger(LoginServlet.class.getName());
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String userName = request.getParameter("username");
		String password = request.getParameter("password");
		LoginBean loginBean = new LoginBean();
		
		String level = "task";
		String category = "all";
		 
		 loginBean.setUserName(userName);
		 loginBean.setPassword(password);
		 
		 //KPIDao loginDao = new KPIDao();
		 
		 ArrayList<KPIBean> kpiInitialList = new ArrayList<KPIBean>();
			ArrayList<KPIBean> kpicomputedList = null;
			ArrayList<KPIBean> intermediate = null;
			ArrayList<KPIBean> results = new ArrayList<KPIBean>();
			ArrayList<KPIBean> cityKpi = null;
			ArrayList<Integer> ids = null;
			String nextJsp = "";
			//logger.debug("id selected pilot: "+ chosenPilot);
			//kpiInitialList = KPIDao.getKPI(level, category, 3);
			//String cityS = GetCommonCategory.getCity(city);
			Integer category_city = 0;
			
			//results.addAll(kpiInitialList);
			
			
		 
		 try
		 {
		 String userValidate = KPIDao.authenticateUser(loginBean);
		 
		 if(userValidate.equals("admin"))
		 {
			 //System.out.println("Admin's Home");
		 
			 HttpSession session = request.getSession(); //Creating a session
			 session.setMaxInactiveInterval(5*60);
			 session.setAttribute("user", userValidate); //setting session attribute
			 //request.setAttribute("userName", userName);
			 kpiInitialList = KPIDao.getKPI(level, category, 3);
			 nextJsp = "admin.jsp?city=3&level="+level+"&category="+category;
			 request.setAttribute("kpiList", kpiInitialList);
			 request.getRequestDispatcher(nextJsp).forward(request, response);
		 }
		 else if(userValidate.equals("eindhoven_city_manager"))
		 {
			 	System.out.println("City Manager");
		 
			 	HttpSession session = request.getSession(); //Creating a session
				 session.setMaxInactiveInterval(5*60);
				 session.setAttribute("user", userValidate);
				 kpiInitialList = KPIDao.getKPI(level, category, 3);
				 results.addAll(kpiInitialList);
				 //request.getRequestDispatcher(nextJsp).forward(request, response);
				 category_city = CalculatedKPIDao.getCategoryId("EINDHOVEN", "KPI_KPI_CATEGORY");
			 	logger.debug("category CITY: " + category_city);
				intermediate = CalculatedKPIDao.getComputedKPI(level, category, 3);
				ids = CalculatedKPIDao.getIds(intermediate, category_city);
				if(ids != null) {
					kpicomputedList = CalculatedKPIDao.getComputedValues(intermediate, ids);
				}
				if(kpicomputedList!= null) {
					results.addAll(kpicomputedList);
					
				}
				cityKpi = KPIDao.getCityKPI(level, category,3);
				if(cityKpi!=null) {
					results.addAll(cityKpi);
				}
				
				nextJsp = "/GetKPIList?city=3&level="+level+"&category="+category;
				
				request.setAttribute("kpiList", results);
				response.sendRedirect(request.getContextPath() + nextJsp);
		 }
		 
		 else if(userValidate.equals("genova_city_manager"))
		 {
			 	System.out.println("City Manager");
		 
			 	HttpSession session = request.getSession(); //Creating a session
				 session.setMaxInactiveInterval(5*60);
				 session.setAttribute("user", userValidate);
				 kpiInitialList = KPIDao.getKPI(level, category, 1);
				 results.addAll(kpiInitialList);
				 //request.getRequestDispatcher(nextJsp).forward(request, response);
				 category_city = CalculatedKPIDao.getCategoryId("GENOVA", "KPI_KPI_CATEGORY");
			 	logger.debug("category CITY: " + category_city);
				intermediate = CalculatedKPIDao.getComputedKPI(level, category,1);
				
				
				ids = CalculatedKPIDao.getIds(intermediate, category_city);
					
				
				
				if(ids != null) {
					kpicomputedList = CalculatedKPIDao.getComputedValues(intermediate, ids);
				}
				
				
				if(kpicomputedList!= null) {
					results.addAll(kpicomputedList);
					
				}
				cityKpi = KPIDao.getCityKPI(level, category,1);
				if(cityKpi!=null) {
					results.addAll(cityKpi);
				}
				//response.sendRedirect(request.getContextPath() + "/index.jsp");
				nextJsp = "/GetKPIList?city=1&level="+level+"&category="+category;
				 
				 request.setAttribute("kpiList", results);
				 response.sendRedirect(request.getContextPath() + nextJsp);
				 //request.getRequestDispatcher(nextJsp).forward(request, response);
		 }
		 
		 else if(userValidate.equals("tampere_city_manager"))
		 {
			 	System.out.println("City Manager");
		 
			 	HttpSession session = request.getSession(); //Creating a session
				 session.setMaxInactiveInterval(5*60);
				 session.setAttribute("user", userValidate);
				 kpiInitialList = KPIDao.getKPI(level, category, 2);
				 results.addAll(kpiInitialList);
				 //request.getRequestDispatcher(nextJsp).forward(request, response);
				 category_city = CalculatedKPIDao.getCategoryId("TAMPERE", "KPI_KPI_CATEGORY");
			 	logger.debug("category CITY: " + category_city);
				intermediate = CalculatedKPIDao.getComputedKPI(level, category, 2);
				ids = CalculatedKPIDao.getIds(intermediate, category_city);
				if(ids != null) {
					kpicomputedList = CalculatedKPIDao.getComputedValues(intermediate, ids);
				}
				if(kpicomputedList!= null) {
					results.addAll(kpicomputedList);
					
				}
				cityKpi = KPIDao.getCityKPI(level, category,2);
				if(cityKpi!=null) {
					results.addAll(cityKpi);
				}
				
				nextJsp = "/GetKPIList?city=2&level="+level+"&category="+category;
				 
				request.setAttribute("kpiList", results);
				response.sendRedirect(request.getContextPath() + nextJsp);
		 }
		 
		 
		 else
		 {
			 System.out.println("Error message = "+userValidate);
			 request.setAttribute("errMessage", userValidate);
			 
			 request.getRequestDispatcher("login.jsp").forward(request, response);
		 }
		 }
		 catch (IOException e1)
		 {
			 e1.printStackTrace();
		 }
		 catch (Exception e2)
		 {
			 e2.printStackTrace();
		 }
	}

}
