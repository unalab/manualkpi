package main.java;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Array;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;

import main.utils.GetCommonCategory;

/**
 * Servlet implementation class GetFilteredIndicators
 */
@WebServlet("/GetFilteredIndicators")
public class GetFilteredIndicators extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger(GetFilteredIndicators.class.getName());
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetFilteredIndicators() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		String filters[];
		String categories = "";
		ArrayList<KPIBean> filteredKPI = new ArrayList<KPIBean>();
		ArrayList<KPIBean> intermediate = null;
		ArrayList<KPIBean> kpicomputedList = null;
		ArrayList<KPIBean> results = new ArrayList<KPIBean>();
		ArrayList<Integer> ids = null;
		String city = request.getParameter("city");
		logger.info("selected pilot: " + city);
		Integer id_city = Integer.parseInt(city);
		categories = request.getParameter("categories");
		logger.info("filters: " + categories);
		categories = categories.replace("[", "");
		categories = categories.replace("]", "");
		categories = categories.replace("\"", "");
		logger.info ("categories senza parentesi: "+ categories);
		filters = categories.split(",");
		
		
//		for (int i = 0; i<filters.length; i++) {
////			
////			categories = categories + filters[i];
//			logger.info("FILTERS: "+ filters[i]);
//		}
//		
		
		String cityS = GetCommonCategory.getCity(city);
		Integer category_city = CalculatedKPIDao.getCategoryId(cityS.toUpperCase(), "KPI_KPI_CATEGORY");		
		filteredKPI = KPIDao.getFilteredCommon(id_city, filters);
		intermediate = CalculatedKPIDao.getFilteredComputed(id_city, filters);
		ids = CalculatedKPIDao.getIds(intermediate, category_city);
		kpicomputedList = CalculatedKPIDao.getComputedValues(intermediate, ids);
		
		
		
		
		//request.setAttribute("kpiList", filteredKPI);
		results.addAll(filteredKPI);
		if(kpicomputedList!= null) {
			results.addAll(kpicomputedList);
			}
		
		String json = new Gson().toJson(results);
		logger.info("json: " + json);
		response.setContentType("application/json");
		out.write(json);
		
//		String nextJsp= "commonKPI.jsp?city="+id_city;
//		logger.debug("nextJSP: " + nextJsp);
//		request.getRequestDispatcher(nextJsp).forward(request,response);
		//System.out.println(filters.get(0));
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
