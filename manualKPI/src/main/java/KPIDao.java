package main.java;


//import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import main.bean.City;
//import main.utils.GetCommonCategory;
import main.bean.LoginBean;



public class KPIDao {
	
	static Logger logger = Logger.getLogger(KPIDao.class.getName());
	private static ArrayList<KPIBean> kpis;
	
	
	
	public static void editKPIValue (Integer idCity, Integer idKPI, float newValue) {
		
		Connection conn = null;
		try {
			conn = DBConnection.createConnection();
			String query="INSERT INTO input(`id_city`,`id_kpi`,`value`) VALUES ("+idCity+","+idKPI+","+newValue+")";
			//logger.info(query);
			Statement st = conn.createStatement();
		      
		      // execute the query, and get a java resultset
		     st.executeUpdate(query);
		     // KPIBean kpi = null;
		      
			
		      
		     //System.out.println("ho creato un nuovo input"); 
		     
		     
		      conn.close();
		      
		}
		catch(Exception e)
		{			
			e.printStackTrace();
			
		}
	}
	
public static void setValue (Integer idCity, Integer idKPI, float newValue, String motivation) {
		
		Connection conn = null;
		try {
			conn = DBConnection.createConnection();
			String query="INSERT INTO input(`id_city`,`id_kpi`,`value`,`motivation`) VALUES ("+idCity+","+idKPI+","+newValue+",'"+motivation+"')";
			//logger.info(query);
			Statement st = conn.createStatement();
		      
		      // execute the query, and get a java resultset
		     st.executeUpdate(query);
		     // KPIBean kpi = null;
		      
			
		      
		     //System.out.println("ho creato un nuovo input"); 
		     
		     
		      conn.close();
		      
		}
		catch(Exception e)
		{			
			e.printStackTrace();
			
		}
	}
	
public static void editCityKPIValue (Integer idCity, Integer idKPI, float newValue) {
		
		Connection conn = null;
		try {
			conn = DBConnection.createConnection();
			String query ="";
			switch(idCity) {
			case 1: 
				query = "INSERT INTO input(`id_city`,`id_kpi_genova`,`value`) VALUES ("+idCity+","+idKPI+","+newValue+")";
				logger.debug(query);
				break;
			case 2: 
				query = "INSERT INTO input(`id_city`,`id_kpi_tampere`,`value`) VALUES ("+idCity+","+idKPI+","+newValue+")";
				logger.debug(query);
				break;
			case 3: 
				query = "INSERT INTO input(`id_city`,`id_kpi_eindhoven`,`value`) VALUES ("+idCity+","+idKPI+","+newValue+")";
				logger.debug(query);
				break;
			}
			
			
			
			//logger.info(query);
			Statement st = conn.createStatement();
		      
		      // execute the query, and get a java resultset
		     st.executeUpdate(query);
		     // KPIBean kpi = null;
		      
			
		      
		     //System.out.println("ho creato un nuovo input"); 
		     
		     
		      conn.close();
		      
		}
		catch(Exception e)
		{			
			e.printStackTrace();
			
		}
	}


public static void setCityKPI (Integer idCity, Integer idKPI, float newValue, String motivation) {
	
	Connection conn = null;
	try {
		conn = DBConnection.createConnection();
		String query ="";
		switch(idCity) {
		case 1: 
			query = "INSERT INTO input(`id_city`,`id_kpi_genova`,`value`,`motivation`) VALUES ("+idCity+","+idKPI+","+newValue+",'"+motivation+"')";
			logger.debug(query);
			break;
		case 2: 
			query = "INSERT INTO input(`id_city`,`id_kpi_tampere`,`value`,`motivation`) VALUES ("+idCity+","+idKPI+","+newValue+",'"+motivation+"')";
			logger.debug(query);
			break;
		case 3: 
			query = "INSERT INTO input(`id_city`,`id_kpi_eindhoven`,`value`,`motivation`) VALUES ("+idCity+","+idKPI+","+newValue+",'"+motivation+"')";
			logger.debug(query);
			break;
		}
		
		
		
		//logger.info(query);
		Statement st = conn.createStatement();
	      
	      // execute the query, and get a java resultset
	     st.executeUpdate(query);
	     // KPIBean kpi = null;
	      
		
	      
	     //System.out.println("ho creato un nuovo input"); 
	     
	     
	      conn.close();
	      
	}
	catch(Exception e)
	{			
		e.printStackTrace();
		
	}
}
		
	
	public static String authenticateUser (LoginBean user) {
		
		
		 String userName = user.getUserName();
		 String password = user.getPassword();
		 
		 Connection con = null;
		 Statement statement = null;
		 ResultSet resultSet = null;
		 
		 String userNameDB = "";
		 String passwordDB = "";
		 String roleDB = "";
		 Integer userId;
		 String pilot = "";
		 
		 try
		 {
		 con = DBConnection.createConnection();
		 statement = con.createStatement();
		 resultSet = statement.executeQuery("select * from users");
		 
		 while(resultSet.next())
		 {
			 userNameDB = resultSet.getString("username");
			 passwordDB = resultSet.getString("password");
			 roleDB = resultSet.getString("role");
			 userId = resultSet.getInt("id");
			 pilot = resultSet.getString("pilot");
			 
			 
		 
			 if(userName.equals(userNameDB) && password.equals(passwordDB) && roleDB.equals("admin") && pilot.equals("all"))
				 return "admin";
			 else if(userName.equals(userNameDB) && password.equals(passwordDB) && roleDB.equals("city_manager") && pilot.equals("eindhoven"))
				 return "eindhoven_city_manager";
			 else if(userName.equals(userNameDB) && password.equals(passwordDB) && roleDB.equals("city_manager") && pilot.equals("genova"))
				 return "genova_city_manager";
			 else if(userName.equals(userNameDB) && password.equals(passwordDB) && roleDB.equals("city_manager") && pilot.equals("tampere"))
				 return "tampere_city_manager";
			 
		 
		 }
		 }
		 catch(SQLException e)
		 {
		 e.printStackTrace();
		 }
		 return "Invalid user credentials";
	}
		
	
	
	public static float getManualValue(Integer id_kpi, Integer id_city)
	{
		Logger logger = Logger.getLogger(KPIDao.class.getName());
		Connection conn = null;
		float value = 0;
		try {
			conn = DBConnection.createConnection();
			String query = "SELECT value, MAX(lastUpdated) FROM kpilist WHERE id_kpi="+id_kpi+" AND id_city="+id_city;
			
			//logger.info(query);
			Statement st = conn.createStatement();
		      
		      // execute the query, and get a java resultset
		      ResultSet rs = st.executeQuery(query);
		     if (rs!=null) {
		    	 	while (rs.next()) {
		    	 		value = rs.getFloat("value");
		    	 	}
		     }
		}
		
		catch(Exception e)
		{			
			e.printStackTrace();
			return 0;
		}

		
		return value;
	}
	
	public static ArrayList<KPIBean> getKPI(String level, String category, Integer id_city) {
		Logger logger = Logger.getLogger(KPIDao.class.getName());
		
		Connection conn = null;
		
		//CallableStatement ps = null;
		//ResultSet res = null;

		ArrayList<KPIBean> kpiList = new ArrayList<KPIBean>();
		
		try {
			conn = DBConnection.createConnection();
			String query;
		if(!category.equalsIgnoreCase("all")) {
			query="SELECT a.lTS, b.*\n" + 
					"FROM (SELECT id_kpi, MAX(lastUpdated) as lTS\n" + 
					"	from kpilist \n" + 
					"	where kpiLevel='"+level+"' AND kpiCategory='"+category+"' AND id_city="+id_city+ " AND isManual='manual'\n" + 
					"	GROUP BY id_kpi) a JOIN kpilist b\n" + 
					"      ON a.id_kpi = b.id_kpi\n" + 
					"WHERE b.kpiLevel='"+level+"' AND b.kpiCategory='" + category+"' AND b.id_city="+id_city+ " AND b.isManual='manual' AND\n" + 
					"      a.lTS = b.lastUpdated;";
			//logger.info(query);
			Statement st = conn.createStatement();
		      
		      // execute the query, and get a java resultset
		      ResultSet rs = st.executeQuery(query);
		      KPIBean kpi = null;
		      
			//ps = (CallableStatement) conn.prepareCall("");
		      if(rs != null) 
			  {
		    	  java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				Timestamp t;
		      while (rs.next())
		      {
		    	  	kpi = new KPIBean();
		    	  	
		    	  	kpi.setName(rs.getString("name"));
		    	  	kpi.setDescription(rs.getString("description"));
		    	  	kpi.setUnit(rs.getString("measureunit"));
		    	  	kpi.setValue(rs.getFloat("value"));
		    	  	t = rs.getTimestamp("lastUpdated");
		    	  	kpi.setDate(sdf.format(t));
		    	  	kpi.setIdCity(id_city);
		    	  	kpi.setKpiLevel(level);
		    	  	kpi.setKpiCategory(category);
		    	  	kpi.setIdKPI(rs.getInt("id_kpi"));
		    	  	kpi.setTableId(rs.getInt("tableid"));
		    	  	kpi.setIsManual("manual");
		    	  	kpi.setMotivation(rs.getString("motivation"));
		    	  	//logger.debug("trovato kpi" + kpi.getName());
		    	  	kpiList.add(kpi);
		      }
		      
		      
			}
	
			
		}
		
		else {
			query="SELECT a.lTS, b.*\n" + 
					"FROM (SELECT id_kpi, MAX(lastUpdated) as lTS\n" + 
					"	from kpilist \n" + 
					"	where kpiLevel='"+level+"' AND id_city="+id_city+ " AND isManual='manual'\n" + 
					"	GROUP BY id_kpi) a JOIN kpilist b\n" + 
					"      ON a.id_kpi = b.id_kpi\n" + 
					"WHERE b.kpiLevel='"+level+"' AND b.id_city="+id_city+ " AND isManual='manual' AND\n" + 
					"      a.lTS = b.lastUpdated;";
			//logger.info(query);
			Statement st = conn.createStatement();
		      
		      // execute the query, and get a java resultset
		      ResultSet rs = st.executeQuery(query);
		      KPIBean kpi = null;
		      
			//ps = (CallableStatement) conn.prepareCall("");
		      if(rs != null) 
			  {
		    	  java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				Timestamp t;
		      while (rs.next())
		      {
		    	  	kpi = new KPIBean();
		    	  	
		    	  	kpi.setName(rs.getString("name"));
		    	  	kpi.setDescription(rs.getString("description"));
		    	  	kpi.setUnit(rs.getString("measureunit"));
		    	  	kpi.setValue(rs.getFloat("value"));
		    	  	t = rs.getTimestamp("lastUpdated");
		    	  	kpi.setDate(sdf.format(t));
		    	  	kpi.setIdCity(id_city);
		    	  	kpi.setKpiLevel(level);
		    	  	kpi.setKpiCategory(rs.getString("kpiCategory"));
		    	  	kpi.setIdKPI(rs.getInt("id_kpi"));
		    	  	kpi.setTableId(rs.getInt("tableid"));
		    	  	kpi.setIsManual("manual");
		    	  	kpi.setMotivation(rs.getString("motivation"));
		    	  	
		    	  	//logger.debug("trovato kpi" + kpi.getName());
		    	  	kpiList.add(kpi);
		      }
		      
		      
			}
			
		}
		      
		     
		      conn.close();
		}
		catch(Exception e)
		{			
			e.printStackTrace();
			return null;
		}

		return kpiList;
		
		}
	
	
	
	public static ArrayList<KPIBean> getCityKPI(String level, String category, Integer id_city) {
		Logger logger = Logger.getLogger(KPIDao.class.getName());
		
		Connection conn = null;
		
		//CallableStatement ps = null;
		//ResultSet res = null;

		ArrayList<KPIBean> kpiList = new ArrayList<KPIBean>();
		
		try {
			conn = DBConnection.createConnection();
			String query;
			switch(id_city) {
			case 1:
				if(!category.equalsIgnoreCase("all")) {
					query="SELECT a.lTS, b.*\n" + 
						"FROM (SELECT id, MAX(lastUpdated) as lTS\n" + 
						"	from genovakpilist \n" + 
						"	where kpiLevel='"+level+"' AND kpiCategory='"+category+"' AND id_city="+id_city+"\n" + 
						"	GROUP BY id) a JOIN genovakpilist b\n" + 
						"      ON a.id = b.id\n" + 
						"WHERE b.kpiLevel='"+level+"' AND b.kpiCategory='" + category+"' AND b.id_city="+id_city+ " AND\n" + 
						"      a.lTS = b.lastUpdated;";
					
					Statement st = conn.createStatement();
				      
				      // execute the query, and get a java resultset
				      ResultSet rs = st.executeQuery(query);
				      KPIBean kpi = null;
				      
					//ps = (CallableStatement) conn.prepareCall("");
				      if(rs != null) 
					  {
				    	  java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						Timestamp t;
				      while (rs.next())
				      {
				    	  	kpi = new KPIBean();
				    	  	
				    	  	kpi.setName(rs.getString("name"));
				    	  	kpi.setDescription(rs.getString("description"));
				    	  	kpi.setUnit(rs.getString("measureunit"));
				    	  	kpi.setValue(rs.getFloat("value"));
				    	  	t = rs.getTimestamp("lastUpdated");
				    	  	kpi.setDate(sdf.format(t));
				    	  	kpi.setIdCity(id_city);
				    	  	kpi.setKpiLevel(level);
				    	  	kpi.setKpiCategory(rs.getString("kpiCategory"));
				    	  	kpi.setIdKPI(rs.getInt("id"));
				    	  	kpi.setTableId(rs.getInt("tableid"));
				    	  	kpi.setIsManual("citymanual");
				    	  	kpi.setMotivation(rs.getString("motivation"));
				    	  	kpi.setHumanCategory(getHCategory(kpi.getKpiCategory()));
				    	  	kpi.setIcon(getMyIcon(kpi.getKpiCategory()));
				    	  	logger.debug("trovata icona" + kpi.getIcon());
				    	  	kpiList.add(kpi);
				      }
				      
				      
					}
			
				}
				
				else {
					query="SELECT a.lTS, b.*\n" + 
							"FROM (SELECT id, MAX(lastUpdated) as lTS\n" + 
							"	from genovakpilist \n" + 
							"	where kpiLevel='"+level+"' AND id_city="+id_city+ " \n" + 
							"	GROUP BY id) a JOIN genovakpilist b\n" + 
							"      ON a.id = b.id\n" + 
							"WHERE b.kpiLevel='"+level+"' AND b.id_city="+id_city+ " AND\n" + 
							"      a.lTS = b.lastUpdated;";
					//logger.info(query);
					Statement st = conn.createStatement();
				      
				      // execute the query, and get a java resultset
				      ResultSet rs = st.executeQuery(query);
				      KPIBean kpi = null;
				      
					//ps = (CallableStatement) conn.prepareCall("");
				      if(rs != null) 
					  {
				    	  java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						Timestamp t;
				      while (rs.next())
				      {
				    	  	kpi = new KPIBean();
				    	  	
				    	  	kpi.setName(rs.getString("name"));
				    	  	kpi.setDescription(rs.getString("description"));
				    	  	kpi.setUnit(rs.getString("measureunit"));
				    	  	kpi.setValue(rs.getFloat("value"));
				    	  	t = rs.getTimestamp("lastUpdated");
				    	  	kpi.setDate(sdf.format(t));
				    	  	kpi.setIdCity(id_city);
				    	  	kpi.setKpiLevel(level);
				    	  	kpi.setKpiCategory(rs.getString("kpiCategory"));
				    	  	kpi.setIdKPI(rs.getInt("id"));
				    	  	kpi.setTableId(rs.getInt("tableid"));
				    	  	kpi.setIsManual("citymanual");
				    	  	kpi.setMotivation(rs.getString("motivation"));
				    	  	kpi.setHumanCategory(getHCategory(kpi.getKpiCategory()));
				    	  	kpi.setIcon(getMyIcon(kpi.getKpiCategory()));
				    	  	logger.debug("trovata icona" + kpi.getIcon());
				    	  	//logger.debug("trovato kpi" + kpi.getName());
				    	  	kpiList.add(kpi);
					
				}
				
			}
		
			
		      
			}
				
			break;
			case 2:
				if(!category.equalsIgnoreCase("all")) {
					query="SELECT a.lTS, b.*\n" + 
						"FROM (SELECT id, MAX(lastUpdated) as lTS\n" + 
						"	from tamperekpilist \n" + 
						"	where kpiLevel='"+level+"' AND kpiCategory='"+category+"' AND id_city="+id_city+"\n" + 
						"	GROUP BY id) a JOIN tamperekpilist b\n" + 
						"      ON a.id = b.id\n" + 
						"WHERE b.kpiLevel='"+level+"' AND b.kpiCategory='" + category+"' AND b.id_city="+id_city+ " AND\n" + 
						"      a.lTS = b.lastUpdated;";
					
					Statement st = conn.createStatement();
				      
				      // execute the query, and get a java resultset
				      ResultSet rs = st.executeQuery(query);
				      KPIBean kpi = null;
				      
					//ps = (CallableStatement) conn.prepareCall("");
				      if(rs != null) 
					  {
				    	  java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						Timestamp t;
				      while (rs.next())
				      {
				    	  	kpi = new KPIBean();
				    	  	
				    	  	kpi.setName(rs.getString("name"));
				    	  	kpi.setDescription(rs.getString("description"));
				    	  	kpi.setUnit(rs.getString("measureunit"));
				    	  	kpi.setValue(rs.getFloat("value"));
				    	  	t = rs.getTimestamp("lastUpdated");
				    	  	kpi.setDate(sdf.format(t));
				    	  	kpi.setIdCity(id_city);
				    	  	kpi.setKpiLevel(level);
				    	  	kpi.setKpiCategory(rs.getString("kpiCategory"));
				    	  	kpi.setIdKPI(rs.getInt("id"));
				    	  	kpi.setTableId(rs.getInt("tableid"));
				    	  	kpi.setIsManual("citymanual");
				    	  	kpi.setMotivation(rs.getString("motivation"));
				    	  	kpi.setHumanCategory(getHCategory(kpi.getKpiCategory()));
				    	  	kpi.setIcon(getMyIcon(kpi.getKpiCategory()));
				    	  	logger.debug("trovata icona" + kpi.getIcon());
				    	  	//logger.debug("trovato kpi" + kpi.getName());
				    	  	kpiList.add(kpi);
				      }
				      
				      
					}
			
				}
				
				else {
					query="SELECT a.lTS, b.*\n" + 
							"FROM (SELECT id, MAX(lastUpdated) as lTS\n" + 
							"	from tamperekpilist \n" + 
							"	where kpiLevel='"+level+"' AND id_city="+id_city+ " \n" + 
							"	GROUP BY id) a JOIN tamperekpilist b\n" + 
							"      ON a.id = b.id\n" + 
							"WHERE b.kpiLevel='"+level+"' AND b.id_city="+id_city+ " AND\n" + 
							"      a.lTS = b.lastUpdated;";
					//logger.info(query);
					Statement st = conn.createStatement();
				      
				      // execute the query, and get a java resultset
				      ResultSet rs = st.executeQuery(query);
				      KPIBean kpi = null;
				      
					//ps = (CallableStatement) conn.prepareCall("");
				      if(rs != null) 
					  {
				    	  java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						Timestamp t;
				      while (rs.next())
				      {
				    	  	kpi = new KPIBean();
				    	  	
				    	  	kpi.setName(rs.getString("name"));
				    	  	kpi.setDescription(rs.getString("description"));
				    	  	kpi.setUnit(rs.getString("measureunit"));
				    	  	kpi.setValue(rs.getFloat("value"));
				    	  	t = rs.getTimestamp("lastUpdated");
				    	  	kpi.setDate(sdf.format(t));
				    	  	kpi.setIdCity(id_city);
				    	  	kpi.setKpiLevel(level);
				    	  	kpi.setKpiCategory(rs.getString("kpiCategory"));
				    	  	kpi.setIdKPI(rs.getInt("id"));
				    	  	kpi.setTableId(rs.getInt("tableid"));
				    	  	kpi.setIsManual("citymanual");
				    	  	kpi.setMotivation(rs.getString("motivation"));
				    	  	kpi.setHumanCategory(getHCategory(kpi.getKpiCategory()));
				    	  	kpi.setIcon(getMyIcon(kpi.getKpiCategory()));
				    	  	logger.debug("trovata icona" + kpi.getIcon());
				    	  	//logger.debug("trovato kpi" + kpi.getName());
				    	  	kpiList.add(kpi);
					
				}
				
			}
		
			
		      
			}
				
			break;
			case 3: 
				if(!category.equalsIgnoreCase("all")) {
					query="SELECT a.lTS, b.*\n" + 
						"FROM (SELECT id, MAX(lastUpdated) as lTS\n" + 
						"	from eindhovenkpilist \n" + 
						"	where kpiLevel='"+level+"' AND kpiCategory='"+category+"' AND id_city="+id_city+"\n" + 
						"	GROUP BY id) a JOIN eindhovenkpilist b\n" + 
						"      ON a.id = b.id\n" + 
						"WHERE b.kpiLevel='"+level+"' AND b.kpiCategory='" + category+"' AND b.id_city="+id_city+ " AND\n" + 
						"      a.lTS = b.lastUpdated;";
					
					Statement st = conn.createStatement();
				      
				      // execute the query, and get a java resultset
				      ResultSet rs = st.executeQuery(query);
				      KPIBean kpi = null;
				      
					//ps = (CallableStatement) conn.prepareCall("");
				      if(rs != null) 
					  {
				    	  java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						Timestamp t;
				      while (rs.next())
				      {
				    	  	kpi = new KPIBean();
				    	  	
				    	  	kpi.setName(rs.getString("name"));
				    	  	kpi.setDescription(rs.getString("description"));
				    	  	kpi.setUnit(rs.getString("measureunit"));
				    	  	kpi.setValue(rs.getFloat("value"));
				    	  	t = rs.getTimestamp("lastUpdated");
				    	  	kpi.setDate(sdf.format(t));
				    	  	kpi.setIdCity(id_city);
				    	  	kpi.setKpiLevel(level);
				    	  	kpi.setKpiCategory(rs.getString("kpiCategory"));
				    	  	kpi.setIdKPI(rs.getInt("id"));
				    	  	kpi.setTableId(rs.getInt("tableid"));
				    	  	kpi.setIsManual("citymanual");
				    	  	kpi.setMotivation(rs.getString("motivation"));
				    	  	kpi.setHumanCategory(getHCategory(kpi.getKpiCategory()));
				    	  	kpi.setIcon(getMyIcon(kpi.getKpiCategory()));
				    	  	logger.debug("trovata icona" + kpi.getIcon());
				    	  	//logger.debug("trovato kpi" + kpi.getName());
				    	  	kpiList.add(kpi);
				      }
				      
				      
					}
			
				}
				
				else {
					query="SELECT a.lTS, b.*\n" + 
							"FROM (SELECT id, MAX(lastUpdated) as lTS\n" + 
							"	from eindhovenkpilist \n" + 
							"	where kpiLevel='"+level+"' AND id_city="+id_city+ " \n" + 
							"	GROUP BY id) a JOIN eindhovenkpilist b\n" + 
							"      ON a.id = b.id\n" + 
							"WHERE b.kpiLevel='"+level+"' AND b.id_city="+id_city+ " AND\n" + 
							"      a.lTS = b.lastUpdated;";
					//logger.info(query);
					Statement st = conn.createStatement();
				      
				      // execute the query, and get a java resultset
				      ResultSet rs = st.executeQuery(query);
				      KPIBean kpi = null;
				      
					//ps = (CallableStatement) conn.prepareCall("");
				      if(rs != null) 
					  {
				    	  java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						Timestamp t;
				      while (rs.next())
				      {
				    	  	kpi = new KPIBean();
				    	  	
				    	  	kpi.setName(rs.getString("name"));
				    	  	kpi.setDescription(rs.getString("description"));
				    	  	kpi.setUnit(rs.getString("measureunit"));
				    	  	kpi.setValue(rs.getFloat("value"));
				    	  	t = rs.getTimestamp("lastUpdated");
				    	  	kpi.setDate(sdf.format(t));
				    	  	kpi.setIdCity(id_city);
				    	  	kpi.setKpiLevel(level);
				    	  	kpi.setKpiCategory(rs.getString("kpiCategory"));
				    	  	kpi.setIdKPI(rs.getInt("id"));
				    	  	kpi.setTableId(rs.getInt("tableid"));
				    	  	kpi.setIsManual("citymanual");
				    	  	kpi.setMotivation(rs.getString("motivation"));
				    	  	kpi.setHumanCategory(getHCategory(kpi.getKpiCategory()));
				    	  	kpi.setIcon(getMyIcon(kpi.getKpiCategory()));
				    	  	logger.debug("trovata icona" + kpi.getIcon());
				    	  	
				    	  	//logger.debug("trovato kpi" + kpi.getName());
				    	  	kpiList.add(kpi);
					
				}
				
			}
		
			
		      
			}
				
			
		}
		      
		     
		      conn.close();
		}
		catch(Exception e)
		{			
			e.printStackTrace();
			return null;
		}

		return kpiList;
		
		}

	
	
	public static String getHCategory(String c) {
		String category;
		switch(c) {
		case "climate":
			category = "climate change adaptation and mitigation";
			break;
		case "airquality":
			category = "air quality";
			break;
		case "economic":
			category = "economic opportunities and green jobs";
			break;
		case "water":
			category = "water management";
			break;
		case "green":
			default:
			category = "green space management";
			break;
		}
			
		
		return category;
		
		
	}
	
	public static String getMyIcon(String category) {
		String i;
		switch(category) {
		case "climate":
			i = "cloud_queue";
			break;
		case "airquality":
			i = "warning";
			break;
		case "economic":
			i = "euro_symbol";
			break;
		case "water":
			i = "waves";
			break;
		case "green":
			default:
			i = "nature_people";
			break;
		}
			
		
		return i;
		
		
	}

	public static ArrayList<City> getCities(){
		Logger logger = Logger.getLogger(KPIDao.class.getName());
		Connection conn = null;
		City city;
		ArrayList<City> cities = new ArrayList<City>();
		try {
		
			conn = DBConnection.createConnection();
			String query = "SELECT city_name from cities";
		      //logger.debug("query:" + query);
			     Statement st = conn.createStatement();
			     ResultSet rs = st.executeQuery(query);
			     //Integer retrieveID = 0;
			     if(rs != null) 
				  {
					
			      while (rs.next())
			      { 
			    	  	city = new City();
			    	  	city.setName(rs.getString("city_name")); 
			    	  	cities.add(city);
			      }
				  }
			     
			     conn.close();
			
		}
		catch(Exception e)
		{			
			e.printStackTrace();
			
		}
		
		return cities;
		
	}
	
	public static Integer getMaxTableId() {
		Logger logger = Logger.getLogger(KPIDao.class.getName());
		Integer max = 0;
		Connection conn = null;
		
		try {
			conn = DBConnection.createConnection();
			String query = "SELECT MAX(`tableid`) FROM `kpi`";
			//logger.debug("query: " + query);
			Statement st = conn.createStatement();
		    ResultSet rs = st.executeQuery(query);
		     //Integer retrieveID = 0;
		     if(rs != null) 
			  {
				
		      while (rs.next())
		      { 
		    	  	max = rs.getInt("MAX(`tableid`)");
		      }
			  }
		     
		     conn.close();
		
	}
	catch(Exception e)
	{			
		e.printStackTrace();
		
	}
		return max;
	}
	
	public static String getCategory(Integer idKPI) {
		Logger logger = Logger.getLogger(KPIDao.class.getName());
		Connection conn = null;
		String cat = null; 
		try {
			
			conn = DBConnection.createConnection();
			String query = "SELECT kpiCategory from kpi";
		      //logger.debug("query:" + query);
			     Statement st = conn.createStatement();
			     ResultSet rs = st.executeQuery(query);
			     //Integer retrieveID = 0;
			     if(rs != null) 
				  {
					
			      while (rs.next())
			      { 
			    	  	cat = rs.getString("kpiCategory");
			      }
				  }
			     
			     conn.close();
			
		}
		catch(Exception e)
		{			
			e.printStackTrace();
			
		}
		
		return cat;
		
		
		
	}
	
	public static ArrayList<Integer> getIdKnowageInKpi(){
		Logger logger = Logger.getLogger(KPIDao.class.getName());
		ArrayList<Integer> idsKnowageinKpi = new ArrayList<Integer>();
		Connection conn = null;
		try {
			conn = DBConnection.createConnection();
			String query="SELECT id_knowage FROM kpi WHERE id_knowage IS NOT NULL";
			//logger.info(query);
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery(query);
			Integer id= 0;
			if(rs!=null) {
				while(rs.next()) {
					id = rs.getInt("id_knowage");
					idsKnowageinKpi.add(id);
				}
			}
			
			conn.close();
		}
		catch(Exception e)
		{			
			e.printStackTrace();
			
		}
		
		return idsKnowageinKpi;
	}

	public static void addKPI(Integer id, String name, String desc, String unit, Integer city, String level,
			String category, String isManual, float value) {
		
		Logger logger = Logger.getLogger(KPIDao.class.getName());
		Connection conn = null;
		try {
			conn = DBConnection.createConnection();
			String query="INSERT INTO kpi(`name`,`isManual`,`tableid`,`description`,`measureUnit`,`kpiLevel`,`kpiCategory`) VALUES ('"+name+"','"+isManual+"',"+id+",'"+desc+"','"+unit+"','"+level+"','"+category+"')";
			//logger.info(query);
			Statement st = conn.createStatement();
		    //Integer lastId;
		      // execute the query, and get a java resultset
		     st.executeUpdate(query);
		      //logger.debug("new kpi con id: " + lastId);
		     System.out.println("ho creato un nuovo kpi"); 
		     
		     
		     
		      conn.close();
		      
		      conn = DBConnection.createConnection();
		      String query2 = "SELECT id_kpi from kpi WHERE name='"+name+"'";
		      //logger.debug("altra query:" + query2);
			     Statement st2 = conn.createStatement();
			     ResultSet rs = st2.executeQuery(query2);
			     Integer retrieveID = 0;
			     if(rs != null) 
				  {
					
			      while (rs.next())
			      { 
			    	  	retrieveID = rs.getInt("id_kpi");
			    	  	//logger.debug("id kpi creato: "+retrieveID);
			      }
				  }
			     
			     conn.close();
			     
			     //inserisco l'input iniziale per tutti i piloti... 
			     //sarebbe meglio parametrizzarlo
			     editKPIValue(1,retrieveID,value);
			     editKPIValue(2,retrieveID,value);
			     editKPIValue(3,retrieveID,value);
			  
		}
		catch(Exception e)
		{			
			e.printStackTrace();
			
		}
	}
	
	
	public static void addCityKPI(Integer id, String name, String desc, String unit, Integer city, String level,
			String category, float value) {
		
		Logger logger = Logger.getLogger(KPIDao.class.getName());
		Connection conn = null;
		try {
			conn = DBConnection.createConnection();
			String query= "";
			String query2="";
			Statement st;
			Statement st2;
			ResultSet rs;
			Integer retrieveID = 0;
			switch(city) {
			case 1: 
				query = "INSERT INTO genova_kpi(`name`,`tableid`,`description`,`measureUnit`,`kpiLevel`,`kpiCategory`) VALUES ('"+name+"',"+id+",'"+desc+"','"+unit+"','"+level+"','"+category+"')";
				st = conn.createStatement();
				st.executeUpdate(query);
				conn.close();
			    conn = DBConnection.createConnection();
			    query2 = "SELECT id from genova_kpi WHERE name='"+name+"'";
			    st2 = conn.createStatement();
			    rs = st2.executeQuery(query2);
			    
			     if(rs != null) 
				  {
					
			      while (rs.next())
			      { 
			    	  	retrieveID = rs.getInt("id");
			    	  	//logger.debug("id kpi creato: "+retrieveID);
			      }
				  }
			     
			     conn.close();
			     editCityKPIValue(1,retrieveID,value);
				break;
			case 2:
				query = "INSERT INTO tampere_kpi(`name`,`tableid`,`description`,`measureUnit`,`kpiLevel`,`kpiCategory`) VALUES ('"+name+"',"+id+",'"+desc+"','"+unit+"','"+level+"','"+category+"')";
				st = conn.createStatement();
				st.executeUpdate(query);
				conn.close();
			    conn = DBConnection.createConnection();
			    query2 = "SELECT id from tampere_kpi WHERE name='"+name+"'";
			    st2 = conn.createStatement();
			    rs = st2.executeQuery(query2);
			    
			     if(rs != null) 
				  {
					
			      while (rs.next())
			      { 
			    	  	retrieveID = rs.getInt("id");
			    	  	//logger.debug("id kpi creato: "+retrieveID);
			      }
				  }
			     
			     conn.close();
			     editCityKPIValue(2,retrieveID,value);
				break;
			case 3: 
				query = "INSERT INTO eindhoven_kpi(`name`,`tableid`,`description`,`measureUnit`,`kpiLevel`,`kpiCategory`) VALUES ('"+name+"',"+id+",'"+desc+"','"+unit+"','"+level+"','"+category+"')";
				logger.debug(query);
				st = conn.createStatement();
				st.executeUpdate(query);
				conn.close();
			    conn = DBConnection.createConnection();
			    query2 = "SELECT id from eindhoven_kpi WHERE name='"+name+"'";
			    logger.debug(query2);
			    st2 = conn.createStatement();
			    rs = st2.executeQuery(query2);
			     
			     if(rs != null) 
				  {
					
			      while (rs.next())
			      { 
			    	  	retrieveID = rs.getInt("id");
			    	  	//logger.debug("id kpi creato: "+retrieveID);
			      }
				  }
			     
			     conn.close();
			     editCityKPIValue(3,retrieveID,value);
				break;
			}
			  
		}
		catch(Exception e)
		{			
			e.printStackTrace();
			
		}
	}



	public static ArrayList<KPIBean> getCommon(Integer id_city) {
		Logger logger = Logger.getLogger(KPIDao.class.getName());
		String level = "task";
		Connection conn = null;
		
		ArrayList<KPIBean> commonKpi = new ArrayList<KPIBean>();
		
		try {
			conn = DBConnection.createConnection();
			

			String query="SELECT a.lTS, b.*\n" + 
					"FROM (SELECT id_kpi, MAX(lastUpdated) as lTS\n" + 
					"	from kpilist \n" + 
					"	where kpiLevel='"+level+"' AND id_city="+id_city+ " AND isManual='manual'\n" + 
					"	GROUP BY id_kpi) a JOIN kpilist b\n" + 
					"      ON a.id_kpi = b.id_kpi\n" + 
					"WHERE b.kpiLevel='"+level+"' AND b.id_city="+id_city+ " AND isManual='manual' AND\n" + 
					"      a.lTS = b.lastUpdated;";
			//logger.info(query);
			Statement st = conn.createStatement();
		      
		      // execute the query, and get a java resultset
		      ResultSet rs = st.executeQuery(query);
		      KPIBean kpi = null;
		      
			//ps = (CallableStatement) conn.prepareCall("");
		      if(rs != null) 
			  {
		    	  java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		    	  Timestamp t;
		    	  //GetCommonCategory g = new GetCommonCategory();
		      while (rs.next())
		      {
		    	  	kpi = new KPIBean();
		    	  	
		    	  	kpi.setName(rs.getString("name"));
		    	  	kpi.setDescription(rs.getString("description"));
		    	  	kpi.setUnit(rs.getString("measureunit"));
		    	  	kpi.setValue(rs.getFloat("value"));
		    	  	t = rs.getTimestamp("lastUpdated");
		    	  	kpi.setDate(sdf.format(t));
		    	  	kpi.setIdCity(id_city);
		    	  	kpi.setKpiLevel(level);
		    	  	kpi.setKpiCategory(rs.getString("kpiCategory"));
		    	  	kpi.setIdKPI(rs.getInt("id_kpi"));
		    	  	kpi.setTableId(rs.getInt("tableid"));
		    	  	kpi.setHumanCategory(getHCategory(kpi.getKpiCategory()));
		    	  	kpi.setIcon(getMyIcon(kpi.getKpiCategory()));
		    	  	kpi.setIsManual("manual");
		    	  	kpi.setMotivation(rs.getString("motivation"));
		    	  	//logger.debug("trovato kpi" + kpi.getName());
		    	  	commonKpi.add(kpi);
		      }
		      
		      
			}
		      
		     System.out.println("sono fuori"); 
		      conn.close();
		}
		catch(Exception e)
		{			
			e.printStackTrace();
			return null;
		}

		
		return commonKpi;
	}
	
	public static ArrayList<KPIBean> getFilteredCommon(Integer id_city, String filters[]){
		Logger logger = Logger.getLogger(KPIDao.class.getName());
		String level = "task";
		Connection conn = null;
		ArrayList<KPIBean> filteredKPI = new ArrayList<KPIBean>();
		String categories = "";
		for (int i = 0; i<filters.length; i++) {
			categories = categories + "b.kpiCategory='"+filters[i]+"'";
			if(i <= filters.length-2) {
				categories = categories + " OR ";
			}
		}
		
		//logger.debug("categories: "+ categories);
		
		try {
			conn = DBConnection.createConnection();
			

			String query="SELECT a.lTS, b.*\n" + 
					"FROM (SELECT id_kpi, MAX(lastUpdated) as lTS\n" + 
					"	from kpilist \n" + 
					"	where kpiLevel='"+level+"' AND id_city="+id_city+ " AND isManual='manual'\n" + 
					"	GROUP BY id_kpi) a JOIN kpilist b\n" + 
					"      ON a.id_kpi = b.id_kpi\n" + 
					"WHERE b.kpiLevel='"+level+"' AND (" + categories + ") AND b.id_city="+id_city+ " AND isManual='manual' AND\n" + 
					"      a.lTS = b.lastUpdated;";
			//logger.info(query);
			Statement st = conn.createStatement();
		      
		      // execute the query, and get a java resultset
		      ResultSet rs = st.executeQuery(query);
		      KPIBean kpi = null;
		      
			//ps = (CallableStatement) conn.prepareCall("");
		      if(rs != null) 
			  {
		    	  java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		    	  Timestamp t;
		    	  //GetCommonCategory g = new GetCommonCategory();
		      while (rs.next())
		      {
		    	  	kpi = new KPIBean();
		    	  	
		    	  	kpi.setName(rs.getString("name"));
		    	  	kpi.setDescription(rs.getString("description"));
		    	  	kpi.setUnit(rs.getString("measureunit"));
		    	  	kpi.setValue(rs.getFloat("value"));
		    	  	kpi.setMotivation(rs.getString("motivation"));
		    	  	t = rs.getTimestamp("lastUpdated");
		    	  	kpi.setDate(sdf.format(t));
		    	  	kpi.setIdCity(id_city);
		    	  	kpi.setKpiLevel(level);
		    	  	kpi.setKpiCategory(rs.getString("kpiCategory"));
		    	  	kpi.setIdKPI(rs.getInt("id_kpi"));
		    	  	kpi.setTableId(rs.getInt("tableid"));
		    	  	kpi.setHumanCategory(getHCategory(kpi.getKpiCategory()));
		    	  	kpi.setIcon(getMyIcon(kpi.getKpiCategory()));
		    	  	//logger.debug("trovato kpi" + kpi.getName());
		    	  	filteredKPI.add(kpi);
		      }
		      
		      
			}
		      
		     //System.out.println("sono fuori"); 
		      conn.close();
		}
		catch(Exception e)
		{			
			e.printStackTrace();
			return null;
		}

		return filteredKPI;
		
	}



	public static void updateKPI(Integer kPI_id, String name, String desc, String unit, String isManual, Integer id_knowage) {
		Logger logger = Logger.getLogger(KPIDao.class.getName());
		Connection conn = null;
		String query;
		if(id_knowage != null) {
			query = "UPDATE kpi SET name='" + name + "', description='" + desc + "', isManual='"+isManual+"', measureUnit='"+ unit +"', id_knowage="+id_knowage+ " where id_kpi="+ kPI_id;
		}
		else {
			query = "UPDATE kpi SET name='" + name + "', description='" + desc + "', isManual='"+isManual+"', measureUnit='"+ unit +"', id_knowage=NULL" + " where id_kpi="+ kPI_id;
		}
		try {
			conn = DBConnection.createConnection();
			
			Statement st = conn.createStatement();
		     st.executeUpdate(query);
		    
		     
		     
		     
		      conn.close();
		}
		
		catch(Exception e)
		{			
			e.printStackTrace();
			
		}

		
	}

	
	public static void updateCityKPI(Integer kPI_id, String name, String desc, String unit, String isManual, Integer id_knowage, Integer city) {
		Logger logger = Logger.getLogger(KPIDao.class.getName());
		Connection conn = null;
		String query = "";
		switch(city) {
		case 1: 
			if(id_knowage != null) {
				query = "UPDATE kpi SET name='" + name + "', description='" + desc + "', isManual='"+isManual+"', measureUnit='"+ unit +"', id_knowage="+id_knowage+ " where id_kpi="+ kPI_id;
				logger.debug(query);
			}
			else {
				query = "UPDATE genova_kpi SET name='" + name + "', description='" + desc + "', measureUnit='"+ unit +"' where id="+ kPI_id;
				logger.debug(query);
			}
			break;
		case 2: 
			if(id_knowage != null) {
				query = "UPDATE kpi SET name='" + name + "', description='" + desc + "', isManual='"+isManual+"', measureUnit='"+ unit +"', id_knowage="+id_knowage+ " where id_kpi="+ kPI_id;
				logger.debug(query);
			}
			else {
				query = "UPDATE tampere_kpi SET name='" + name + "', description='" + desc + "', measureUnit='"+ unit +"' where id="+ kPI_id;
				logger.debug(query);
			}
			break;
		case 3:
			if(id_knowage != null) {
				query = "UPDATE kpi SET name='" + name + "', description='" + desc + "', isManual='"+isManual+"', measureUnit='"+ unit +"', id_knowage="+id_knowage+ " where id_kpi="+ kPI_id;
				logger.debug(query);
			}
			else {
				query = "UPDATE eindhoven_kpi SET name='" + name + "', description='" + desc + "', measureUnit='"+ unit +"' where id="+ kPI_id;
				logger.debug(query);
			}
			break;
		}
		try {
			conn = DBConnection.createConnection();
			
			Statement st = conn.createStatement();
		     st.executeUpdate(query);
		    
		     
		     
		     
		      conn.close();
		
		}
		
		catch(Exception e)
		{			
			e.printStackTrace();
			
		}

		
	}



	public static void addComputedKPI(String idS, Integer id_knowage, String name, String desc, String unit,
			String level, String category, String isManual) {
		Logger logger = Logger.getLogger(KPIDao.class.getName());
		Connection conn = null;
		try {
			conn = DBConnection.createConnection();
			String query="INSERT INTO kpi(`name`,`isManual`,`tableid`,`description`,`measureUnit`,`kpiLevel`,`kpiCategory`,`id_knowage`) VALUES ('"+name+"','"+isManual+"',"+idS+",'"+desc+"','"+unit+"','"+level+"','"+category+"'," + id_knowage+")";
			//logger.info(query);
			Statement st = conn.createStatement();
		    //Integer lastId;
		      // execute the query, and get a java resultset
		     st.executeUpdate(query);
		      //logger.debug("new kpi con id: " + lastId);
		     //System.out.println("ho creato un nuovo kpi calcolato"); 
		     
		     
		     
		      conn.close();
		      
		      
			  
		}
		catch(Exception e)
		{			
			e.printStackTrace();
			
		}
		
	}



	public static Integer getSingleIdKnowage(String tableID) {
		Logger logger = Logger.getLogger(KPIDao.class.getName());
		Connection conn = null;
		Integer id = 0;
		try {
			conn = DBConnection.createConnection();
			String query="SELECT id_knowage from kpi where tableid="+tableID;
			logger.info(query);
			Statement st = conn.createStatement();
		   
		      // execute the query, and get a java resultset
		     ResultSet rs = st.executeQuery(query);
		      //logger.debug("new kpi con id: " + lastId);
		     
		     
		     if(rs != null) {
		    	 	while(rs.next()) {
		    	 		id = rs.getInt("id_knowage");
		    	 	}
		     }
		     
		      conn.close();
		      
		      
			  
		}
		catch(Exception e)
		{			
			e.printStackTrace();
			
		}
		return id;
	}

	public static Integer getIdKnowageByidKpi(Integer id) {
		Logger logger = Logger.getLogger(KPIDao.class.getName());
		Connection conn = null;
		Integer idK = 0;
		try {
			conn = DBConnection.createConnection();
			String query="SELECT id_knowage from kpi where id_kpi="+id;
			//logger.info(query);
			Statement st = conn.createStatement();
		   
		      // execute the query, and get a java resultset
		     ResultSet rs = st.executeQuery(query);
		      //logger.debug("new kpi con id: " + lastId);
		     
		     
		     if(rs != null) {
		    	 	while(rs.next()) {
		    	 		idK = rs.getInt("id_knowage");
		    	 	}
		     }
		     
		      conn.close();
		      
		      
			  
		}
		catch(Exception e)
		{			
			e.printStackTrace();
			
		}
		return idK;
	}


	public static String getValue(Integer id) {
		Logger logger = Logger.getLogger(KPIDao.class.getName());
		Connection conn = null;
		String name = null;
		try {
			conn = DBConnectionCalculated.createConnection();
			String query="SELECT NAME from SBI_KPI_KPI where ID="+id;
			logger.info(query);
			Statement st = conn.createStatement();
		   
		      // execute the query, and get a java resultset
		     ResultSet rs = st.executeQuery(query);
		      //logger.debug("new kpi con id: " + lastId);
		     
		     
		     if(rs != null) {
		    	 	while(rs.next()) {
		    	 		name = rs.getString("NAME");
		    	 	}
		     }
		     
		      conn.close();
		      
		      
			  
		}
		catch(Exception e)
		{			
			e.printStackTrace();
			
		}
		return name;	
	}



	public static boolean isPresentManualKpi(Integer kPI_id) {
		boolean isPresent = false;
		
		Logger logger = Logger.getLogger(KPIDao.class.getName());
		Connection conn = null;
		
		try {
			conn = DBConnection.createConnection();
			String query="SELECT id_input from input where id_kpi="+kPI_id;
			//logger.info(query);
			Statement st = conn.createStatement();
		   
		      // execute the query, and get a java resultset
		     ResultSet rs = st.executeQuery(query);
		      //logger.debug("new kpi con id: " + lastId);
		     
		     
		     if(rs != null) {
		    	 		isPresent = true;
		    	 	}
		     else {
		    	  	isPresent = false;
		     }
		     
		      conn.close();
		      
		      
			  
		}
		catch(Exception e)
		{			
			e.printStackTrace();
			
		}
		
		
		return isPresent;
	}
	
	public static void deleteCityKpi (Integer id, Integer city) {
		Logger logger = Logger.getLogger(KPIDao.class.getName());
		Connection conn = null;
		String query = "";
		
		switch (city) {
		case 1:
			query= "DELETE FROM genova_kpi WHERE id="+id;
			break;
		case 2: 
			query= "DELETE FROM tampere_kpi WHERE id="+id;
			break;
		case 3:
			query= "DELETE FROM eindhoven_kpi WHERE id="+id;
			break;
			
		}
		Statement st;
		int rs;
		conn = DBConnection.createConnection();
		try {
			st = conn.createStatement();
			rs = st.executeUpdate(query);
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
	}
	
	
	public static void deleteKpi (Integer id) {
		Logger logger = Logger.getLogger(KPIDao.class.getName());
		Connection conn = null;
		String query = "DELETE FROM kpi WHERE id_kpi="+id;
		Statement st;
		int rs;
		conn = DBConnection.createConnection();
		try {
			st = conn.createStatement();
			rs = st.executeUpdate(query);
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
	}
	
	public static boolean isPresentCityKpi(Integer kPI_id, Integer city) {
		boolean isPresent = false;
		
		Logger logger = Logger.getLogger(KPIDao.class.getName());
		Connection conn = null;
		String query= "";
		Statement st;
		ResultSet rs;
		
		try {
			
			switch(city) {
			case 1: 
			
			conn = DBConnection.createConnection();
			query="SELECT COUNT(*) as valore from KPI_UNaLab.genova_kpi where tableid="+kPI_id;
			//logger.info(query);
			st = conn.createStatement();
		   
		      // execute the query, and get a java resultset
		     rs = st.executeQuery(query);
		      //logger.debug("new kpi con id: " + lastId);
		     
		     
		     if(rs.next()) {
		    	 		isPresent = true;
		    	 	}
		     else {
		    	  	isPresent = false;
		     }
		     
		      conn.close();
		      break;
		      
			case 2: 
				conn = DBConnection.createConnection();
				query="SELECT COUNT(*) as valore from KPI_UNaLab.tampere_kpi where tableid="+kPI_id;
				//logger.info(query);
				st = conn.createStatement();
			   
			      // execute the query, and get a java resultset
			     rs = st.executeQuery(query);
			      //logger.debug("new kpi con id: " + lastId);
			     
			     
			     if(rs.next()) {
			    	 		isPresent = true;
			    	 	}
			     else {
			    	  	isPresent = false;
			     }
			     
			      conn.close();
			      break;
			      
			case 3:
				
				conn = DBConnection.createConnection();
				query="SELECT COUNT(*) as valore from KPI_UNaLab.eindhoven_kpi where tableid="+kPI_id;
				//logger.info(query);
				st = conn.createStatement();
			   
			      // execute the query, and get a java resultset
			     rs = st.executeQuery(query);
			      //logger.debug("new kpi con id: " + lastId);
			     
			     
			     if(rs.next()) {
			    	 		isPresent = true;
			    	 	}
			     else {
			    	  	isPresent = false;
			     }
			      conn.close();
			      break;
		      
			}  
			  
		}
		catch(Exception e)
		{			
			e.printStackTrace();
			
		}
		
		
		return isPresent;
	}
	
	
	
   public static ArrayList<KPIBean> getKpi4Mashup() {
	   ArrayList<KPIBean> kpis = new ArrayList<KPIBean>();
	   Logger logger = Logger.getLogger(KPIDao.class.getName());
		Connection conn = null;
		try {
			conn = DBConnection.createConnection();
			String query="SELECT id_kpi, name, isManual, description from kpi";
			//logger.info(query);
			Statement st = conn.createStatement();
		   
		      // execute the query, and get a java resultset
		     ResultSet rs = st.executeQuery(query);
		     KPIBean kpi = null;
		     if(rs != null) {
		    	 		while (rs.next()) {
		    	 			kpi = new KPIBean();
		    	 			
		    	 			
		    	 			kpi.setIdKPI(rs.getInt("id_kpi"));
		    	 			kpi.setName(rs.getString("name"));
		    	 			kpi.setDescription(rs.getString("description"));
		    	 			kpi.setIsManual(rs.getString("isManual"));
		    	 			
		    	 		    kpis.add(kpi);
		    	 		}
		     }
		      //logger.debug("new kpi con id: " + lastId);
		}
		catch(Exception e)
		{			
			e.printStackTrace();
			
		}
	   return kpis;
   }
	
		
	}
	
		

