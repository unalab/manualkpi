package main.java;
import java.util.ArrayList;
import java.util.List;

public class RolePermission {

	
	public static List<String> basePermission(){
		
		List<String> basePermission = new ArrayList<String>();
		
		basePermission.add("/");
		basePermission.add("/css/cardScroll.css");
		basePermission.add("/css/common.css");
		basePermission.add("/css/custom.css");
		basePermission.add("/css/materialize.css");
		basePermission.add("/css/materialize.min.css");
		basePermission.add("/css/table-sorter.css");
	
		basePermission.add("/img/Eindhoven.jpg");
		basePermission.add("/img/Genova.jpg");
		basePermission.add("/img/logo_alpha_270x.png");
		basePermission.add("/img/Tampere.jpg");
		basePermission.add("/img/icons/icon-uptake.png");
		basePermission.add("/img/umbrella.png");
		basePermission.add("/img/digital-enabler-white.png");
		
		basePermission.add("/js/materialize.js");
		basePermission.add("/js/common.js");
		basePermission.add("/js/custom.js");
		basePermission.add("/js/materialize.min.js");
		basePermission.add("/js/materialize-pagination.js");
		basePermission.add("/js/paginathing.js");
		basePermission.add("/js/pagination.js");
		basePermission.add("/js/table-sorter.js");
		basePermission.add("/js/variable.js");
		
		
		
		basePermission.add("/login.jsp");
		basePermission.add("/dashboard.jsp");
		basePermission.add("/index.jsp");
		basePermission.add("/commonKPI.jsp");
		basePermission.add("/guest.jsp");
		
		basePermission.add("/ChoosePilot");
		//basePermission.add("/GetKPIList");
		
		basePermission.add("/GetCalculatedIndicators");
		basePermission.add("/GetCommonKPIs");
		basePermission.add("/GetFilteredIndicators");
		basePermission.add("/GetIdCityKnowage");
		basePermission.add("/GetIdKnowage");
		basePermission.add("/GetGuestKPIList");
		basePermission.add("/GetValue");
		basePermission.add("/Log4JInitServlet");
		basePermission.add("/LoginServlet");
		

		
		return basePermission;
	}
	
	public static boolean adminPermission(String path) {
		
		List<String> admin = basePermission();
		
		  admin.add("/admin.jsp");
		
		
		admin.add("/AddAdminKPI");
		admin.add("/EditKPI");
		admin.add("/GetAdminKPIList");
		admin.add("/ModifyAdminKPI");
		admin.add("/LogoutServlet");
		
		
		
		if (admin.contains(path))
			return true;
		else 
			return false;
	}
	
public static boolean citymanagerPermission(String path) {
		
		List<String> citymanager = basePermission();
		
		 citymanager.add("/view.jsp");
		
		
		
		citymanager.add("/AddKPI");
		citymanager.add("/EditKPI");
		citymanager.add("/EditCityKPI");
		citymanager.add("/GetKPIList");
		citymanager.add("/ModifyCityKPI");
		citymanager.add("/LogoutServlet");
		citymanager.add("/AddComputedKPI");
		
		
		
		if (citymanager.contains(path))
			return true;
		else 
			return false;
	}
	
	public static boolean guestPermission(String path) {
		
		List<String> guest = basePermission();
				
		if (guest.contains(path))
			return true;
		else	
			return false;
	}
}

