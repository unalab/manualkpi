package main.java;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import main.bean.*;

public class AuthenticationFilter implements Filter {
	private ServletContext context;
	Logger logger = Logger.getLogger(AuthenticationFilter.class.getName());
	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;

		HttpSession session = req.getSession(true);

		String path = req.getRequestURI().substring(req.getContextPath().length()).toString();
		String role = "null";

		
		role = (String) session.getAttribute("user");
			
		if(role==null) {
			role = "null";
		}


		//        String role = String.valueOf(session.getAttribute("userStatus"));
		//        String path = req.getRequestURI().substring(req.getContextPath().length()).toString();

		switch (role) {

		case "admin":
			//System.out.println(RolePermission.adminPermission(path));
			if (RolePermission.adminPermission(path)) {
				chain.doFilter(request, response);
			    
			}
			else {
				res.sendRedirect(req.getContextPath() +"/index.jsp"); }//sostituire con pagina iniziale admin
			break;
		
		case "eindhoven_city_manager":
		case "genova_city_manager":
		case "tampere_city_manager":
			if(RolePermission.citymanagerPermission(path)) {
				chain.doFilter(request, response);
				
			}
			else {
				res.sendRedirect(req.getContextPath() + "/index.jsp"); }
			break;
			
		
		case "null":
			//System.out.println(RolePermission.guestPermission(path));
			if (RolePermission.guestPermission(path))
				chain.doFilter(request, response);
			else {
				this.context.log(path + " Accesso non autorizzato");
				res.sendRedirect(req.getContextPath() + "/index.jsp");}
			break;
		default:
			break;
		}  

	}

	@Override
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
		this.context = fConfig.getServletContext();
		this.context.log("AuthenticationFilter è inizializzata");
	}

}
