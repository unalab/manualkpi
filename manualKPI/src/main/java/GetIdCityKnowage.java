package main.java;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import main.utils.GetCommonCategory;

/**
 * Servlet implementation class GetIdCityKnowage
 */
@WebServlet("/GetIdCityKnowage")
public class GetIdCityKnowage extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static Logger logger = Logger.getLogger(GetIdCityKnowage.class.getName());
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetIdCityKnowage() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String city = request.getParameter("city");
		logger.debug("CITY STRING: "+ city);
		if(city.equalsIgnoreCase("Eindhoven") || city.equalsIgnoreCase("Genova") || city.equalsIgnoreCase("Tampere")) {
			city = GetCommonCategory.getIdCity(city);
		}
		String cit = GetCommonCategory.getCity(city);
		Integer id_category = CalculatedKPIDao.getCategoryId(cit, "KPI_KPI_CATEGORY");
		logger.debug("ID CATEGORY: " + id_category);
		
		String s = id_category.toString();
		response.getWriter().write(s);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
