package main.java;
 
import java.sql.Connection;
import java.sql.DriverManager;

import org.apache.log4j.Logger;


 
public class DBConnection {
	
	static Logger logger = Logger.getLogger(DBConnection.class.getName());
 
public static Connection createConnection()
{
Connection con = null;

String url = PropertyManager.getProperty(manualKPIProperties.DB_HOST);
String username = PropertyManager.getProperty(manualKPIProperties.DB_USERNAME);
String password = PropertyManager.getProperty(manualKPIProperties.DB_PASSWORD);
 
try
{
	try
	{
		Class.forName("com.mysql.jdbc.Driver");
	}
	catch (ClassNotFoundException e)
	{
		e.printStackTrace();
	}
con = DriverManager.getConnection(url, username, password);
//logger.info("db connection");
//System.out.println("Post establishing a DB connection - "+con);
}
catch (Exception e)
{
e.printStackTrace();
}
 
return con;
}
}