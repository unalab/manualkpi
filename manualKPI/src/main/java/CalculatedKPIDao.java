package main.java;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.json.JSONObject;

import main.java.DBConnectionCalculated;

public class CalculatedKPIDao {
	static Logger logger = Logger.getLogger(CalculatedKPIDao.class.getName());
	
	public static ArrayList<Integer> getIds (ArrayList<KPIBean> kpicomputedList,Integer id_city){
		ArrayList<Integer> idKnowage = new ArrayList<Integer>();
		Connection conn = null;
		
		try {
			conn = DBConnectionCalculated.createConnection();
			String query = "SELECT ID from SBI_KPI_KPI where CATEGORY_ID="+id_city;
			//logger.info(query);
			Statement st = conn.createStatement();
		      
//		      // execute the query, and get a java resultset
		      ResultSet rs = st.executeQuery(query);
		      Integer id = 0;
		      if(rs!=null) {
		    	  		
		    	  	while (rs.next()) {
		    	  		id = rs.getInt("ID");
		    	  		idKnowage.add(id);
		    	  	}
		      }
		      
		      conn.close();
		}
		catch(Exception e)
		{			
			e.printStackTrace();
			return null;
		}

		return idKnowage;
		
	}
	
	
	public static ArrayList<KPIBean> getFilteredComputed(Integer id_city, String filters[]){
		Logger logger = Logger.getLogger(CalculatedKPIDao.class.getName());
		String level = "task";
		Connection conn = null;
		ArrayList<KPIBean> filteredKPI = new ArrayList<KPIBean>();
		String categories = "";
		for (int i = 0; i<filters.length; i++) {
			categories = categories + "kpiCategory='"+filters[i]+"'";
			if(i <= filters.length-2) {
				categories = categories + " OR ";
			}
		}
		
		//logger.debug("categories: "+ categories);
		
		try {
			conn = DBConnection.createConnection();
			
			String query = "SELECT * FROM kpi WHERE kpiLevel='"+level+"' AND (" + categories + ") AND isManual='automatic' AND id_knowage IS NOT NULL";

			
			//logger.info(query);
			Statement st = conn.createStatement();
		      
		      // execute the query, and get a java resultset
		      ResultSet rs = st.executeQuery(query);
		      KPIBean kpi = null;
		      
			//ps = (CallableStatement) conn.prepareCall("");
		      if(rs != null) 
			  {
//		    	  java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//		    	  Timestamp t;
//		    	  //GetCommonCategory g = new GetCommonCategory();
		      while (rs.next())
		      {
		    	  	kpi = new KPIBean();
		    	  	
		    	  	kpi.setName(rs.getString("name"));
		    	  	kpi.setDescription(rs.getString("description"));
		    	  	kpi.setUnit(rs.getString("measureunit"));
		    	  	//kpi.setValue(rs.getFloat("value"));
		    	  	//t = rs.getTimestamp("lastUpdated");
		    	  	//kpi.setDate(sdf.format(t));
		    	  	kpi.setIdCity(id_city);
		    	  	kpi.setKpiLevel(level);
		    	  	kpi.setKpiCategory(rs.getString("kpiCategory"));
		    	  	kpi.setIdKPI(rs.getInt("id_kpi"));
		    	  	kpi.setTableId(rs.getInt("tableid"));
		    	  	kpi.setIdKnowage(rs.getInt("id_knowage"));
		    	  	kpi.setHumanCategory(KPIDao.getHCategory(kpi.getKpiCategory()));
		    	  	kpi.setIcon(KPIDao.getMyIcon(kpi.getKpiCategory()));
		    	  	//logger.debug("trovato kpi" + kpi.getName());
		    	  	filteredKPI.add(kpi);
		      }
		      
		      
			}
		      
		     System.out.println("sono fuori"); 
		      conn.close();
		}
		catch(Exception e)
		{			
			e.printStackTrace();
			return null;
		}

		return filteredKPI;
		
	}

	
	public static ArrayList<KPIBean> getComputedKPI(String level, String category, Integer id_city){
		Connection conn = null;
		
		
		

		ArrayList<KPIBean> kpicomputedList = new ArrayList<KPIBean>();
		//ArrayList<Integer> idKnowage = new ArrayList<Integer>();
		
		try {
			conn = DBConnection.createConnection();
			String query;
			if(category.equalsIgnoreCase("all")) {
				query = "SELECT `id_kpi`,`kpiCategory`,`name`,`tableid`,`description`,`measureUnit`,`id_knowage` FROM kpi WHERE kpiLevel='"+level+"' AND isManual='AUTOMATIC'";
				//logger.info(query);
				Statement st = conn.createStatement();
			      
			      // execute the query, and get a java resultset
			      ResultSet rs = st.executeQuery(query);
			      KPIBean kpi = null;
			      if(rs!=null) {
			    	  	while (rs.next()) {
			    	  		kpi = new KPIBean();
				    	  	
				    	  	kpi.setName(rs.getString("name"));
				    	  	kpi.setDescription(rs.getString("description"));
				    	  	kpi.setUnit(rs.getString("measureunit"));
				    	  	kpi.setKpiLevel(level);
				    	  	kpi.setKpiCategory(rs.getString("kpiCategory"));
				    	  	kpi.setIdKPI(rs.getInt("id_kpi"));
				    	  	kpi.setTableId(rs.getInt("tableid"));
				    	  	kpi.setIdKnowage(rs.getInt("id_knowage"));
				    	  	kpi.setIsManual("automatic");
				    	  	kpi.setIdCity(id_city);
				    	  	kpi.setIcon(KPIDao.getMyIcon(kpi.getKpiCategory()));
				    	  	
				    	  	//logger.debug("trovato kpi" + kpi.getName());
				    	  	kpicomputedList.add(kpi);
			    	  	}
			      }
			}
			else {
				query = "SELECT `id_kpi`,`name`,`tableid`,`description`,`measureUnit`,`id_knowage` FROM kpi WHERE kpiLevel='"+level+"' AND kpiCategory='"+category+"' AND isManual='AUTOMATIC'";
				//logger.info(query);
				Statement st = conn.createStatement();
			      
			      // execute the query, and get a java resultset
			      ResultSet rs = st.executeQuery(query);
			      KPIBean kpi = null;
			      if(rs!=null) {
			    	  	while (rs.next()) {
			    	  		kpi = new KPIBean();
				    	  	
				    	  	kpi.setName(rs.getString("name"));
				    	  	kpi.setDescription(rs.getString("description"));
				    	  	kpi.setUnit(rs.getString("measureunit"));
				    	  	kpi.setKpiLevel(level);
				    	  	kpi.setKpiCategory(category);
				    	  	kpi.setIdKPI(rs.getInt("id_kpi"));
				    	  	kpi.setTableId(rs.getInt("tableid"));
				    	  	kpi.setIdKnowage(rs.getInt("id_knowage"));
				    	  	kpi.setIsManual("automatic");
				    	  	kpi.setIcon(KPIDao.getMyIcon(kpi.getKpiCategory()));
				    	  	//logger.debug("trovato kpi" + kpi.getName());
				    	  	kpicomputedList.add(kpi);
			    	  	}
			      }
			}
			
			
			conn.close();


		      }
		
		
		
		catch(Exception e)
		{			
			e.printStackTrace();
			return null;
		}

		return kpicomputedList;
	}
	
	public static boolean isPresent4City(Integer kPI_id, Integer categoryCity) {
		boolean isPresent = false;
		
		Logger logger = Logger.getLogger(CalculatedKPIDao.class.getName());
		Connection conn = null;
		
		try {
			conn = DBConnectionCalculated.createConnection();
			String query="SELECT * from SBI_KPI_KPI where ID="+kPI_id + " AND CATEGORY_ID="+categoryCity;
			//logger.info(query);
			Statement st = conn.createStatement();
		   
		      // execute the query, and get a java resultset
		     ResultSet rs = st.executeQuery(query);
		      //logger.debug("new kpi con id: " + lastId);
		     
		     
		     if(rs != null) {
		    	 		isPresent = true;
		    	 	}
		     else {
		    	  	isPresent = false;
		     }
		     
		      conn.close();
		      
		      
			  
		}
		catch(Exception e)
		{			
			e.printStackTrace();
			
		}
		
		
		return isPresent;
	}
	
	
	public static ArrayList<KPIBean> getComputedValues(ArrayList<KPIBean> kpicomputedList, ArrayList<Integer> idKnowage) {
		//ArrayList computed = new ArrayList<KPIBean>();
		Connection conn =  null;
		
		//Statement st = conn.createStatement();
		try {
	      for(int i = 0; i<idKnowage.size(); i++) {
	    	  	
	    	  for (int j=0; j<kpicomputedList.size(); j++) {
	    		  if(idKnowage.get(i)==kpicomputedList.get(j).getIdKnowage()) {
	    			  conn = DBConnectionCalculated.createConnection();
	    			  String query = "SELECT COMPUTED_VALUE, MAX(TIME_RUN) FROM SBI_KPI_VALUE WHERE KPI_ID="+kpicomputedList.get(j).getIdKnowage();
	    			 //logger.info(query);
	    			 Statement st = conn.createStatement();
	    			 ResultSet rs = st.executeQuery(query);
	    			 logger.debug("RS : " + rs.toString());
	    			 
	    		if(rs.next()) {
	    			 do {
						logger.debug("perchè sei qui?");
						java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					 Timestamp t;
					float value = (float) rs.getInt("COMPUTED_VALUE");
						t=rs.getTimestamp("MAX(TIME_RUN)");
						if(t == null) {
							//sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
							 Date dateobj = new Date();
							 //System.out.println(sdf.format(dateobj));
							 kpicomputedList.get(j).setDate(sdf.format(dateobj));
							 value = 0;
						  	 kpicomputedList.get(j).setValue(value);
						}
						else {
							kpicomputedList.get(j).setDate(sdf.format(t));
							kpicomputedList.get(j).setValue(value);
						}
	    			 } while (rs.next());
	    			 rs.close();
	    		  }
	    			 else{
	    				     java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						 Date dateobj = new Date();
						 //System.out.println(sdf.format(dateobj));
						 kpicomputedList.get(j).setDate(sdf.format(dateobj));
						 float value = 0;
					  	 kpicomputedList.get(j).setValue(value);
	    				 
	    			 }
	 		    	  	}
	    			 
	    		  }
	      }
	      
	      
	      conn.close();
	      
		}
//	
//	
//	
	catch(Exception e)
	{			
		e.printStackTrace();
		//return null;
	}

	return kpicomputedList;
}
	
	public static List<JSONObject> getFormattedFilteredResult(ResultSet rs) {
		List<JSONObject> resList = new ArrayList<JSONObject>();
		try {
			// get column names
			ResultSetMetaData rsMeta = rs.getMetaData();
			int columnCnt = rsMeta.getColumnCount();
			List<String> columnNames = new ArrayList<String>();
			for(int i=1;i<=columnCnt;i++) {
				
				columnNames.add(rsMeta.getColumnName(i).toUpperCase());
				
			}

			while(rs.next()) { // convert each object to an human readable JSON object
				JSONObject obj = new JSONObject();
				for(int i=1;i<=columnCnt;i++) {
					String key = columnNames.get(i - 1);
					String value = rs.getString(i);
					obj.put(key, value);
				}
				resList.add(obj);
			}
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return resList;
	}
	
	public static List<JSONObject> getComputedIndicators(List<Integer> presentIds) {
		List<JSONObject> indicators = new ArrayList<JSONObject>();
		Connection conn = null;
		try {

			
			String ids = " ";
			for (int i = 0; i<presentIds.size(); i++) {
				ids = ids + "ID="+presentIds.get(i);
				if(i <= presentIds.size()-2) {
					ids = ids + " OR ";
				}
			}
			
			
			
			conn = DBConnectionCalculated.createConnection();
			String query = "SELECT ID,NAME from SBI_KPI_KPI where ("+ids+")";
			logger.debug("query:" + query);
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery(query);
			indicators = getFormattedFilteredResult(rs); 			     
			conn.close();

		}
		catch(Exception e)
		{			
			e.printStackTrace();

		}
		return indicators;
		
	}
	
	public static List<Integer> getComputedIds(Integer category_city) {
		List<Integer> id = new ArrayList<Integer>();
		Connection conn = null;
		Integer i = 0;
		try {

			conn = DBConnectionCalculated.createConnection();
			String query = "SELECT ID from SBI_KPI_KPI where CATEGORY_ID="+category_city;
			//logger.debug("query:" + query);
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery(query);
			if (rs!=null) {
				while(rs.next()) {
					i = rs.getInt("ID");
					id.add(i);
				}
			}
			//indicators = getFormattedFilteredResult(rs); 			     
			conn.close();

		}
		catch(Exception e)
		{			
			e.printStackTrace();

		}
		return id;
		
	}
	
	public static List<Integer> verifyId(List<Integer> ids) {
		List<Integer> x = new ArrayList<Integer>();
		Connection conn = null;
		Integer i = -1;
		try {

			
			for (int j = 0; j<ids.size(); j++) {
				conn = DBConnection.createConnection();
				String query = "SELECT EXISTS(SELECT * FROM kpi WHERE id_knowage="+ids.get(j)+")";
				//logger.debug("query:" + query);
				Statement st = conn.createStatement();
				ResultSet rs = st.executeQuery(query);
				if (rs!=null) {
					while(rs.next()) {
						i= rs.getInt(1);
						if (i == 0) {
							x.add(ids.get(j)); //non c'è quindi aggiungilo
						}
						
						
						
				}
			}
			//indicators = getFormattedFilteredResult(rs); 			     
			conn.close();
			}

		}
		catch(Exception e)
		{			
			e.printStackTrace();

		}
		return x;
	}
	
	public static Integer getCategoryId (String city, String categoryType) {
		Integer categoryId = 0;
		
		Connection conn = null;
		
		try {
			conn = DBConnectionCalculated.createConnection();
			String query="SELECT `VALUE_ID` FROM `SBI_DOMAINS` WHERE VALUE_NM='"+city.toUpperCase()+"' AND DOMAIN_NM='"+categoryType+"'";
			//logger.info(query);
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery(query);
		      
		     if(rs!=null) {
		    	 	while(rs.next()) {
		    	 		categoryId = rs.getInt("VALUE_ID");
		    	 	}
		     }
		     
		    
		     
		     
		     
		      conn.close();
		}
		catch(Exception e)
		{			
			e.printStackTrace();
			
		}
		
		
		return categoryId;
	}

	
	
	public static Integer getLastValue(Integer numberId) {
		Integer lastValue = 0;
		Connection conn = null;
		
		try {
			conn = DBConnectionCalculated.createConnection();
			String query="SELECT `COMPUTED_VALUE`, MAX(`TIME_RUN`) FROM `SBI_KPI_VALUE` WHERE KPI_ID="+numberId;
			//logger.info(query);
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery(query);
		      
		     if(rs!=null) {
		    	 	while(rs.next()) {
		    	 		lastValue = rs.getInt("COMPUTED_VALUE");
		    	 	}
		     }
		     
		    
		     
		     
		     
		      conn.close();
		}
		catch(Exception e)
		{			
			e.printStackTrace();
			
		}
		
		
		return lastValue;
	}
	
	
	
}
