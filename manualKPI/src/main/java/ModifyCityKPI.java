package main.java;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import main.utils.GetCommonCategory;

/**
 * Servlet implementation class ModifyKPI
 */
@WebServlet("/ModifyCityKPI")
public class ModifyCityKPI extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger(ModifyCityKPI.class.getName());
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ModifyCityKPI() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//String tableId;
		String name;
		String desc;
		String unit;
		Integer idCity;
		String level;
		String category;
		Integer KPI_id;
		String idCityP;
		String KPI_idP;
		String isManual;
		String id_knowage;
		Integer id_knowageInt;
//		tableId = request.getParameter("tableId");
//		logger.debug(tableId);
		name = request.getParameter("name");
		logger.debug(name);
		isManual = request.getParameter("setComp");
		logger.debug("IS MANUAL: " + isManual);
		desc = request.getParameter("desc");
		logger.debug(desc);
		unit = request.getParameter("unit");
		logger.debug(unit);
		idCityP = request.getParameter("city");
		logger.debug(idCityP);
		level = request.getParameter("level");
		logger.debug(level);
		category = request.getParameter("category");
		logger.debug(category);
		KPI_idP = request.getParameter("KPI_id");
		id_knowage = request.getParameter("id_knowage");
		logger.debug("ID KNOWAGE: " + id_knowage);
		
		if(idCityP.equalsIgnoreCase("Eindhoven") || idCityP.equalsIgnoreCase("Genova") || idCityP.equalsIgnoreCase("Tampere")) {
			idCityP = GetCommonCategory.getIdCity(idCityP);
		}
		
		boolean presente;
		
		idCity = Integer.parseInt(idCityP);
		KPI_id = Integer.parseInt(KPI_idP);
		if(id_knowage != null && id_knowage != "") {
			id_knowageInt = Integer.parseInt(id_knowage);
		}
		else {
			id_knowageInt = null;
		}
		
		if(isManual != null) {
			isManual = "automatic";
			KPIDao.deleteCityKpi(KPI_id, idCity);
			KPIDao.addComputedKPI(KPI_idP, id_knowageInt, name, desc, unit, level, category, isManual);
		}
		else{
			isManual = "citymanual";
			//cancello il kpi dalla tabella kpi
			KPIDao.deleteKpi(KPI_id);
			//vedo se c'è già tra quelli manuali
		    presente = KPIDao.isPresentCityKpi(KPI_id, idCity);
			//se c'è, prende l'ultimo valore  ( e dovrebbe già farlo)
		    if (presente == false) {
		    	    KPIDao.addCityKPI(KPI_id, name, desc, unit, idCity, level, category, 0);
		    		//KPIDao.editCityKPIValue(idCity, KPI_id, 0);
		    }
		    id_knowageInt = null;
			//se non c'è fa una insert anche in input mettendo il valore zero di default
			
		}
		
		
		
		KPIDao.updateCityKPI(KPI_id, name, desc, unit,isManual,id_knowageInt,idCity);
		String nextJSP = "/cpm/GetKPIList?city="+idCity+"&level="+level+"&category="+category;
		response.sendRedirect(nextJSP);
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
