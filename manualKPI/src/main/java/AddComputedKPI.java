package main.java;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.JSONObject;

import main.bean.City;
import main.utils.SubscriptionManager;

/**
 * Servlet implementation class AddComputedKPI
 */
@WebServlet("/AddComputedKPI")
public class AddComputedKPI extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger(AddComputedKPI.class.getName());
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddComputedKPI() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String isManual= "automatic";
		String fiware_service=null;
		String fiware_servicepath = null;
		String createdOnUpdateKPISubscription = null;
		String createdKPISubscriptionEntity = null;
		String subscriptionIdCreated = null;
		JSONObject subscrIdj = null;
		
		float value = 0;
		
		Integer city;
		String idtable = request.getParameter("id");
		logger.debug("ID: "+ idtable);
		//aggiungere 
		String id_knowage = request.getParameter("id_knowage");
		
		String name = request.getParameter("name");
		logger.debug("name: " + name);
		String desc = request.getParameter("desc");
		logger.debug("description: " + desc);
		String unit = request.getParameter("unit");
		logger.debug("unit measure: " + unit);
		String cityP= request.getParameter ("city");
		logger.debug("city: " + cityP);
		String level = request.getParameter ("level");
		logger.debug("level: " + level);
		String category;
		city = Integer.parseInt(cityP);
		
		
		Integer numberId = Integer.parseInt(id_knowage);
		Integer lastValue = CalculatedKPIDao.getLastValue(numberId);
		//per assegnare un id mmi vado a fare una select su tutti gli id, prendo il più grande e l'id che assegnerò sarà il più grande +1. 
		//per assegnare un valore mi vado a prendere l'ultimo valore presente su knowage
		
		
		String valueS= lastValue.toString();
		
		ArrayList<City> cities = new ArrayList<City>();
		//String idS = id.toString();
		
		HashSet<String> notificationAttrs = new HashSet<String>();
		
		notificationAttrs.add("value");
		
	try {
		cities = KPIDao.getCities();
		for (City c : cities) {
			
		
			fiware_service = c.getName().toLowerCase();
			fiware_servicepath = "/"+c.getName().toLowerCase()+"_computedKPI";
			
			
				createdOnUpdateKPISubscription = SubscriptionManager.createDeviceSubscription(idtable, name, notificationAttrs, fiware_service, fiware_servicepath);
			
			
			// STEP 2: Get the subscriptionId
			
				subscrIdj = new JSONObject(createdOnUpdateKPISubscription);
			
				subscriptionIdCreated = subscrIdj.getString("subscriptionId");
			
			logger.info("Subscription Id Created: "+ subscriptionIdCreated);
			
			// STEP3: CREATE ENTITY WITH subscription_id and device_id
			
			
				createdKPISubscriptionEntity = SubscriptionManager.createDeviceSubscriptionEntity(idtable, subscriptionIdCreated, valueS, fiware_service, fiware_servicepath);
				logger.info("Created KPI Subscription Entity on Orion: " + createdKPISubscriptionEntity);
		
		}
		
	}
	catch(Exception e) {
		e.printStackTrace();
	}
		
		
		
//		if (city == 1) {
//			fiware_service = "genova";
//			fiware_servicepath="/genova_manualKPI";
//		}
//		else if (city == 2) {
//			fiware_service = "tampere";
//			fiware_servicepath="/tampere_manualKPI";
//		}
//		else if (city==3) {
//			fiware_service = "eindhoven";
//			fiware_servicepath="/eindhoven_manualKPI";
//		}
//		
		if (level.equalsIgnoreCase("task")) {
			category = request.getParameter("cat1");
		}
		else {
			category = request.getParameter("cat2");
		}
		
		
		
		
		logger.debug("category: " + category);
		KPIDao.addComputedKPI(idtable,numberId,name,desc,unit,level,category,isManual);
		String nextJSP = "/cpm/GetKPIList?city="+city+"&level="+level+"&category="+category;
		response.sendRedirect(nextJSP);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
