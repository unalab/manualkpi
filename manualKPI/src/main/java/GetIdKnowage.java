package main.java;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;



/**
 * Servlet implementation class GetIdKnowage
 */
@WebServlet("/GetIdKnowage")
public class GetIdKnowage extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger(GetIdKnowage.class.getName());
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetIdKnowage() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String idKpi = request.getParameter("idkpi");
		logger.debug("ID KPI: " + idKpi);
		Integer id = Integer.parseInt(idKpi);
		Integer idK = KPIDao.getIdKnowageByidKpi(id);
		logger.debug("ID KNOWAGE: " + idK);
		String s = idK.toString();
		response.getWriter().write(s);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
