package main.java;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import main.utils.GetCommonCategory;

/**
 * Servlet implementation class ChoosePilot
 */
@WebServlet("/ChoosePilot")
public class ChoosePilot extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger(ChoosePilot.class.getName());
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ChoosePilot() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String pilot;
		Integer chosenPilot = 0;
		String level = "task";
		String category = "all";
		
		
		ArrayList<KPIBean> kpiInitialList = new ArrayList<KPIBean>();
		ArrayList<KPIBean> kpicomputedList = null;
		ArrayList<KPIBean> intermediate = null;
		ArrayList<KPIBean> results = new ArrayList<KPIBean>();
		ArrayList<Integer> ids = null;
		
		pilot = request.getParameter("city");
		logger.debug("selected pilot: " + pilot);
		if (pilot.equalsIgnoreCase("Eindhoven") || pilot.equalsIgnoreCase("3"))
		{
			chosenPilot = 3;
		}
		else if (pilot.equalsIgnoreCase("Genova") || pilot.equalsIgnoreCase("1")) {
			chosenPilot = 1;
		}
		else if (pilot.equalsIgnoreCase("Tampere") || pilot.equalsIgnoreCase("2")) {
			
			chosenPilot = 2;
		}
		
		logger.debug("id selected pilot: "+ chosenPilot);
		kpiInitialList = KPIDao.getKPI(level, category, chosenPilot);
		//String cityS = GetCommonCategory.getCity(city);
		Integer category_city = CalculatedKPIDao.getCategoryId(pilot.toUpperCase(), "KPI_KPI_CATEGORY");
		logger.debug("category CITY: " + category_city);
		intermediate = CalculatedKPIDao.getComputedKPI(level, category, chosenPilot);
		ids = CalculatedKPIDao.getIds(intermediate, category_city);
		kpicomputedList = CalculatedKPIDao.getComputedValues(intermediate, ids);
		results.addAll(kpiInitialList);
		if(kpicomputedList!= null) {
		results.addAll(kpicomputedList);
		}
		request.setAttribute("kpiList", results);
		String nextJsp = "commonKPI.jsp?city="+chosenPilot;
		//String nextJsp= "view.jsp?city="+chosenPilot+"&level="+level+"&category="+category;
		request.getRequestDispatcher(nextJsp).forward(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//logger.debug("sto per chiamare la doGet");
		doGet(request, response);
	}

}
