package main.java;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import main.utils.*;


import main.java.CalculatedKPIDao;


@WebServlet("/GetCalculatedIndicators")
public class GetCalculatedIndicators extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static Logger logger = Logger.getLogger(GetCalculatedIndicators.class.getName());
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetCalculatedIndicators() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 String city_id = request.getParameter("city");
		 Integer category_id;
		 String city;
		 logger.debug("CITY_ID FROM AJAX: " + city_id);
		 if(city_id.equalsIgnoreCase("genova") || city_id.equalsIgnoreCase("eindhoven") || city_id.equalsIgnoreCase("tampere")) {
			 category_id = CalculatedKPIDao.getCategoryId(city_id.toUpperCase(), "KPI_KPI_CATEGORY");
		 }
		 
		 else {
		   city = GetCommonCategory.getCity(city_id);
		   category_id = CalculatedKPIDao.getCategoryId(city.toUpperCase(), "KPI_KPI_CATEGORY");
		 }
		//mi prendo gli id knowage per una città
		 
		 List<Integer> idInKnowage = CalculatedKPIDao.getComputedIds(category_id);
		 for(int i = 0; i< idInKnowage.size(); i++)
			 logger.debug("Id in KNOWAGE: " + idInKnowage.get(i));
		 
		 
		 //per ogni id vado a vedere se è già censito nella lista dei kpi automatici della città.
		 // se è presente lo ignoro, altrimenti lo aggiungo alla lista degli indicatori validi
		 
		 List<Integer> presenti = CalculatedKPIDao.verifyId(idInKnowage);
		 
         List<JSONObject> filters = new ArrayList<JSONObject>();
         if(presenti.size()>0) {
        	 filters = CalculatedKPIDao.getComputedIndicators(presenti);
         }
		 
		
		 Gson gson = new Gson();
		 String jsonString;
		 
		 if(filters.isEmpty()) {
			 jsonString = null;
		 }
		 else {
			 jsonString = gson.toJson(filters);
		 }
		 jsonString = jsonString.replace("{\"map\":", "");
		 jsonString = jsonString.replace("}}", "}"); 
		 jsonString = jsonString.replace(",{}", "");
		 jsonString = jsonString.replace("{},", "");
		 
		 
		 
		 logger.debug("INDICATORS TABLE: "+ jsonString);
		 response.getWriter().write(jsonString);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

}
