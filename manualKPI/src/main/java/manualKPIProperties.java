package main.java;

public enum manualKPIProperties {
	DB_HOST("DB_HOST"),
	DB_HOST_CALCULATED("DB_HOST_CALCULATED"),
	DB_NAME("DB_NAME"),
	DB_NAME_CALCULATED("DB_NAME_CALCULATED"),
	DB_USERNAME("DB_USERNAME"),
	DB_PASSWORD("DB_PASSWORD"),
	OrionProtocol("orion.protocol"),
	OrionHost("orion.host"),
	OrionSub("orion.v2.subscriptions"),
	OrionTrust("orion.iot.trust"),
	OrionService("orion.headers.service"),
	OrionServicePath("orion.headers.servicepath"),
	OrionEnt("orion.v2.entities"),
	OrionContext("orion.v1.updateContext"),
	OrionGetType("orion.get.content.type"),
	OrionDeleteType("orion.delete.content.type"),
	MapZoom("orion.mapcenter.zoom"),
	LimitSearch("orion.limit.search"),
	DmeProtocol("dme.protocol"),
	DmeHost("dme.host"),
	DmeMashupGenova("dme.invoke.mashup.genova"),
	DmeMashupEindhoven("dme.invoke.mashup.eindhoven"),
	DmeMashupTampere("dme.invoke.mashup.tampere");
	
	
	private final String text;

	private manualKPIProperties(final String text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return text;
	}

}
