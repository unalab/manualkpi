package main.java;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

/**
 * Servlet implementation class GetValue
 */
@WebServlet("/GetValue")
public class GetValue extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger(GetValue.class.getName());
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetValue() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String tableID = request.getParameter("id");
		logger.debug("TABLE ID: " + tableID);
		Integer id = KPIDao.getSingleIdKnowage(tableID);
		logger.debug("KNOWAGE ID: " + id);
		String name = KPIDao.getValue(id);
		
		logger.debug("NAME: " + name);
		response.getWriter().write(name);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
