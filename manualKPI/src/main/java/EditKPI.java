package main.java;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import main.utils.SubscriptionManager;




@WebServlet("/EditKPI")
public class EditKPI extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger(EditKPI.class.getName());
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditKPI() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		float newValue;
		Integer idCity;
		Integer idKPI;
		String idCityP;
		String idKPIP;
		String newValueP;
		String level;
		String category;
		String updatedEntity;
		String idEntity;
		String nameKPI;
		String tableId;
		String motivation;
		
		tableId= request.getParameter("tableId");
		nameKPI = request.getParameter("title");
		logger.debug("nome KPI: " + nameKPI);
		newValueP = request.getParameter("value");
		motivation = request.getParameter("motivation");
		idCityP = request.getParameter("city");
		//logger.debug("city: " + idCity);
		idKPIP = request.getParameter("KPI_id");
		logger.debug("id KPI: "+idKPIP);
		level = request.getParameter("level");
		category = request.getParameter("category");
		logger.debug("category:" + category);
		newValue = Float.parseFloat(newValueP);
		idCity = Integer.parseInt(idCityP);
		idKPI = Integer.parseInt(idKPIP);
		nameKPI = nameKPI.replaceAll("\\s+","");
		nameKPI = nameKPI.toLowerCase();
		
		String fiware_service = null;
		String fiware_servicepath = null;
		if (idCity == 1) {
			fiware_service = "genova";
			fiware_servicepath="/genova_manualKPI";
		}
		else if (idCity == 2) {
			fiware_service = "tampere";
			fiware_servicepath="/tampere_manualKPI";
		}
		else if (idCity==3) {
			fiware_service = "eindhoven";
			fiware_servicepath="/eindhoven_manualKPI";
		}
		
		idEntity = tableId+"_"+tableId+"_"+nameKPI;
		//aggiorno entità su Orion 
		updatedEntity = SubscriptionManager.updateEntity(idEntity,newValueP,fiware_service,fiware_servicepath);
		//String nextJSP;
		
		KPIDao.setValue(idCity,idKPI,newValue,motivation);
			
		
		String nextJSP = "/cpm/GetKPIList?city="+idCity+"&level="+level+"&category="+category;
		response.sendRedirect(nextJSP);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
