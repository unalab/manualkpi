package main.java;


import java.io.File;
import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 * Servlet implementation class Log4JInitServlet
 */
@WebServlet(
		urlPatterns = "/Log4jInitServlet",
			initParams=
		{
				@WebInitParam(name="log4j-properties-location", value= "/WEB-INF/log4j.properties")
		},
		loadOnStartup = 1
		)
public class Log4JInitServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static Logger logger = Logger.getLogger(Log4JInitServlet.class.getName());
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public void init (ServletConfig config) throws ServletException {
    	//System.out.println("Log4JInitServlet sta inizializzando log4j");
    	String log4jLocation = config.getInitParameter("log4j-properties-location");
    	ServletContext sc = config.getServletContext();
    	String webappPath = sc.getRealPath("/");
    	String log4jProp = webappPath + log4jLocation;
    	File fileDiPropLog4j = new File (log4jProp);
    	if (fileDiPropLog4j.exists()) {
    		PropertyConfigurator.configure(log4jProp);
    	}
    	
    	else {
    		BasicConfigurator.configure();
    	}
    	
    	super.init(config);
    }

}
