package main.java;

//import java.sql.Timestamp;

public class KPIBean {

	private Integer idKPI;
	private Integer idCity;
	private String name;
	private String description;
	private float value;
	private String unit;
	private String date;
	private String kpiLevel;
	private String kpiCategory;
	private Integer tableId;
	private String humanCategory;
	private String icon;
	private Integer idKnowage;
	private String isManual;
	private Integer knowageCity;
	private String motivation;
	
	public void setKnowageCity (Integer knowageCity) {
		this.knowageCity = knowageCity;
	}
	
	public Integer getKnowageCity() {
		return knowageCity;
	}
	
	public void setIdKnowage(Integer idKnowage) {
		this.idKnowage = idKnowage;
	}
	
	public Integer getIdKnowage () {
		return idKnowage;
	}
	
	public void setIcon(String icon) {
		this.icon = icon;
	}
	
	public String getIcon() {
		return icon;
	}
	
	public void setIdKPI(Integer idKPI) {
		this.idKPI = idKPI;
	}
	
	public void setIdCity(Integer idCity) {
		this.idCity = idCity;
	}
	
	public void setTableId(Integer tableId) {
		this.tableId = tableId;
	}
	
	public void setName(String name) {
		// TODO Auto-generated method stub
		this.name = name;
	}

	public void setDescription(String description) {
		// TODO Auto-generated method stub
		this.description = description;
	}
	
	public void setMotivation(String motivation) {
		// TODO Auto-generated method stub
		this.motivation = motivation;
	}

	public void setValue(float value) {
		// TODO Auto-generated method stub
		this.value = value;
	}
	
	public void setUnit(String unit) {
		// TODO Auto-generated method stub
		this.unit = unit;
	}
	
	public void setDate(String date) {
		// TODO Auto-generated method stub
		this.date = date;
	}
	
	public void setKpiLevel(String kpiLevel) {
		this.kpiLevel = kpiLevel;
	}
	
	public void setKpiCategory(String kpiCategory) {
		this.kpiCategory = kpiCategory;
	}
	
	public String getName() {
		// TODO Auto-generated method stub
		return name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public String getMotivation() {
		return motivation;
	}
	
	public float getValue() {
		return value;
	}
	
	public String getUnit() {
		return unit;
	}
	
	public String getDate() {
		return date;
	}
	
	public String getKpiLevel () {
		return kpiLevel;
	}
	
	public String getKpiCategory() {
		return kpiCategory;
	}
	
	public Integer getIdKPI () {
		return idKPI;
	}
	
	public Integer getIdCity() {
		return idCity;
	}
	
	public Integer getTableId() {
		return tableId;
	}
	
	public String getHumanCategory() {
		return humanCategory;
	}
	
	public void setHumanCategory(String humanCategory) {
		this.humanCategory = humanCategory;
	}
	
	public void setIsManual (String isManual) {
		this.isManual = isManual;
	}
	
	public String getIsManual() {
		return isManual;
	}
	
}
