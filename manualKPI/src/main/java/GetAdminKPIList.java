package main.java;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import main.utils.GetCommonCategory;



/**
 * Servlet implementation class GetKPIList
 */
@WebServlet("/GetAdminKPIList")
public class GetAdminKPIList extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger(GetAdminKPIList.class.getName());
	
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetAdminKPIList() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		ArrayList<KPIBean> kpiList = null;
//		ArrayList<KPIBean> kpicomputedList = null;
//		ArrayList<KPIBean> intermediate = null;
		ArrayList<KPIBean> results = new ArrayList<KPIBean>();
//		ArrayList<Integer> ids = null;
		String level = request.getParameter("one");
		
		
		logger.debug(level);
		String category;
		String city = request.getParameter("city");
		logger.debug("selected pilot: " + city);
		Integer id_city = Integer.parseInt(city);
		if (level == null) {
			level = request.getParameter("level");
			category=request.getParameter("category");
			logger.debug("category post edit:" + category); 
		}
		else {
			if (level.equalsIgnoreCase("task")) {
				category = request.getParameter("two");
				logger.debug("category post edit:" + category); 
			}
			else{
				category = request.getParameter("third"); 
				logger.debug("category post edit:" + category); 
			}
		
		}
		
		kpiList = KPIDao.getKPI(level, category, id_city);
//		String cityS = GetCommonCategory.getCity(city);
//		Integer category_city = CalculatedKPIDao.getCategoryId(cityS.toUpperCase(), "KPI_KPI_CATEGORY");
//		intermediate = CalculatedKPIDao.getComputedKPI(level, category, id_city);
//		ids = CalculatedKPIDao.getIds(intermediate, category_city);
//		
//		kpicomputedList = CalculatedKPIDao.getComputedValues(intermediate, ids);
		
		 
		
		
		//logger.debug("kpilist");
		results.addAll(kpiList);
//		if(kpicomputedList!= null) {
//			results.addAll(kpicomputedList);
//			}
		request.setAttribute("kpiList", results);
		String nextJsp= "admin.jsp?city="+id_city+"&level="+level+"&category="+category;
		logger.debug("nextJSP: " + nextJsp);
		request.getRequestDispatcher(nextJsp).forward(request,response);
		
}
		
		
		
		
//		
		
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
