package main.java;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.logging.Level;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import main.bean.City;
import main.utils.SubscriptionManager;

/**
 * Servlet implementation class AddKPI
 */
@WebServlet("/AddAdminKPI")
public class AddAdminKPI extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger(AddAdminKPI.class.getName());
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddAdminKPI() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String isManual= "manual";
		String fiware_service=null;
		String fiware_servicepath = null;
		String createdOnUpdateKPISubscription = null;
		String createdKPISubscriptionEntity = null;
		String subscriptionIdCreated = null;
		JSONObject subscrIdj = null;
		
		float value = 0;
		Integer id; 
		//Integer city;
		String idP = request.getParameter("id");
		String name = request.getParameter("name");
		logger.debug("name: " + name);
		String desc = request.getParameter("desc");
		logger.debug("description: " + desc);
		String unit = request.getParameter("unit");
		logger.debug("unit measure: " + unit);
		//String cityP= request.getParameter ("city");
		//logger.debug("city: " + cityP);
		String level = request.getParameter ("level");
		logger.debug("level: " + level);
		String category;
		//city = Integer.parseInt(cityP);
		id = Integer.parseInt(idP);
		String valueS="0";
		
		ArrayList<City> cities = new ArrayList<City>();
		
		
		HashSet<String> notificationAttrs = new HashSet<String>();
		
		notificationAttrs.add("value");
		
	try {
		cities = KPIDao.getCities();
		for (City c : cities) {
			
		
			fiware_service = c.getName().toLowerCase();
			fiware_servicepath = "/"+c.getName().toLowerCase()+"_manualKPI";
			
			
				createdOnUpdateKPISubscription = SubscriptionManager.createDeviceSubscription(idP, name, notificationAttrs, fiware_service, fiware_servicepath);
			
			
			// STEP 2: Get the subscriptionId
			
				subscrIdj = new JSONObject(createdOnUpdateKPISubscription);
			
				subscriptionIdCreated = subscrIdj.getString("subscriptionId");
			
			logger.info("Subscription Id Created: "+ subscriptionIdCreated);
			
			// STEP3: CREATE ENTITY WITH subscription_id and device_id
			
			
				createdKPISubscriptionEntity = SubscriptionManager.createDeviceSubscriptionEntity(idP, subscriptionIdCreated, valueS, fiware_service, fiware_servicepath);
				logger.info("Created KPI Subscription Entity on Orion: " + createdKPISubscriptionEntity);
		
		}
		
	}
	catch(Exception e) {
		e.printStackTrace();
	}
		
		
		
//		if (city == 1) {
//			fiware_service = "genova";
//			fiware_servicepath="/genova_manualKPI";
//		}
//		else if (city == 2) {
//			fiware_service = "tampere";
//			fiware_servicepath="/tampere_manualKPI";
//		}
//		else if (city==3) {
//			fiware_service = "eindhoven";
//			fiware_servicepath="/eindhoven_manualKPI";
//		}
//		
		if (level.equalsIgnoreCase("task")) {
			category = request.getParameter("cat1");
		}
		else {
			category = request.getParameter("cat2");
		}
		
		
		
		
		logger.debug("category: " + category);
		KPIDao.addKPI(id,name,desc,unit,3,level,category,isManual,value);
		String nextJSP = "/cpm/GetAdminKPIList?city=3&level="+level+"&category="+category;
		response.sendRedirect(nextJSP);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
