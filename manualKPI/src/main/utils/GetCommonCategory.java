package main.utils;

import org.apache.log4j.Logger;



public class GetCommonCategory {

	static Logger logger = Logger.getLogger(GetCommonCategory.class.getName());
	public String getCategory(String c) {
		String category;
		switch(c) {
		case "climate":
			category = "climate change adaptation and mitigation";
			break;
		case "airquality":
			category = "air quality";
			break;
		case "economic":
			category = "economic opportunities and green jobs";
			break;
		case "water":
			category = "water management";
			break;
		case "green":
			default:
			category = "green space management";
			break;
		}
			
		
		return category;
		
		
	}
	
	public static String getCity(String city_id) {
		String city;
		logger.debug("city_id: " + city_id);
		switch(city_id) {
		case "1": 
			city = "Genova";
			break;
		case "2":
			city = "Tampere";
			break;
		case "3":
		default:
			city = "Eindhoven";
			break;
		}
		
		return city;
	}
	
	public static String getIdCity(String city) {
		String city_id;
		
		switch(city) {
		case "Genova": 
			city_id = "1";
			break;
		case "Tampere":
			city_id = "2";
			break;
		case "Eindhoven":
		default:
			city_id = "3";
			break;
		}
		
		return city_id;
	}
}
