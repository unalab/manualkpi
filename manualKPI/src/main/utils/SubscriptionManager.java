package main.utils;



import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.*;

import javax.ws.rs.core.MediaType;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;



import main.bean.SubcriptionSubjectEntity;
import main.bean.Subscription;
import main.bean.SubscriptionNotification;
import main.bean.SubscriptionNotificationHttpCustom;
import main.bean.SubscriptionSubject;
import main.bean.SubscriptionSubjectCondition;
import main.bean.SubscriptionSubjectConditionExpression;
import main.bean.UpdatedEntity;
import main.java.PropertyManager;
import main.java.manualKPIProperties;
import main.tools.Orion;
import main.bean.DeviceSubscriptionEntity;
import main.bean.EntityAttribute;


public abstract class SubscriptionManager {
	
	private static final Logger LOGGER = Logger.getLogger(SubscriptionManager.class.getName() );

	static String orionHostURL = PropertyManager.getProperty(manualKPIProperties.OrionProtocol) + PropertyManager.getProperty(manualKPIProperties.OrionHost);
	static String orionEntity = PropertyManager.getProperty(manualKPIProperties.OrionEnt);
	static String orionSubscription = PropertyManager.getProperty(manualKPIProperties.OrionSub);
	static String notificationHttpURL = "http://localhost:8080/cpm";

	// Type of expression to define the conditions in the subscription
	static String expressionTypeQ = "q";
	static String expressionTypeMq = "mq";
	static String expressionTypeGeorel = "georel";
	static String expressionTypeGeometri = "geometri";
	static String expressionTypeCoords = "coords";


	private static Orion orion;

	static{
		try{ 
			orion = new Orion();
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

	/*
	 * Create a new Subscription on orion Context Broker 
	 * METHOD: POST
	 * ENDPOINT: 217.172.12.177:1026/v2/subscriptions/
	 * 
	 * HEADERS:
	 * fiware-service: parameter
	 * fiware-servicepath: /parameter
	 * 
		{
		"id": "5975a8ad2d0975a576f6680e",
		"description": "OnDelete Device Subscription",
		"status": "active",
		"subject": {
			"entities": [{
				"idPattern": ".*",
				"type": "Device"
			}],
			"condition": {
				"attrs": [
					"opType"
				],
				"expression": {
					"q": "opType==deleted"
				}
			}
		},
		"notification": {
			"timesSent": 2,
			"lastNotification": "2017-07-24T09:21:11.00Z",
			"attrs": [],
			"attrsFormat": "normalized",
			"httpCustom": {
				"url": "http://217.172.12.224/dme/orion/device/delete/tourism",
				"headers": {
					"contextService": "bologna_1",
					"contextServicePath": "/bologna_1_tourism/harmonized",
					"typeOut": "ParkingSpot"
				}
			}
		}
	}
	 */

//	public static String createOnDeleteDeviceSubscription(HashSet<String> notificationAttrs, String headerService, String headerServicePath) throws Exception {
//		LOGGER.log(Level.INFO, "Create Subscription on ORION CONTEXT BROKER");
//
//		// Composing the subscriptionBean with the client parameters
//
//		JSONObject jResp = new JSONObject();
//		String serviceResponse = "";
//		try {
//
//			String endpoint = orionHostURL+ orionSubscription;
//			LOGGER.log(Level.INFO, "Invoking " + endpoint);
//
//			Subscription subscription = new Subscription();
//			// Create the subscription
//
//			// Description
//			subscription.setDescription("OnDelete Device Subscription");
//			//subscription.setThrottling(5);
//
//			// Status
//			subscription.setStatus("active");
//
//			//subjectEntity
//			SubcriptionSubjectEntity subjectEntity = new SubcriptionSubjectEntity();
//			String idPattern = ".*";
//			String type = "Device";
//			subjectEntity.setIdPattern(idPattern);
//			subjectEntity.setType(type);
//
//			Set<SubcriptionSubjectEntity> setSubcriptionSubjectEntity = new HashSet<SubcriptionSubjectEntity>();
//			setSubcriptionSubjectEntity.add(subjectEntity);
//
//			SubscriptionSubject subject= new SubscriptionSubject();
//			subject.setEntities(setSubcriptionSubjectEntity);
//
//			//subjectCondition - conditionAttrs
//			SubscriptionSubjectCondition subjectCondition = new SubscriptionSubjectCondition();
//			HashSet<String> conditionAttrs = new HashSet<String>();
//			conditionAttrs.add("opType");
//			subjectCondition.setAttrs(conditionAttrs);
//
//			//subjectCondition - expression
//			SubscriptionSubjectConditionExpression expression = new SubscriptionSubjectConditionExpression();
//			String conditionExpression = "opType==deleted";
//			expression.setQ(conditionExpression);
//			subjectCondition.setExpression(expression);
//			subject.setCondition(subjectCondition);
//			LOGGER.log(Level.INFO, "subjectCondition: " + subjectCondition);
//
//			subscription.setSubject(subject);
//
//			// notification
//			SubscriptionNotification subscriptionNotification = new SubscriptionNotification();
//
//			subscriptionNotification.setTimesSent(2);
//			String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
//			subscriptionNotification.setLastNotification(timeStamp);
//
//			// notification - attrs 
//			subscriptionNotification.setAttrs(notificationAttrs);
//
//			subscriptionNotification.setAttrsFormat("normalized");
//
//			SubscriptionNotificationHttpCustom httpCustom = new SubscriptionNotificationHttpCustom();
//			String notificationOrionUrl = notificationOrionDmeOnDeviceDelete;
//			httpCustom.setUrl(notificationOrionUrl);
//
////			// httpCustom headers
////			SubscriptionNotificationHttpCustomHeaders headers = new SubscriptionNotificationHttpCustomHeaders();
////			
////			// CUSTOM HARMONIZED CONTEXT FOR TRENTO AND MALAGA
////			if (headerService.contains("rento")){
////				headers.setContextService(harmonizedServiceMalaga);
////				headers.setContextServicePath(harmonizedServicepathMalaga);
////				headers.setTypeOut(dmeTypeOutParkingSpot);
////			}else if (headerService.contains("alaga")){
////				headers.setContextService(harmonizedServiceTrento);
////				headers.setContextServicePath(harmonizedServicepathTrento);
////				headers.setTypeOut(dmeTypeOutParkingSpot);
////			} else {
////				headers.setContextService(headerService);
////				headers.setContextServicePath(headerServicePath);
////				headers.setTypeOut(null);
////				headers.setContextUrl(orionHostURL+ orionEntity);
////			}
////
////			httpCustom.setHeaders(headers);
//
//			subscriptionNotification.setHttpCustom(httpCustom);	
//
//			subscription.setNotification(subscriptionNotification);
//
//			// Transform the bean into json
//			String subscriptionJson = beanToJson(subscription);
//			LOGGER.log(Level.INFO, "\tSubscriptionJson" + subscriptionJson);
//
//			Map<String, String> configHeaders = new HashMap<String, String>();
//			configHeaders.put("fiware-service", headerService);
//			configHeaders.put("fiware-servicepath", headerServicePath);
//			LOGGER.log(Level.INFO, "\theConfigHaders" + configHeaders);
//
//			LOGGER.log(Level.INFO, "\tbody: " + subscriptionJson);
//
//			serviceResponse = RestUtils.consumePost(endpoint, subscriptionJson, configHeaders);
//
//			LOGGER.log(Level.INFO, "Create Subscription on Orion: " + serviceResponse);
//
//			try {
//				jResp = new JSONObject(serviceResponse);
//			} catch (org.json.JSONException jsone) {
//				jResp = new JSONObject();
//				jResp.put("message", serviceResponse);
//			}
//
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//		return serviceResponse;
//	}

	/*
	 * Create a new Subscription on orion Context Broker 
	 * METHOD: POST
	 * ENDPOINT: 217.172.12.177:1026/v2/subscriptions/
	 * 
	 * HEADERS:
	 * fiware-service: parameter
	 * fiware-servicepath: /parameter
	 * 
	 * {
	"id": "infopoint001595cf8544f2k",
	"description": "onUpdateDevice Subscription",
	"status": "active",
	"subject": {
		"entities": [{
			"id": "infopoint001",
			"type": "Device"
		}],
      "condition": {
                  "attrs": ["opType"],
                  "expression": {
                      "q": "opType==ready"
                  }
              }
	},
	"notification": {
		"timesSent": 2,
		"lastNotification": "2017-07-11T14:07:12.00Z",
		"attrs": [],
		"attrsFormat": "normalized",
		"httpCustom": {
			"url": "http://217.172.12.224/dme/orion/device/update/{mashupId}",
			"headers": {
				"contextService": "bologna_1",
				"contextServicePath": "/bologna_1_tourism",
				"typeOut": "ParkingSpot"
			}
		}
	}
}
	 */
	
	public static String createDeviceSubscription(String kpiId, String name, HashSet<String> notificationAttrs, String headerService, String headerServicePath) throws Exception {
		LOGGER.log(Level.INFO, "Create Subscription onKPI on ORION CONTEXT BROKER");
		// Composing the subscriptionBean with the client parameters

		JSONObject jResp = new JSONObject();
		String serviceResponse = "";
		try {

			String endpoint = orionHostURL+ orionSubscription;
			LOGGER.log(Level.INFO, "Invoking " + endpoint);

			Subscription subscription = new Subscription();
			// Create the subscription
			//subscription.setId();
			// Description
			subscription.setDescription("Subscprition onKPI " + kpiId);

			// Status
			subscription.setStatus("active");

			//subjectEntity
			SubcriptionSubjectEntity subjectEntity = new SubcriptionSubjectEntity();
			//String id = kpiId;
			String type = "KPI";
			subjectEntity.setId(kpiId);
			subjectEntity.setType(type);

			Set<SubcriptionSubjectEntity> setSubcriptionSubjectEntity = new HashSet<SubcriptionSubjectEntity>();
			setSubcriptionSubjectEntity.add(subjectEntity);

			SubscriptionSubject subject= new SubscriptionSubject();
			subject.setEntities(setSubcriptionSubjectEntity);

			//subjectCondition - conditionAttrs
			SubscriptionSubjectCondition subjectCondition = new SubscriptionSubjectCondition();
			HashSet<String> conditionAttrs = new HashSet<String>();
			conditionAttrs.add("dateModified");
			subjectCondition.setAttrs(conditionAttrs);

			//subjectCondition - expression
			SubscriptionSubjectConditionExpression expression = new SubscriptionSubjectConditionExpression();
			String conditionExpression = "opType==ready,deleted"; 
			expression.setQ(conditionExpression);
			subjectCondition.setExpression(expression);
			subject.setCondition(subjectCondition);
			LOGGER.log(Level.INFO, "subjectCondition: " + subjectCondition);

			subscription.setSubject(subject);

			// notification
			SubscriptionNotification subscriptionNotification = new SubscriptionNotification();

			subscriptionNotification.setTimesSent(2);
			String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
			subscriptionNotification.setLastNotification(timeStamp);

			// notification - attrs 
			subscriptionNotification.setAttrs(notificationAttrs);

			subscriptionNotification.setAttrsFormat("normalized");
			

			SubscriptionNotificationHttpCustom httpCustom = new SubscriptionNotificationHttpCustom();
			httpCustom.setUrl("http://localhost:8080/cpm");

			httpCustom.putHeader("contextUrl", orion.getBaseUrl()+orion.getPathEntities());
			
			subscriptionNotification.setHttpCustom(httpCustom);
			
			subscription.setNotification(subscriptionNotification);

			// Transform the bean into json
			String subscriptionJson = beanToJson(subscription);
			LOGGER.log(Level.INFO, "\tSubscriptionJson" + subscriptionJson);

			Map<String, String> configHeaders = new HashMap<String, String>();
			configHeaders.put("fiware-service", headerService);
			configHeaders.put("fiware-servicepath", headerServicePath);
			LOGGER.log(Level.INFO, "\theConfigHaders" + configHeaders);

			LOGGER.log(Level.INFO, "\tbody: " + subscriptionJson);

			ClientResponse clientResponse = RestUtils.consumePostFull(endpoint, subscriptionJson, configHeaders);

			//String location = clientResponse.getHeaders().getFirst("Location");
			name = name.replaceAll("\\s+","");
			name = name.toLowerCase();
			String subscriptionId = kpiId + "_" + name;
					//location.substring(location.lastIndexOf("/")+1, location.length());
			LOGGER.log(Level.INFO, "Subscription " + subscriptionId + " successfully created!");

			serviceResponse = clientResponse.getEntity(String.class);

			try {
				jResp = new JSONObject(serviceResponse);
				jResp.put("subscriptionId", subscriptionId);
			} catch (org.json.JSONException jsone) {
				jResp = new JSONObject();
				jResp.put("message", serviceResponse);
				jResp.put("subscriptionId", subscriptionId);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		//return serviceResponse;
		return jResp.toString();
	}



	public static String createSubscription_NOTUSED(String subscriptionEvent,HashSet<String> conditionAttrs, String conditionExpressionType, String conditionExpression ,HashSet<String> notificationAttrs, String entityType, String headerService, String headerServicePath) throws Exception {
		LOGGER.log(Level.INFO, "Create Subscription on ORION CONTEXT BROKER");
		// COMPONGO IL BEAN subscriptionBean con i parametri che mi arrivano dal client

		JSONObject jResp = new JSONObject();
		String serviceResponse = "";
		try {

			String endpoint = orionHostURL+ orionSubscription;
			LOGGER.log(Level.INFO, "Invoking " + endpoint);

			Subscription subscription = new Subscription();
			// Create the subscription

			// Description
			subscription.setDescription("TEST IoTManager subscription");
			subscription.setThrottling(5);


			//subject
			SubcriptionSubjectEntity subjectEntity = new SubcriptionSubjectEntity();
			String idPattern = ".*";
			subjectEntity.setIdPattern(idPattern);
			subjectEntity.setType(entityType);

			Set<SubcriptionSubjectEntity> setSubcriptionSubjectEntityBean = new HashSet<SubcriptionSubjectEntity>();
			setSubcriptionSubjectEntityBean.add(subjectEntity);

			SubscriptionSubject subject= new SubscriptionSubject();
			subject.setEntities(setSubcriptionSubjectEntityBean);

			/*
			SubscriptionSubjectConditionBean subjectCondition = null;
			if (conditionExpressionType != null && conditionExpression != null) {
				SubscriptionSubjectConditionExpressionQ subjectConditionQ = new SubscriptionSubjectConditionExpressionQ();
				if (conditionExpressionType.equalsIgnoreCase(expressionTypeQ)) {
					subjectConditionQ.setQ(conditionExpression);
					subjectConditionQ.setAttrs(conditionAttrs);

					subject.setCondition(subjectConditionQ);
					LOGGER.log(Level.INFO, "subjectCondition: " + subjectConditionQ);

				}
			} else {
				subjectCondition = new SubscriptionSubjectConditionBean(conditionAttrs);
				subject.setCondition(subjectCondition);
			}
			//subjectCondition.setAttrs(conditionAttrs);
			 */

			/* {
			        "Description": "test",
			        "subject": {
			                "entities": [{
			                        "idPattern": ".*",
			                        "type": "air"
			                }],
			                "condition": {
			                        "attrs": ["CO2"],
			                        "expression": {
										"q": "temperature > 40"
										}
			                }
			        },
			        "notification": {
			                "http": {
			                        "url": "http://192.168.150.15:5050/notify"
			                },
			        "attrsFormat": "legacy",
			        "attrs": ["CO2"]
			        }
			}
			 */

			SubscriptionSubjectCondition subjectCondition = new SubscriptionSubjectCondition();
			subjectCondition.setAttrs(conditionAttrs);

			if (conditionExpressionType != null && conditionExpression != null) {
				SubscriptionSubjectConditionExpression expression = new SubscriptionSubjectConditionExpression();
				if (conditionExpressionType.equalsIgnoreCase(expressionTypeQ)) {
					expression.setQ(conditionExpression);
					subjectCondition.setExpression(expression);
					subject.setCondition(subjectCondition);
					LOGGER.log(Level.INFO, "subjectCondition: " + subjectCondition);
				}
				if (conditionExpressionType.equalsIgnoreCase(expressionTypeMq)) {
					expression.setMq(conditionExpression);
					subjectCondition.setExpression(expression);
					subject.setCondition(subjectCondition);
					LOGGER.log(Level.INFO, "subjectCondition: " + subjectCondition);
				}
				if (conditionExpressionType.equalsIgnoreCase(expressionTypeGeometri)) {
					expression.setGeometry(conditionExpression);
					subjectCondition.setExpression(expression);
					subject.setCondition(subjectCondition);
					LOGGER.log(Level.INFO, "subjectCondition: " + subjectCondition);
				}
				if (conditionExpressionType.equalsIgnoreCase(expressionTypeGeorel)) {
					expression.setGeorel(conditionExpression);
					subjectCondition.setExpression(expression);
					subject.setCondition(subjectCondition);
					LOGGER.log(Level.INFO, "subjectCondition: " + subjectCondition);
				}
				if (conditionExpressionType.equalsIgnoreCase(expressionTypeCoords)) {
					expression.setCoords(conditionExpression);
					subjectCondition.setExpression(expression);
					subject.setCondition(subjectCondition);
					LOGGER.log(Level.INFO, "subjectCondition: " + subjectCondition);
				}
			}


			subscription.setSubject(subject);

			// notification
			SubscriptionNotification subscriptionNotification = new SubscriptionNotification();

			SubscriptionNotificationHttpCustom http = new SubscriptionNotificationHttpCustom();
			http.setUrl(notificationHttpURL);
			subscriptionNotification.setHttpCustom(http);	
			subscriptionNotification.setAttrsFormat("legacy");
			subscriptionNotification.setAttrs(notificationAttrs);

			subscription.setNotification(subscriptionNotification);

			// Transform the bean into json
			String subscriptionJson = beanToJson(subscription);
			LOGGER.log(Level.INFO, "\tSubscriptionJson" + subscriptionJson);

			Map<String, String> configHeaders = new HashMap<String, String>();
			configHeaders.put("fiware-service", headerService);
			configHeaders.put("fiware-servicepath", headerServicePath);
			LOGGER.log(Level.INFO, "\theConfigHaders" + configHeaders);

			LOGGER.log(Level.INFO, "\tbody: " + subscriptionJson);

			serviceResponse = RestUtils.consumePost(endpoint, subscriptionJson, configHeaders);

			LOGGER.log(Level.INFO, "Create Subscription on Orion: " + serviceResponse);

			try {
				jResp = new JSONObject(serviceResponse);
			} catch (org.json.JSONException jsone) {
				jResp = new JSONObject();
				jResp.put("message", serviceResponse);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return serviceResponse;
	}





	/*
	 * Sets the status of the subscription.
	 * 
	 * ENDPOINT: http://217.172.12.177:1026/v2/subscriptions/58a2f9d8701698a211c76f6b
	 * 
	 * METHOD: PATCH
	 * HEADERS:
	 * fiware-service: air
	 * fiware-servicepath: /naples
	 * Content-Type: application/json
	 * 
	 * BODY
	 * {   "status": "active" }
	 * 
	 */

	public static boolean changeSubscriptionStatus(String headerService, String headerServicePath, String subscriptionId, String status) throws Exception {

		LOGGER.log(Level.INFO, SubscriptionManager.class.getName());
		LOGGER.log(Level.INFO, "Going to change Subscription Status on ORION CONTEXT BROKER");
		LOGGER.log(Level.INFO, "Subscription id " + subscriptionId );

		String endpoint = orionHostURL+orionSubscription;

		if (subscriptionId!=null) endpoint = endpoint.concat("/").concat(subscriptionId);
		LOGGER.log(Level.INFO, "Invoking URL " + endpoint);

		Map<String, String> configHeaders = new HashMap<String, String>();
		configHeaders.put("fiware-service", headerService);
		configHeaders.put("fiware-servicepath", "/" + headerServicePath);
		configHeaders.put("Content-Type", MediaType.APPLICATION_JSON);
		LOGGER.log(Level.INFO, "\tTheConfigHaders " + configHeaders);
		JSONObject json = new JSONObject();
		json.put("status", status);
		
		LOGGER.log(Level.INFO, "\tThePayload " + json.toString());

		RestUtils.consumePatch(endpoint, json, configHeaders);

		LOGGER.log(Level.INFO, "Invocation completed");
		return true;

	}



	/*
	 * Get Subscription on orion Context Broker 
	 * METHOD: GET
	 * ENDPOINT: 217.172.12.177:1026/v2/subscriptions/
	 * HEADERS:
	 * fiware-service: parameter
	 * fiware-servicepath: /parameter
	 */
	public static JSONObject getSubscriptions_NotUsed(String headerService, String headerServicePath) throws Exception {

		LOGGER.log(Level.INFO, "SubscriptionManager.getSubscriptions");

		JSONObject jResp = new JSONObject();
		String endpoint = orionHostURL+orionSubscription;

		try {
			LOGGER.log(Level.INFO, "Invoking " + endpoint);
			Map<String, String> headers = new HashMap<String, String>();
			headers.put("fiware-service", headerService);
			headers.put("fiware-servicepath", headerServicePath);
			LOGGER.log(Level.INFO, "\theHaders" + headers);

			String serviceResponse = RestUtils.consumeGet(endpoint, headers);

			JSONArray jsArr = new JSONArray(serviceResponse);
			try {
				JSONObject jso = new JSONObject();
				int count = jsArr.length();
				jso.put("count", count);
				jResp = jso.put("subscriptions", jsArr);
				LOGGER.log(Level.INFO, "Response: " + jResp.toString());
			} catch (org.json.JSONException jsone) {
				jResp = new JSONObject();
				jResp.put("message: ", serviceResponse);
			}
		} catch (org.json.JSONException jsone) {
			jResp = new JSONObject();
			jResp.put("message: ", jsone.getMessage());

		}
		return jResp;

	}


	/*
	 * Get Subscription on orion Context Broker 
	 * METHOD: GET
	 * ENDPOINT: 217.172.12.177:1026/v2/subscriptions/<subscriptionId>
	 * HEADERS:
	 * fiware-service: parameter
	 * fiware-servicepath: /parameter
	 */
	public static Subscription getSubscription(String headerService, String headerServicePath, String subscriptionId) throws Exception {

		LOGGER.log(Level.INFO, "SubscriptionManager.getSubscription by subscriptionId");

		JSONObject jResp = new JSONObject();
		String endpoint = orionHostURL+orionSubscription;
		Subscription apiResp = new Subscription();

		try {
			Map<String, String> headers = new HashMap<String, String>();
			headers.put("fiware-service", headerService);
			headers.put("fiware-servicepath", "/"+headerServicePath);
			LOGGER.log(Level.INFO, "\ttheHaders" + headers);

			endpoint=endpoint+"/"+ subscriptionId;
			LOGGER.log(Level.INFO, "Invoking " + endpoint);
			String resp = RestUtils.consumeGet(endpoint, headers);

			Type type = new TypeToken<Subscription>(){}.getType();
			apiResp = new Gson().fromJson(resp, type);

			LOGGER.log(Level.INFO, "Subscription: " + apiResp);

		} catch (org.json.JSONException jsone) {
			jResp = new JSONObject();
			jResp.put("message: ", jsone.getMessage());
		}
		return apiResp;
	}


	/**
	 * Get Subscription for the specific deviceId among the entities on Orion Context Broker 
	 * METHOD: GET
	 * ENDPOINT: http://217.172.12.224:1026/v2/entities/?type=DeviceSubscription&q=deviceId==<deviceId>
	 * HEADERS:
	 * fiware-service: parameter
	 * fiware-servicepath: /parameter
	 */
	public static Set<DeviceSubscriptionEntity> getDeviceSubscriptions(String headerService, String headerServicePath, String deviceId) throws Exception {

		LOGGER.log(Level.INFO, "SubscriptionManager.getDeviceSubscriptions");

		Set<BasicNameValuePair> params = new HashSet<BasicNameValuePair>();
		params.add(new BasicNameValuePair("type", "KPISubscription"));	
		params.add(new BasicNameValuePair("q", "deviceId=='"+deviceId+"'"));

		String resp =  orion.getEntities(headerService, headerServicePath, params);

		Type type = new TypeToken<Set<DeviceSubscriptionEntity>>(){}.getType();
		Set<DeviceSubscriptionEntity> apiResp = new Gson().fromJson(resp, type);

		LOGGER.log(Level.INFO, "subscription Ids: " + apiResp);
		return apiResp;
	}



	/** 
	 * Delete the subscriptions existing for the specific deviceId
	 * - STEP 1 - Get the deviceSubscriptionEntity for the specific device
	 * - STEP 2 - Delete the subscription on Orion
	 * - STEP 3 - Delete the entity having type=DeviceSubscription and deviceId=<deviceId>
	 * 			  The DeviceSubscription Entity has the relationship among deviceId-subscriptionId-mashupId
	 * DeviceSubscription
	 */

	public static boolean deleteDeviceSubscription(String headerService, String headerServicePath, String deviceId) throws Exception {
		LOGGER.log(Level.INFO, "Delete Subscription on ORION CONTEXT BROKER");

		boolean out = true;

		try{
			Set<DeviceSubscriptionEntity> subscriptions = getDeviceSubscriptions(headerService, headerServicePath, deviceId);
			String subscriptionId = "";
			String entitiyId = "";

			for (DeviceSubscriptionEntity subscription : subscriptions){
				subscriptionId = subscription.getSubscriptionId().getValue();
				entitiyId = subscription.getId();

				// STEP 2: Delete orion subscription
				LOGGER.log(Level.INFO, "Deleting subscription Id: " + subscriptionId);
				deleteEntityOnOrion(orionHostURL+orionSubscription,  subscriptionId,  headerService,  headerServicePath);

				// STEP 3: Delete orion entity DeviceSubscription
				LOGGER.log(Level.INFO, "Deleting entity Id: " + entitiyId);
				deleteEntityOnOrion(orionHostURL+orionEntity,  entitiyId,  headerService,  headerServicePath);
			}

		}
		catch(Exception e){
			e.printStackTrace();
			out = false;
		}

		return out;
	}

	/*
	 * Delete an entity on Orion Context Broker 
	 * METHOD: DELETE
	 * ENDPOINT: 217.172.12.177:1026/v2/subscriptions/
	 * HEADERS:
	 * fiware-service: parameter
	 * fiware-servicepath: /parameter
	 */

	public static boolean deleteEntityOnOrion(String orionUrlPath, String entityId, String headerService, String headerServicePath) throws Exception {

		LOGGER.log(Level.INFO, "Deleting entity " + entityId + " on Orion...");
		ClientResponse serviceResponse = null;
		String endpoint = "";

		if (entityId!=null) endpoint = orionUrlPath.concat("/").concat(entityId);

		LOGGER.log(Level.INFO, "Invoking URL " + endpoint);

		Map<String, String> configHeaders = new HashMap<String, String>();
		configHeaders.put("fiware-service", headerService);
		configHeaders.put("fiware-servicepath", headerServicePath);
		LOGGER.log(Level.INFO, "\tTheConfigHaders" + configHeaders);

		boolean out = true;
		try{
			serviceResponse = RestUtils.consumeDelete(endpoint, configHeaders);
			if (serviceResponse.getStatus() != 200 && serviceResponse.getStatus() != 201
					&& serviceResponse.getStatus() != 301 && serviceResponse.getStatus() != 204
					&& serviceResponse.getStatus() != 404) {
				throw new Exception("URL " + endpoint + " responded with status " + serviceResponse.getStatus()
				+ " and message: " + serviceResponse.getEntity(String.class));
			}
		}
		catch(Exception e){
			e.printStackTrace();
			out = false;
		}
		LOGGER.log(Level.INFO, "Invocation completed: " + out);
		return out;
	}


	/* Create an entity on orion with the relationship between device and subscription */
	public static String createDeviceSubscriptionEntity(String deviceId, String subscriptionId, String value, String headerService, String headerServicePath) throws Exception {
		// COMPONGO IL BEAN DeviceSubscriptionEntity con i parametri che mi arrivano dal client
		JSONObject jResp = new JSONObject();
		String serviceResponse = "";
		try {

			String endpoint = orionHostURL+ orionEntity;
			LOGGER.log(Level.INFO, "Invoking " + endpoint);

			DeviceSubscriptionEntity entity = new DeviceSubscriptionEntity();
			// Create the device subscription entity
			entity.setId(deviceId+"_"+subscriptionId); // TODO: IS CORRECT
			entity.setType("KPISubscription"); // TODO: IS CORRECT
			entity.setValue(new EntityAttribute<String>(value));
			entity.setDeviceId(new EntityAttribute<String>(deviceId));
			
			entity.setSubscriptionId(new EntityAttribute<String>(subscriptionId));
			//entity.setMashupId(new EntityAttribute<String>(mashupId));

			// Transform the bean into json
			String entityJ = beanToJson(entity);
			LOGGER.log(Level.INFO, "EntityJson" + entityJ);

			Map<String, String> configHeaders = new HashMap<String, String>();
			configHeaders.put("fiware-service", headerService);
			configHeaders.put("fiware-servicepath", headerServicePath);
			LOGGER.log(Level.INFO, "\ttheConfigHaders" + configHeaders);
			LOGGER.log(Level.INFO, "\tbody: " + entityJ);

			serviceResponse = RestUtils.consumePost(endpoint, entityJ, configHeaders);
			LOGGER.log(Level.INFO, "Create Device Subscription Entity on Orion: " + serviceResponse);

			try {
				jResp = new JSONObject(serviceResponse);
			} catch (org.json.JSONException jsone) {
				jResp = new JSONObject();
				jResp.put("message", serviceResponse);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return serviceResponse;
	}



	public static String beanToJson(Object beanObject) throws Exception {

		LOGGER.log(Level.INFO, "Transforming the bean into json specific structure...");

		String obj = new Gson().toJson(beanObject);
		System.out.print("JSON: " + obj);
		return obj;

	}

	/* For TESTING */
	public static void main(String[] args) throws Exception {

		JSONObject obj = new JSONObject();
		obj.put("id", "22");
		obj.put("type", new Integer(100));

		JSONObject schemaOJ = new JSONObject();
		schemaOJ.put("value", "pippo");
		schemaOJ.put("type", "String");
		obj.put("schema", schemaOJ);

		// deleteServiceConfig();
		// deleteIotService();
		//JSONObject devicesJson = getDeviceList("test5", "water");

		/*int count = devicesJson.getInt("count");
		JSONArray jsArray = devicesJson.getJSONArray("devices");

		for (int i = 0; i < count; i++) {
			try {
				JSONObject currDevice = jsArray.getJSONObject(i);
				String device_id = currDevice.getString("device_id");
				//deleteIotDevice(device_id, "test5", "water");
			} catch (Exception ex) {
				ex.printStackTrace();
			}

		}



		 */


		// deleteIotDevice("deviceId", headerService, headerServicePath);

		// TEST OK
		//LOGGER.log(Level.INFO, "getSubscriptions " + getSubscriptions("public", "/water"));

		//TEST OK
		//LOGGER.log(Level.INFO, "deleteSubscription " + deleteSubscription("58ad6b27226ec80488483c69", "test", "/anna"));

		// TEST OK
		//changeSubscriptionStatus("58a2f9d8701698a211c76f6b", "inactive", "air", "/naples");

		HashSet<String> conditionAttrs = new HashSet<String>();
		conditionAttrs.add("CO2");
		conditionAttrs.add("status");

//		HashSet<String> notificationAttrs = new HashSet<String>();
		/*
		notificationAttrs.add("status");
		notificationAttrs.add("location");
		notificationAttrs.add("dateCreated");
		notificationAttrs.add("dateModified");
		 */

//		String conditionExpressionType = "q";
//		String conditionExpression = "temperature > 100" ; 
//
//		String subscriptionEvent = "OnDeviceDelete";
		//createSubscription(subscriptionEvent, conditionAttrs, conditionExpressionType, conditionExpression, notificationAttrs , "test_anna" ,"test", "/anna");

		// TEST OK
		//createOnDeleteDeviceSubscription(notificationAttrs , "bologna_1", "/bologna_1_tourism");

		// TEST OK
		//String subscriptionId = createOnUpdateDeviceSubscription("device01", "mashup01", notificationAttrs , "test", "/anna1");
		//System.out.print("subscriptionId " + subscriptionId);


		// TEST OK
		//createDeviceSubscriptionEntity("deviceId", "subscriptionId", "test", "/anna1");

		// TEST OK
		//Set<DeviceSubscriptionEntity> res = getDeviceSubscriptions("bologna_1", "/bologna_1_tourism", "infoanna001");
		//System.out.print("Result: " + res);

		//TEST OK
		//Subscription subs = getSubscription("bologna_1", "/bologna_1_tourism", "596f4d3d2d0975a576f667ff");
		//System.out.print("Result: " + subs.getStatus());

		System.out.print("END MAIN");

	}



	public static String updateEntity(String idEntity, String newValueP, String fiware_service, String fiware_servicepath) {
		// TODO Auto-generated method stub
		JSONObject jResp = new JSONObject();
		String serviceResponse = "";
		try {

			String endpoint = orionHostURL+ orionEntity + "/" + idEntity + "/attrs";
			LOGGER.log(Level.INFO, "Invoking " + endpoint);

			UpdatedEntity entity = new UpdatedEntity();
			// Create the device subscription entity
//			entity.setId(deviceId+"_"+subscriptionId); // TODO: IS CORRECT
//			entity.setType("KPISubscription"); // TODO: IS CORRECT
			entity.setValue(new EntityAttribute<String>(newValueP));
			//entity.setDeviceId(new EntityAttribute<String>(deviceId));
			
			//entity.setSubscriptionId(new EntityAttribute<String>(subscriptionId));
			//entity.setMashupId(new EntityAttribute<String>(mashupId));

			// Transform the bean into json
			String entityJ = beanToJson(entity);
			LOGGER.log(Level.INFO, "EntityJson" + entityJ);

			Map<String, String> configHeaders = new HashMap<String, String>();
			configHeaders.put("fiware-service", fiware_service);
			configHeaders.put("fiware-servicepath", fiware_servicepath);
			LOGGER.log(Level.INFO, "\ttheConfigHaders" + configHeaders);
			LOGGER.log(Level.INFO, "\tbody: " + entityJ);

			RestUtils.consumePatch(endpoint, entityJ, configHeaders);
			LOGGER.log(Level.INFO, "Update Entity on Orion: " + serviceResponse);

			try {
				jResp = new JSONObject(serviceResponse);
			} catch (org.json.JSONException jsone) {
				jResp = new JSONObject();
				jResp.put("message", serviceResponse);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return serviceResponse;
	}

}

