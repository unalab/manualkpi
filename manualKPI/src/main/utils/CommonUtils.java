package main.utils;



import java.net.URL;
//import java.util.Date;
//import java.util.HashSet;
//import java.util.Random;
//import java.util.Set;
//import java.util.logging.Logger;

//import it.eng.iot.configuration.ConfSimulator;
//import it.eng.iot.servlet.model.MeasureEntity;

public abstract class CommonUtils {
	
	//private static final Logger LOGGER = Logger.getLogger(CommonUtils.class.getName() );

	public static boolean isValidURL(String urlString){
	    boolean out = false;
		try{
	        URL url = new URL(urlString);
	        url.toURI();
	        out = true;
	    } 
	    catch (Exception exception){
	        out = false;
	    }
		
		return out;
	}
	
	/** 
	 * Load the business urbanservice, and the related attributes to be measured
	 * in the configuration_simulator.properties file
	 * 
	 */
//	public static Set<MeasureEntity> loadBusinessDataModel(String urbanserviceName, String measure_id , String measure_type) {
//
//		String elem = ConfSimulator.getString(urbanserviceName.toLowerCase());
//		String unitOfMeasure = ConfSimulator.getString("unit_of_measure");
//		int radiusDistance = Integer.parseInt(ConfSimulator.getString("radius_distance"));
//		
//		String[] attributesToMeasure = elem.split(",");
//
//		Set<MeasureEntity> measures = new HashSet<MeasureEntity>();
//		
//		boolean found = false;
//		
//		try {
//			
//			for (String attr :attributesToMeasure){
//
//				String id = attr.substring(0, attr.indexOf("|"));
//				
//				if (measure_id.equalsIgnoreCase(id)) {
//					
//					MeasureEntity measure = new MeasureEntity();
//
//					measure.setId(id);
//					attr = attr.replace(id+"|", "");
//	
//					String type = attr.substring(0, attr.indexOf("|"));
//					measure.setType(type);
//					attr = attr.replace(type+"|", "");
//	
//					String valueRange =	attr.substring(1, attr.length()-1);	// remove []
//					measure.setValueRange(valueRange);
//
//				
//					measure = assignValueToMisure (  measure,  measure_type);
//					measures.add(measure);
//					found = true;
//					break;
//				
//				}
//			}	
//			
//		}catch (Exception e) {
//			e.printStackTrace();
//			LOGGER.severe("NO ATTRIBUTE HAS BEEN CONFIGURED INTO THE SIMULATOR PROPERTIES FILE");
//		}
//		
//		try {
//		
//			//if in the properties there isn't a configuration by measure_id, use the one by type
//			if (!found) {
//				
//				 elem = ConfSimulator.getString(measure_type.toLowerCase());
//				 
//				 MeasureEntity measure = new MeasureEntity();
//				 	measure.setId(measure_id);
//					measure.setType(measure_type);
//					String valueRange =	elem.substring(1, elem.length()-1);	// remove []
//					measure.setValueRange(valueRange);
//					measure.setRadiusDistance(radiusDistance);
//					measure.setUnitOfMeasure(unitOfMeasure);
//					measure = assignValueToMisure (  measure,  measure_type);
//					measures.add(measure);
//			}
//		
//		}catch (Exception e) {
//			e.printStackTrace();
//			LOGGER.severe("NO TYPE ATTRIBUTE HAS BEEN CONFIGURED INTO THE SIMULATOR PROPERTIES FILE");
//		}
//		
//		return measures;
//
//	}
//	
//	
//	private static MeasureEntity assignValueToMisure ( MeasureEntity measure, String measure_type) {
//		
//		//create value
//		if ( measure_type.equalsIgnoreCase("integer") || measure_type.equalsIgnoreCase("number") || measure_type.equalsIgnoreCase("int") ) {
//			String[] values = measure.getValueRange().split("-");
//			int min = Integer.parseInt(values[0]);
//			int max = Integer.parseInt(values[1]);
//			int val = randInt(min, max);
//			measure.setValue(val);
//		}else if (measure_type.equalsIgnoreCase("float") || measure_type.equalsIgnoreCase("decimal")){
//			String[] values = measure.getValueRange().split("-");
//			float min = Float.parseFloat(values[0]);
//			float max = Float.parseFloat(values[1]);
//			float val = randFloat(min, max);
//			measure.setValue(val);
//		
//		}else if (measure_type.equalsIgnoreCase("double") ){
//			String[] values = measure.getValueRange().split("-");
//			double min = Double.parseDouble(values[0]);
//			double max = Double.parseDouble(values[1]);
//			double val = randDouble(min, max);
//			measure.setValue(val);	
//			
//		} else if (measure_type.equalsIgnoreCase("string") || measure_type.equalsIgnoreCase("text") || measure_type.equalsIgnoreCase("chars")){
//			String[] values = measure.getValueRange().split("-");
//			int i = randInt(0, values.length-1); 
//			String val = values[i];
//			measure.setValue(val);
//			
//		} else if (measure_type.equalsIgnoreCase("DateTime") || measure_type.equalsIgnoreCase("date") || measure_type.equalsIgnoreCase("timestamp") ){
//			
//			Date date = new Date(System.currentTimeMillis());
//			measure.setValue(date);	
//		
//		}else if (measure_type.equalsIgnoreCase("boolean")  ){
//				
//				boolean boolRnd = randBoolean();
//				measure.setValue(boolRnd);	
//		}else if (measure_type.equalsIgnoreCase("geo:point")  ){
//			
//			double[] geoRnd = randGeo(35,12, measure.getRadiusDistance(), measure.getUnitOfMeasure());
//			
//			String geoRndS = geoRnd[0]+","+geoRnd[1];
//			
//			
//			measure.setValue(geoRndS);		
//		}
//		
//		return measure;
//	}
//	
//	
//	
//	/**
//	 * Returns a psuedo-random int number between min and max, inclusive.
//	 */
//	public static int randInt(int min, int max) {
//		Random rand = new Random();
//		int randomNum = rand.nextInt((max - min) + 1) + min;
//		return randomNum;
//	}
//	/**
//	 * Returns a psuedo-random float number between min and max, inclusive.
//	 */
//	public static float randFloat(float min, float max) {
//		Random rand = new Random();
//		float result = rand.nextFloat() * (max - min) + min;
//		return result;
//	}
//	
//	/**
//	 * Returns a psuedo-random double number between min and max, inclusive.
//	 */
//	public static double randDouble(double min, double max) {
//		Random rand = new Random();
//		double result = rand.nextDouble() * (max - min) + min;
//		return result;
//	}
//	
//	/**
//	 * Returns a psuedo-random boolean
//	 */
//	public static boolean randBoolean() {
//		Random rand = new Random();
//		boolean result = rand.nextBoolean();
//		return result;
//	}
//	
//	
//	/**
//	 * Returns a psuedo-random geo point
//	 */
//	public static double[] randGeo(double centerX, double centerY, int radius, String unitOfMeasure) {
//		
//		
//		double[] results = new double[] {centerX,centerY };
//		
//		
//		double R = radius;
//		double paramConvKm = 6371;
//		double paramConvMi = 3959;
//		
//		//https://stackoverflow.com/questions/12180290/convert-kilometers-to-radians
//		if (unitOfMeasure.equalsIgnoreCase("km"))
//			R = radius/paramConvKm; 
//		else if (unitOfMeasure.equalsIgnoreCase("mi"))
//			R = radius/paramConvMi;
//		
//		//https://stackoverflow.com/questions/5837572/generate-a-random-point-within-a-circle-uniformly/50746409#50746409	
//		double rnd = randDouble(0,1);
//		
//		double r = R * Math.sqrt(rnd);
//		
//		double theta = rnd * 2 * Math.PI;
//		
//		
//		double x = centerX + r * Math.cos(theta);
//		double y = centerY + r * Math.sin(theta);
//		
//		results[0]=y;
//		results[1]=x;
//		
//		return results;
//	}
}
