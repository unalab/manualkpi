package main.utils;




import java.net.URI;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.core.MediaType;

import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource.Builder;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;


public abstract class RestUtils {
	
	
	
	public static String consumePost(String url, Object body, Map<String, String> headers) 
			throws Exception{
		
		Client client = Client.create();
		Builder builder =  client.resource(url).type(MediaType.APPLICATION_JSON);
		
		if(headers != null){
			Set<Map.Entry<String, String>> hs = headers.entrySet();
			for(Map.Entry<String, String> h : hs){
				builder = builder.header(h.getKey(), h.getValue());
			}
		}
		
		ClientResponse resp =builder.post(ClientResponse.class, body.toString());
		
		
		
		if(resp.getStatus()>301){
			throw new Exception("URL "+url+
					" responded with status "+resp.getStatus()+
					" and message: "+resp.getEntity(String.class));
		}
		
		String r = "";
		if(resp.getStatus()!= HttpStatus.SC_NO_CONTENT  && resp.hasEntity()){
			r = resp.getEntity(String.class);
		}
		
		return r;
	}
	
	public static String consumePost(String url, Object body) 
			throws Exception{
		
		Client client = Client.create();
		Builder builder =  client.resource(url).type(MediaType.APPLICATION_JSON);
		
		
		
		ClientResponse resp =builder.post(ClientResponse.class, body.toString());
		
		
		
		if(resp.getStatus()>301){
			throw new Exception("URL "+url+
					" responded with status "+resp.getStatus()+
					" and message: "+resp.getEntity(String.class));
		}
		
		String r = "";
		if(resp.getStatus()!= HttpStatus.SC_NO_CONTENT  && resp.hasEntity()){
			r = resp.getEntity(String.class);
		}
		
		return r;
	}
	
	
	public static String consumeGet(String url) throws Exception{
		
		Client client = Client.create();
		ClientResponse resp  = client.resource(url).get(ClientResponse.class);
		
		if(resp.getStatus()>301){
			throw new Exception("URL "+url +
					" responded with status "+resp.getStatus() +
					" and message: "+resp.getEntity(String.class));
		}
		String r = "";
		if(resp.getStatus()!= HttpStatus.SC_NO_CONTENT  && resp.hasEntity()){
			r = resp.getEntity(String.class);
		}
		return r;
	}
	
	public static String consumeGet(String url, HTTPBasicAuthFilter token) throws Exception{
		
		Client client = Client.create();
				client.addFilter(token);

		ClientResponse resp = client.resource(url).type(MediaType.APPLICATION_JSON).get(ClientResponse.class);
		
		if(resp.getStatus()>301){
			throw new Exception("URL "+url +
					" responded with status "+resp.getStatus() +
					" and message: "+resp.getEntity(String.class));
		}
		
		String r = "";
		if(resp.getStatus()!= HttpStatus.SC_NO_CONTENT  && resp.hasEntity()){
			r = resp.getEntity(String.class);
		}
		return r;
		
	}
	
	
	public static String consumeGet(String url, Map<String, String> headers) throws Exception{
		
		Client client = Client.create();
		Builder builder = client.resource(url).getRequestBuilder(); 
		
		if(headers != null){
			Set<Map.Entry<String, String>> hs = headers.entrySet();
			for(Map.Entry<String, String> h : hs){
				builder = builder.header(h.getKey(), h.getValue());
			}
		}
		
		ClientResponse resp = builder.get(ClientResponse.class);
		
		if(resp.getStatus()>301){
			throw new Exception("URL "+url +
					" responded with status "+resp.getStatus() +
					" and message: "+resp.getEntity(String.class));
		}
		
		String r = "";
		if(resp.getStatus()!= HttpStatus.SC_NO_CONTENT  && resp.hasEntity()){
			r = resp.getEntity(String.class);
		}
		return r;
		
	}
	
	
	public static ClientResponse consumeDelete(String url, Map<String, String> headers) throws Exception{
		
		Client client = Client.create();
		Builder builder = client.resource(url).getRequestBuilder(); 
		if(headers != null){
			Set<Map.Entry<String, String>> hs = headers.entrySet();
			for(Map.Entry<String, String> h : hs){
				builder = builder.header(h.getKey(), h.getValue());
			}
		}
		ClientResponse resp = builder.delete(ClientResponse.class);
		
		if(resp.getStatus()>301){
			throw new Exception("URL "+url +
					" responded with status "+resp.getStatus() +
					" and message: "+resp.getEntity(String.class));
		}
		
		return resp;
	}

	public static void consumePut (String url, Object body, MediaType type, Map<String, String> headers) throws Exception{
		
		Client client = Client.create();
		Builder builder =  client.resource(url).type(type);
		
		if(headers != null){
			Set<Map.Entry<String, String>> hs = headers.entrySet();
			for(Map.Entry<String, String> h : hs){
				builder = builder.header(h.getKey(), h.getValue());
			}
		}
		
		ClientResponse resp =builder.put(ClientResponse.class, body.toString());
		if(resp.getStatus()>301){
			throw new Exception("URL "+url+
					" responded with status "+resp.getStatus()+
					" and message: "+resp.getEntity(String.class));
		}
		
		return;
	}
	
	public static String consumePost (String url, Object body, MediaType type, Map<String, String> headers) throws Exception{
		
		Client client = Client.create();
		Builder builder =  client.resource(url).type(type);
		
		if(headers != null){
			Set<Map.Entry<String, String>> hs = headers.entrySet();
			for(Map.Entry<String, String> h : hs){
				builder = builder.header(h.getKey(), h.getValue());
			}
		}
		
		ClientResponse resp =builder.post(ClientResponse.class, body.toString());
		if(resp.getStatus()>301){
			throw new Exception("URL "+url+
					" responded with status "+resp.getStatus()+
					" and message: "+resp.getEntity(String.class));
		}
		
		String r = "";
		if(resp.getStatus()!= HttpStatus.SC_NO_CONTENT  && resp.hasEntity()){
			r = resp.getEntity(String.class);
		}
		
		return r;
	}

	public static void consumePatch(String url, Object body, Map<String, String> headers) throws Exception{
		
		CloseableHttpClient httpClient = HttpClients.createDefault();
		HttpPatch httpPatch = new HttpPatch(new URI(url));
		
		try {
		StringEntity entity = new StringEntity(body.toString());
		entity.setContentType("application/json");
		httpPatch.setEntity(entity);
	
		if(headers != null){
			Set<Map.Entry<String, String>> hs = headers.entrySet();
			for(Map.Entry<String, String> h : hs){
				httpPatch.setHeader(h.getKey(), h.getValue());
			}
		}
		
		CloseableHttpResponse response = httpClient.execute(httpPatch);
		
		int status = response.getStatusLine().getStatusCode();
		if(status>301){
			throw new Exception("URL "+url +
					" responded with status "+status);
		}
		
		response.close();
		}
		finally {
			httpClient.close();
		}
		return ;
	}

	
	// Return the full ClientResponse
	public static ClientResponse consumePostFull(String url, Object body, Map<String, String> headers) throws Exception{
		
		Client client = Client.create();
		Builder builder =  client.resource(url).type(MediaType.APPLICATION_JSON);
		
		if(headers != null){
			Set<Map.Entry<String, String>> hs = headers.entrySet();
			for(Map.Entry<String, String> h : hs){
				builder = builder.header(h.getKey(), h.getValue());
			}
		}
		
		ClientResponse resp =builder.post(ClientResponse.class, body.toString());
		if(resp.getStatus()>301){
			throw new Exception("URL "+url+
					" responded with status "+resp.getStatus()+
					" and message: "+resp.getEntity(String.class));
		}

		return resp;
	}
	
	
}
