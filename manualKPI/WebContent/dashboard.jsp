<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="main.java.KPIBean"%>
<%@page import="main.bean.LoginBean"%>
<%@page import="java.util.ArrayList"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%String role = (String) request.getSession().getAttribute("user");%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta charset="ISO-8859-1">
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<title>City Performance Monitor</title> 
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		
<link type="text/css" rel="stylesheet" href="css/table-sorter.css" media="screen,projection" /> 		
 <link type="text/css" rel="stylesheet" href="css/custom.css" media="screen,projection" /> 
 <link type="text/css" rel="stylesheet" href="css/materialize.min.css" media="screen,projection" /> 
<!--   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/css/materialize.min.css"> -->
	
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<!--     <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/js/materialize.min.js"></script> -->
<script type="text/javascript" src="js/materialize.js"></script>
<script type="text/javascript" src="js/pagination.js"></script>
<script type="text/javascript" src="js/variable.js"></script>
<script type="text/javascript" src="js/table-sorter.js"></script>
<script type="text/javascript" src="js/custom.js"></script>




				
</head>
<body>
<%-- <c:import url="/GetKPIList2" /> --%>
<c:set var="city" value="${param.city}" />
<% if(role != null) {
	%>
	<nav id="header">
	<div class="nav-wrapper white">
		<a href="index.jsp" class="brand-logo"><img
			src="img/logo_alpha_270x.png" class="logo" alt="UNaLab logo"
			style="width: 42%"></a> <span class="brand-logo secondary-text"
			style="margin-left: 130px; margin-top: 12px; color: #77943E">City Performance Monitor</span>



		<ul class="right">
			<% if (role == "admin"){%>
				<li><a href="/cpm/GetCommonKPIs?city=${param.city}" class="grey-text text-darken-4">Citizen View</a></li>
				<li><a href="/cpm/GetAdminKPIList?city=3&level=task&category=all" class="grey-text text-darken-4">Expert View</a></li>
			
			<%} else if (role == "eindhoven_city_manager"){%>	
				<li><a href="/cpm/GetCommonKPIs?city=3" class="grey-text text-darken-4">Citizen View</a></li>
				<li><a href="/cpm/GetKPIList?city=3&level=task&category=all" class="grey-text text-darken-4">Expert View</a></li>
				
			<%} else if (role == "genova_city_manager") {%>
				<li><a href="/cpm/GetCommonKPIs?city=1" class="grey-text text-darken-4">Citizen View</a></li>
				<li><a href="/cpm/GetKPIList?city=1&level=task&category=all" class="grey-text text-darken-4">Expert View</a></li>
			
			<%} else if (role == "tampere_city_manager") { %>
				<li><a href="/cpm/GetCommonKPIs?city=2" class="grey-text text-darken-4">Citizen View</a></li>
				<li><a href="/cpm/GetKPIList?city=2&level=task&category=all" class="grey-text text-darken-4">Expert View</a></li>
			<% } %>
			
			<li class="right">
			<a class="username dropdown-button dropdown-user-big" href="#" data-target="userdropdown_big">
			<i class="material-icons left">perm_identity</i>
			<%if (role == "eindhoven_city_manager"){%>
				<span class="val">eindhoven-admin</span> 
			<% } else if (role == "genova_city_manager") { %>
				<span class="val">genova-admin</span> 
			<% } else if(role == "tampere_city_manager") {%>
				<span class="val">tampere-admin</span> 
			<%} else if(role == "admin") {%>
				<span class="val">cpm-admin</span>
			<%} %>			
			<i class="material-icons right">arrow_drop_down</i>
			</a>
			<ul id="userdropdown_big"  class='dropdown-content'>

				<li><a href="/cpm/LogoutServlet">Logout</a></li>

			</ul>
			</li>
		</ul>
	</div>
	</nav>
	<%} else{%>
	<nav id="header">
	<div class="nav-wrapper white">
		<a href="index.jsp" class="brand-logo"><img
			src="img/logo_alpha_270x.png" class="logo" alt="UNaLab logo"
			style="width: 42%"></a> <span class="brand-logo secondary-text"
			style="margin-left: 130px; margin-top: 12px; color: #77943E">City Performance Monitor</span>



		<ul class="right">
			
			<li><a href="/cpm/GetCommonKPIs?city=${param.city}" class="grey-text text-darken-4">Citizen View</a></li>
			<li><a href="/cpm/GetGuestKPIList?city=${param.city}&level=task&category=all" class="grey-text text-darken-4">Expert View</a></li>
			
			<li class="right"><a class="username dropdown-user-big" href="/cpm/login.jsp"><i
					class="material-icons left">input</i><span class="val">Login</span> </a></li>
		</ul>
	</div>
	</nav>
	<%} %>
	<h5 class="center"> City Dashboard </h5>
	<div class="z-depth-1" style="margin:20px">
	<iframe id="cityDashboard" width="100%" height="800"
    src=""
     frameborder="0">
	</iframe>
	</div>
	
	
<footer class="valign-wrapper primary"
		style="position:fixed;bottom:0;left:0;width:100%;">
		<div class="footer-section section-left white-text" style="background-color: black; width: 100%; height: 35px;">&nbsp;</div>
	<div class="footer-section section-center white-text" style="background-color: black; width: 100%; height: 35px;">&copy; UNALAB project</div>
		
		<div class="footer-section section-right white-text" style="background-color: black; width: 100%; height: 35px;"><span>Powered by </span><img class="footer-logo" src="img/digital-enabler-white.png"/></div>
	</footer>
	