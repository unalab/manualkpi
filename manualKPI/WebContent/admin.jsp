<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="main.java.KPIBean"%>
<%@page import="main.bean.LoginBean"%>
<%@page import="java.util.ArrayList"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<% ArrayList<KPIBean> kpiList = new ArrayList<KPIBean>();
  kpiList = (ArrayList<KPIBean>) request.getAttribute("kpiList");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta charset="ISO-8859-1">
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<title>City Performance Monitor</title> 
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		
<link type="text/css" rel="stylesheet" href="css/table-sorter.css" media="screen,projection" /> 		
 <link type="text/css" rel="stylesheet" href="css/custom.css" media="screen,projection" /> 
 <link type="text/css" rel="stylesheet" href="css/materialize.min.css" media="screen,projection" /> 
<!--   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/css/materialize.min.css"> -->
	
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<!--     <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/js/materialize.min.js"></script> -->
<script type="text/javascript" src="js/materialize.js"></script>
<script type="text/javascript" src="js/pagination.js"></script>
<script type="text/javascript" src="js/variable.js"></script>
<script type="text/javascript" src="js/table-sorter.js"></script>
<script type="text/javascript" src="js/custom.js"></script>



				
</head>

<% //In case, if Admin session is not set, redirect to Login page
if((request.getSession(false).getAttribute("user")== null) )
{
%>
<jsp:forward page="login.jsp"></jsp:forward>
<%} 


%>
<body>
<%-- <c:import url="/GetKPIList2" /> --%>
<c:set var="city" value="${param.city}" />

	<nav id="header">
	<div class="nav-wrapper white">
		<a href="index.jsp" class="brand-logo"><img
			src="img/logo_alpha_270x.png" class="logo" alt="UNaLab logo"
			style="width: 42%"></a> <span class="brand-logo secondary-text"
			style="margin-left: 130px; margin-top: 12px; color: #77943E">City Performance Monitor</span>



		<ul class="right">
		   
			<li><a href="/cpm/dashboard.jsp?city=${param.city}" class="grey-text text-darken-4">City Dashboard</a></li>
			<li><a href="/cpm/GetCommonKPIs?city=${param.city}" class="grey-text text-darken-4">Citizen View</a></li>
			<li class="right">
			<a class="username dropdown-button dropdown-user-big" href="#" data-target="userdropdown_big">
			<i class="material-icons left">perm_identity</i>
			<span class="val">cpm-admin</span>
			<i class="material-icons right">arrow_drop_down</i>
			</a>
			<ul id="userdropdown_big"  class='dropdown-content'>

				<li><a href="/cpm/LogoutServlet">Logout</a></li>

			</ul>
			</li>
			
		</ul>
	</div>
	</nav>
	
	<iframe style = "display:none" src="http://unalab.eng.it:80/knowage/public/servlet/AdapterHTTP?ACTION_NAME=EXECUTE_DOCUMENT_ACTION&OBJECT_LABEL=backgroundCockpit&TOOLBAR_VISIBLE=true&ORGANIZATION=DEFAULT_TENANT"></iframe>

<div class = "row">
<div class="col s2">
<a style="margin-top: 15px" class="btn-floating btn-medium waves-effect waves-light unalabGreen modal-trigger tooltipped" data-position="bottom" data-tooltip="Add KPI" href="#modal1" style="margin-left: 40px"><i class="material-icons">add</i></a>
</div>
<div class="col s8">
<h5 class="center" >Expert View</h5>

</div>
</div>

	<div class="row">
	
	 
<form id="selectForm" action="/cpm/GetAdminKPIList"
			 method="post">	 
<div class="input-field col s3">

<input name="city" type="hidden" value="${param.city}"></input> 
<span style="color:#77943e">Filter by Indicators level</span>

<select id="one" name="one" onchange="this.form.submit();">
  <option value="task" <%if(request.getParameter("level").equals("task")){ %> selected <%} %>>Common Indicators</option>
  <option value="city" <%if(request.getParameter("level").equals("city")){ %> selected <%} %>>City-Level Indicators</option>
  <option value="project" <%if(request.getParameter("level").equals("project")){ %> selected <%} %>>Project-Level Indicators</option>
</select>



<span style="color:#77943e">Filter by Indicators category</span>

<select id="two" name="two" onchange="this.form.submit();">

  <option value="all" <%if(request.getParameter("category").equals("all")){ %> selected <%} %>>Show all</option>
  <option value="climate" <%if(request.getParameter("category").equals("climate")){ %> selected <%} %>>Climate change adaptation and mitigation</option>
  <option value="airquality" <%if(request.getParameter("category").equals("airquality")){ %> selected <%} %>>Air Quality</option>
  <option value="economic"<%if(request.getParameter("category").equals("economic")){ %> selected <%} %>>Economic opportunities and green jobs</option>
  <option value="water" <%if(request.getParameter("category").equals("water")){ %> selected <%} %>>Water management</option>
  <option value="green" <%if(request.getParameter("category").equals("green")){ %> selected <%} %>>Green space management</option>

</select>
    
    
    
  
  <select id="third" name="third" onchange="this.form.submit();">

  <option value="all" <%if(request.getParameter("category").equals("all")){ %> selected <%} %>>Show all</option>
  <option value="people" <%if(request.getParameter("category").equals("people")){ %> selected <%} %>>People</option>
  <option value="planet" <%if(request.getParameter("category").equals("planet")){ %> selected <%} %>>Planet</option>
  <option value="prosperity" <%if(request.getParameter("category").equals("prosperity")){ %> selected <%} %>>Prosperity</option>
  <option value="governance" <%if(request.getParameter("category").equals("governance")){ %> selected <%} %>>Governance</option>
  <option value="propagation" <%if(request.getParameter("category").equals("propagation")){ %> selected <%} %>>Propagation</option>
  

</select>

<!-- <button type="submit" id="submit" name="submit" -->
<!-- 						class="waves-effect waves-light btn right hoverable unalabGreen"> -->
<!-- 						Filter -->
<!-- 				</button>				 -->
  
</div>
				

  </form>
	
	 
	 
	
		<div class="col s9" style="padding-left:35px">
			<input class="unalabInput" type="text" id="search" placeholder="Type to search by indicator name..." />
			<div class="tableContent">
			<table cellpadding="1" cellspacing="1" class="table table-hover sortable-table indexed" id="myTable2">
				<thead>
					<tr class="sorter-header">
						<th class="unalabTd no-sort">ID</th>
						<th class="unalabTd no-sort">Indicator</th>
<!-- 						<th class="unalabTd">Description</th> -->
						<th class="unalabTd no-sort">Value</th>
						<th class="unalabTd no-sort">Measure Unit</th>
						
						<th class="unalabTd is-date ascending">Last modified</th>
						<th class="unalabTd no-sort">Actions</th>
<!-- 						<th class="unalabTd"></th> -->
					</tr>
				</thead>
				<tbody id="myTable">
		<c:set var="mykpi" value="${requestScope.kpiList}" />

			<c:forEach var="kpi" items= "${mykpi}" varStatus="i">
			        
<%-- 				<c:set var="editKPI" --%>
<%-- 						value="/cpm/EditKPI?idCity=${kpi.getIdCity()}&idKPI=${kpi.getIdKPI()}&level=${kpi.getKpiLevel()}&category=${kpi.getKpiCategory()}"></c:set> --%>
					<c:set var="isManual" value="${kpi.getIsManual()}"></c:set> 
					
					
					

					
					<tr>
					    <td class="unalabTd">${kpi.getTableId()}</td>
						<td class="unalabTd"><a style="padding:0 4px !important" href=# class="tooltipped btn-flat" data-position="bottom" data-tooltip="${kpi.getDescription()}"> <i class="tiny material-icons">info</i></a>${kpi.getName()} </td>
<%-- 						<td class="unalabTd"style="white-space: nowrap; text-overflow:ellipsis; overflow: hidden; max-width:3px;">${kpi.getDescription()}</td> --%>
						<td class="unalabTd">${kpi.getValue()}</td>
						<td class="unalabTd">${kpi.getUnit()}</td>
						
						<td class="unalabTd">${kpi.getDate()}</td>
						
						<c:choose>
						<c:when test = "${isManual == 'manual'}">
						<td class="unalabTd">
							<a style="padding:0 !important" href="#modal3" id="btnMio2" onclick="myParamAdminInfo('${kpi.getIdKPI()}','${kpi.getKpiLevel()}','${kpi.getKpiCategory()}','${kpi.getIdCity()}', '${kpi.getName()}','${kpi.getTableId()}','${kpi.getDescription()}','${kpi.getUnit()}','${kpi.getIsManual()}')"
							class="tooltipped modal-trigger btn-flat" data-position="top" data-tooltip="Edit indicator" ><i class="material-icons">edit</i></a>
							<a style="padding:0 !important" onclick="dashboardParams('${kpi.getIdKPI()}','${kpi.getIdCity()}')" href="#cont" 
							class="tooltipped btn-flat view" data-position="top" data-tooltip="View indicator" ><i class="material-icons eyes" id="icon_${kpi.getIdKPI()}">remove_red_eye</i></a>
 							</td> 
					</c:when>
					
					<c:otherwise>
					<td class="unalabTd">
					<a style="padding:0 !important" href="#modal3" id="btnMio2" onclick="myParamInfo('${kpi.getIdKPI()}','${kpi.getKpiLevel()}','${kpi.getKpiCategory()}','${kpi.getIdCity()}', '${kpi.getName()}','${kpi.getTableId()}','${kpi.getDescription()}','${kpi.getUnit()}','${kpi.getIsManual()}')"
							class="tooltipped modal-trigger btn-flat" data-position="top" data-tooltip="Edit indicator" ><i class="material-icons">edit</i></a>
					<a style="padding:0 !important" onclick="dashboardParams2('${kpi.getIdKPI()}','${kpi.getIdCity()}','${kpi.getUnit()}')" href="#cont" 
							class="tooltipped btn-flat view" data-position="top" data-tooltip="View indicator" ><i class="material-icons eyes" id="icon_${kpi.getIdKPI()}">remove_red_eye</i></a>
					</td>
					
					</c:otherwise>
					</c:choose>
					
					</tr>
					
			</c:forEach>
			
			
				</tbody>
			</table>
			</div>
			
			
			<div id="pag" class="col-md-12 center text-center" style="position:fixed;bottom:30px;left:0;width:100%;">
				<span class="center" id="total_reg"></span>
				<ul class="pagination pager" id="myPager"></ul>
			</div>
		</div>
		
		

	</div>


<!-- <div class="video-container" id="cont" style="display:none;"> -->
<!--         					<iframe width="453" height="180" src="http://unalab.eng.it:80/knowage/public/servlet/AdapterHTTP?ACTION_NAME=EXECUTE_DOCUMENT_ACTION&OBJECT_LABEL=AirQuality_cockpit&TOOLBAR_VISIBLE=true&ORGANIZATION=DEFAULT_TENANT" frameborder="0" allowfullscreen></iframe> -->
<!--       				</div> -->




<!-- 	<footer class="valign-wrapper primary" -->
<!-- 		style="position:fixed;bottom:0;left:0;width:100%;"> -->
<!-- 	<div style="background-color: black; width: 100%; height: 25px;" -->
<!-- 		class="center white-text">&copy; UNALAB project</div> -->
<!-- 	</footer> -->

<footer class="valign-wrapper primary"
		style="position:fixed;bottom:0;left:0;width:100%;">
		<div class="footer-section section-left white-text" style="background-color: black; width: 100%; height: 35px;">&nbsp;</div>
	<div class="footer-section section-center white-text" style="background-color: black; width: 100%; height: 35px;">&copy; UNALAB project</div>
		
		<div class="footer-section section-right white-text" style="background-color: black; width: 100%; height: 35px;"><span>Powered by </span><img class="footer-logo" src="img/digital-enabler-white.png"/></div>
	</footer>

<!-- Modal Structure -->
  <div id="modal1" class="modal">
    <div class="modal-content">
    
<!--     <div class="switch center"> -->
<!--     <label> -->
<!--       Manual Indicator -->
<!--       <input id="manualComputedSwitch" type="checkbox"> -->
<!--       <span class="lever customSwitch"></span> -->
<!--       Computed indicator -->
<!--     </label> -->
<!--   </div> -->
    <div id="manualForm">
      <h5>Add a new manual indicator</h5>

      <div class="row">
        <form action="/cpm/AddAdminKPI" method="post">
        <div class="row modal-form-row">
            <div class="input-field col s12">
              <input id="id" name="id" type="number" class="validate unalabInput" required>
              <label class="unalabLabel" for="id">Id</label>
            </div>
          </div>
          <div class="row modal-form-row">
            <div class="input-field col s12">
              <textarea  id="name" name="name" type="text" class="validate unalabInput materialize-textarea" required></textarea>
              <label for="name">Name</label>
            </div>
          </div>
          <div class="row modal-form-row">
            <div class="input-field col s12">
              <textarea id="description" name="desc" type="text" class="materialize-textarea validate unalabInput" required></textarea>
              <label for="description">Description</label>
            </div>
          </div>   
          <div class="row modal-form-row">
            <div class="input-field col s12">
              <input id="unit" type="text" name="unit" class="validate unalabInput" required>
              <label for="unit">Unit Measure</label>
            </div>
          </div>     
          <div class="row modal-form-row">  
             
			<div class="input-field col s12">
						<input name="city" type="hidden" value="${param.city}"></input> <span>Filter by Indicators level</span> 
						<select
							id="level" name="level">
							<option value="task">Common Indicators</option>
							<option value="city">City-Level Indicators</option>
							<option value="project">Project-Level Indicators</option>
						</select> 
							
							</div>
							
							<div class="input-field col s12">
							<span>Filter by
							Indicators category</span> 
							<select id="cat1" name="cat1">

							<option value="climate">Climate change adaptation and mitigation</option>
							<option value="airquality">Air Quality</option>
							<option value="economic">Economic opportunities and green jobs</option>
							<option value="water">Water management</option>
							<option value="green">Green space management</option>

						</select> 
						<select id="cat2" name="cat2">

							<option value="people">People</option>
							<option value="planet">Planet</option>
							<option value="prosperity">Prosperity</option>
							<option value="governance">Governance</option>
							<option value="propagation">Propagation</option>


						</select>
						</div>
						</div>
						
						 <div class="modal-footer">
   
      <button type="submit" id="submit" name="submit" class=" modal-action modal-close waves-effect waves-green btn-flat" >Add</button>
       <a class=" modal-action modal-close waves-effect waves-red btn-flat" >Cancel</a>
    </div>
						
				</form>
      </div>
      </div>
      
      
<!--       Computed Indicator Adding Form -->
<!--       <div id="computedForm"> -->
<!--       <h5>Add a new computed indicator</h5> -->

<!--       <div class="row"> -->
<!--         <form action="/cpm/AddComputedKPI" method="post"> -->
        
<!--           <div class="row modal-form-row"> -->
          
<!--           <div class="dropdownSelect"> -->
					
         
<!-- 		   <a class='dropdown-trigger btn-flat truncate' href='#' data-target='compKPI'><i class="material-icons right">arrow_drop_down</i> Knowage Indicators</a>  -->
<!--             	<ul id="compKPI"  class='dropdown-content'> -->



<!-- 			</ul> -->
			
<!--           </div> -->
<!--           <span style="padding-left:20px" id="indicatorValue" class="dropdownValue">none</span> -->
<!--           </div> -->
          
<!--           <div class="row modal-form-row"> -->
<!--             <div class="input-field col s12"> -->
<!--               <input id="idComputed" name="id" type="number" class="validate unalabInput" required> -->
<!--               <label class="unalabLabel" for="id">Id</label> -->
<!--             </div> -->
<!--           </div> -->
          
<!--           <div class="row modal-form-row"> -->
<!--             <div class="input-field col s12"> -->
<!--               <textarea  id="nameComputed" name="name" type="text" class="validate unalabInput materialize-textarea" required></textarea> -->
<!--               <label for="name">Name</label> -->
<!--             </div> -->
<!--           </div> -->
          
<!--           <div class="row modal-form-row"> -->
<!--             <div class="input-field col s12"> -->
<!--               <textarea id="descriptionComputed" name="desc" type="text" class="materialize-textarea validate unalabInput" required></textarea> -->
<!--               <label for="description">Description</label> -->
<!--             </div> -->
<!--           </div>    -->
<!--           <div class="row modal-form-row"> -->
<!--             <div class="input-field col s12"> -->
<!--               <input id="unitComputed" type="text" name="unit" class="validate unalabInput" required> -->
<!--               <label for="unit">Unit Measure</label> -->
<!--             </div> -->
<!--           </div>      -->
<!--           <div class="row modal-form-row">   -->
             
<!-- 			<div class="input-field col s12" id="compForm"> -->
						
<%-- 						<input name="city" type="hidden" value="${param.city}"></input>  --%>
<!-- 						<span>Filter by Indicators level</span>  -->
<!-- 						<select -->
<!-- 							id="computedLevel" name="level"> -->
<!-- 							<option value="task">Common Indicators</option> -->
<!-- 							<option value="city">City-Level Indicators</option> -->
<!-- 							<option value="project">Project-Level Indicators</option> -->
<!-- 						</select>  -->
							
<!-- 							</div> -->
							
<!-- 							<div class="input-field col s12"> -->
<!-- 							<span>Filter by -->
<!-- 							Indicators category</span>  -->
<!-- 							<select id="computedCat1" name="cat1"> -->

<!-- 							<option value="climate">Climate change adaptation and mitigation</option> -->
<!-- 							<option value="airquality">Air Quality</option> -->
<!-- 							<option value="economic">Economic opportunities and green jobs</option> -->
<!-- 							<option value="water">Water management</option> -->
<!-- 							<option value="green">Green space management</option> -->

<!-- 						</select>  -->
<!-- 						<select id="computedCat2" name="cat2"> -->

<!-- 							<option value="people">People</option> -->
<!-- 							<option value="planet">Planet</option> -->
<!-- 							<option value="prosperity">Prosperity</option> -->
<!-- 							<option value="governance">Governance</option> -->
<!-- 							<option value="propagation">Propagation</option> -->


<!-- 						</select> -->
<!-- 						</div> -->
<!-- 						</div> -->
						
<!-- 						 <div class="modal-footer"> -->
   
<!--       <button type="submit" id="submitComputed" name="submit" class=" modal-action modal-close waves-effect waves-green btn-flat" >Add</button> -->
<!--        <a class=" modal-action modal-close waves-effect waves-red btn-flat" >Cancel</a> -->
<!--     		</div> -->
						
<!-- 	</form> -->
      </div>
      </div>
      
      
    </div>
   
  </div>
  
  
  <!-- Modal Structure -->
  <div id="modal3" class="modal">
    <div class="modal-content">
      <h5 id="titlemodale" class="title"></h5>

	
		
      <div class="row" id="overLapping">
        <form action="/cpm/ModifyAdminKPI" method="post">
        <div class="row modal-form-row">
            <div class="input-field col s12">
            
              <input id="tableId" name="tableId" type="number" class="validate unalabInput" disabled>
              <label style="margin-top: -20px" class="unalabLabel active"  for="tableId">Id</label>
            </div>
          </div>
       
<!--        <div class="row modal-form-row">  -->
<!--        <div class="input-field col s12">   -->
<!--           <p > -->
<!--       		<label class="tooltipped" data-position="bottom" data-tooltip="By checking this, you set the indicator as computed. You will not be able to edit its value"> -->
      			
<!--         			<input id="checkComputed" type="checkbox" name="setComp" value="automatic"/> -->
<!--         			<span>Computed indicator</span> -->
<!--       		</label> -->
<!--     		</p> -->
<!--     		</div> -->
<!--     	  </div> -->
    	  
    	  
<!--     	  <div id="shown" class="row modal-form-row hide"> -->
          
<!--           <div class="dropdownSelect"> -->
					
         
<!-- 		   <a class='dropdown-trigger btn-flat truncate' href='#' data-target='compKPI2'><i class="material-icons right">arrow_drop_down</i> Knowage Indicators</a>  -->
<!--             	<ul id="compKPI2"  class='dropdown-content'> -->



<!-- 			</ul> -->
			
<!--           </div> -->
<!--           <span style="padding-left:20px" id="indicatorValue2" class="dropdownValue">none</span> -->
<!--           </div> -->
          
          
          <div class="row modal-form-row">
            <div class="input-field col s12">
              <textarea  id="nameAdminEdit" name="name" type="text" class="validate unalabInput materialize-textarea" required></textarea>
              <label style="margin-top: -20px" class="unalabLabel active" for="name">Name</label>
            </div>
          </div>
          <div class="row modal-form-row">
            <div class="input-field col s12">
              <textarea id="descAdminEdit" name="desc" type="text" class="materialize-textarea validate unalabInput" required></textarea>
              <label style="margin-top: -20px" class="unalabLabel active" for="description">Description</label>
            </div>
          </div>   
          <div class="row modal-form-row">
            <div class="input-field col s12">
              <input id="unitAdminEdit" type="text" name="unit" class="validate unalabInput" required>
              <label style="margin-top: -20px" class="unalabLabel active" for="unit">Unit Measure</label>
            </div>
          </div> 
          <c:out value="${param['id']}"></c:out>
	    
		
		<input type="hidden" name="city" />
		<input type="hidden" name="level" />
		<input type="hidden" name="category" />
		<input type="hidden" name="KPI_id" />
		
		<div class="modal-footer">
   
      <button type="submit" id="submitAdminEdit" name="submit" class=" modal-action modal-close waves-effect waves-green btn-flat" >Confirm</button>
       <a class=" modal-action modal-close waves-effect waves-red btn-flat" >Cancel</a>
    </div>       
       
          </form>
          
          </div>
          </div>
          </div>   
           
  <div class="z-depth-1" style="margin:20px">
  <div id="cont"  style="display:none" >
        <iframe id="cockpit" width="100%" height="600" src="test" frameborder="0"></iframe>
      </div>
      
      </div>
      
      
      
      
      
      
      
  <!-- Modal Structure -->
  <div id="modal2" class="modal">
    <div class="modal-content">
      <h5 class="title"></h5>

      <div class="row">
        <form action="/cpm/EditKPI" method="post">
          <div class="row modal-form-row" id="modal-body">
          
          <c:out value="${param['id']}"></c:out>
	    <input type="hidden" name="title" />
		<input type="hidden" name="KPI_id" />
		<input type="hidden" name="city" />
		<input type="hidden" name="level" />
		<input type="hidden" name="category" />
		<input type="hidden" name="tableId" />
          </div>
          <div class="row">
            <div class="input-field col s12">
            
              <input class="unalabInput" id="value" name="value" type="number" required>
              <label for="value">Value</label>
            </div>
          </div>
          
          <div class="modal-footer">
   
      <button type="submit" id="submit" name="submit" class=" modal-action modal-close waves-effect waves-green btn-flat" >Set</button>
       <a class=" modal-action modal-close waves-effect waves-red btn-flat" >Cancel</a>
    </div>       
       </form>
      </div>
    </div>
    
  </div>

</body>
</html>