<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="main.java.KPIBean"%>
<%@page import="java.util.ArrayList"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<% ArrayList<KPIBean> kpiList = new ArrayList<KPIBean>();
  kpiList = (ArrayList<KPIBean>) request.getAttribute("kpiList");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta charset="ISO-8859-1">

<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<title>City Performance Monitor</title>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">


<link type="text/css" rel="stylesheet" href="css/custom.css"
	media="screen,projection" />
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/css/materialize.min.css">
<script type="text/javascript"
	src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/js/materialize.min.js"></script>

<script type="text/javascript" src="js/pagination.js"></script>
<script type="text/javascript" src="js/table-sorter.js"></script>


<style>
[type="radio"]:checked + span:after,
[type="radio"].with-gap:checked + span:before,
[type="radio"].with-gap:checked + span:after {
  border: 2px solid #ef5350;
}

[type="radio"]:checked + span:after,
[type="radio"].with-gap:checked + span:after {
  background-color: #ef5350 !important;
}
</style>

</head>
<body style="background-color: rgba(119, 148, 62, 0.2)">
	<%-- <c:import url="/GetKPIList2" /> --%>
	<nav id="header">
	<div class="nav-wrapper white">
		<a href="index.jsp" class="brand-logo"><img
			src="img/logo_alpha_270x.png" class="logo" alt="UNaLab logo"
			style="width: 42%"></a> <span class="brand-logo secondary-text"
			style="margin-left: 130px; margin-top: 12px; color: #77943E">City Perfomance Monitor</span>



		<ul class="right">
			<li class="right"><a class="username dropdown-button dropdown-user-big" href="/cpm/login.jsp"><i
					class="material-icons left">input</i><span class="val">Login</span> </a></li>
		</ul>
	</div>
	</nav>

    <div class="container">	
				<h4 class="center">City Perfomance Monitor</h4>
				
				
				
				<p class="center-align">The City Performance Monitor, developed under the UNaLab project, makes the stakeholders aware about the urban conditions allowing them 
				</p>
				
				<p class="center-align">to visualize and discuss social and environmental performance indicators.
				</p>
				
				<h5 class="center" style="margin-top:50px">Select a city to see the indicators</h5>
				

<!-- 				  <div class="row"> -->
				  
<!-- 				    <div class="col s12 m4"> -->
<!-- 				     <a href="/cpm/GetCommonKPIs?city=3"> -->
<!-- 				      <div class="card citycard"> -->
<!-- 				      <div class="card-content" style="padding:10px"> -->
<!-- 				          <p class="center-align"> -->
<!-- 						    <label> -->

<!-- 						      <span style = "font-size: 14px; color: black">See Eindhoven indicators</span> -->
<!-- 						    </label> -->
<!-- 						  </p> -->
<!-- 				        </div> 		         -->
<!-- 						<div class="card-image"> -->
<!-- 				          <img class="img-card" src="img/Eindhoven.jpg"> -->
<!-- 				          <span class="card-title citycardtitle" style="left: 15px;"><b>Eindhoven</b></span> -->
<!-- 				        </div> -->
<!-- 				        <div class="card-content" style="padding:10px"> -->
				          
<!-- 				        </div> 		      -->
				        
<!-- 				      </div> -->
<!-- 				      </a> -->
<!-- 				    </div> -->
				    
				    
				    				    
<!-- 				    <div class="col s12 m4"> -->
<!-- 				    <a href="/cpm/GetCommonKPIs?city=1"> -->
<!-- 				      <div class="card citycard"> -->
<!-- 				      <div class="card-content" style="padding:10px"> -->
<!-- 				          <p class="center-align"> -->
<!-- 						    <label> -->

<!-- 						      <span style = "font-size: 14px; color: black">See Genova indicators</span> -->
<!-- 						    </label> -->
<!-- 						  </p> -->
<!-- 				        </div> -->
<!-- 				        <div class="card-image"> -->
<!-- 				          <img class="img-card" src="img/Genova.jpg"> -->
<!-- 				          <span class="card-title citycardtitle"><b>Genova</b></span> -->
<!-- 				        </div> -->
<!-- 				        <div class="card-content" style="padding:10px"> -->
				         
<!-- 				        </div> -->
<!-- 				      </div> -->
<!-- 				      </a> -->
<!-- 				    </div> -->
				    
<!-- 				    <div class="col s12 m4"> -->
<!-- 				    <a href="/cpm/GetCommonKPIs?city=2"> -->
<!-- 				      <div class="card citycard"> -->
				        
<!-- 				        <div class="card-content" style="padding:10px;"> -->
<!-- 				          <p class="center-align"> -->
<!-- 						    <label> -->

<!-- 						      <span style = "font-size: 14px; color: black">See Tampere indicators</span> -->
<!-- 						    </label> -->
<!-- 						  </p> -->
<!-- 				        </div> -->
<!-- 				        <div class="card-image"> -->
<!-- 				          <img class="img-card" src="img/Tampere.jpg"> -->
<!-- 				          <span class="card-title citycardtitle" style="left: 15px;"><b>Tampere</b></span> -->
<!-- 				        </div> -->
				        
<!-- 				        <div class="card-content" style="padding:10px;"> -->
				          
<!-- 				      </div> -->
<!-- 				      </a> -->
<!-- 				    </div> -->
				    
<!-- 				 </div> -->
				 
				  <div class="row">
						  
						    <div class="col s12 m4">
						    <a href="/cpm/GetCommonKPIs?city=3"> 
						      <div class="card">
						        <div class="card-image">
						          <img src="img/Eindhoven.jpg">
						          <span class="card-title">Eindhoven</span>
						        </div>
						        <div class="card-content">
						          <p>
								    <label>
								      <input class="with-gap" name="city" type="radio"  value="eindhoven" />
								      <span>See Eindhoven indicators</span>
								    </label>
								  </p>
						        </div>
						      </div>
						    </div>
						    				    
						    <div class="col s12 m4" >
						    <a href="/cpm/GetCommonKPIs?city=1"> 
						      <div class="card">
						        <div class="card-image">
						          <img src="img/Genova.jpg">
						          <span class="card-title">Genova</span>
						        </div>
						        <div class="card-content">
						          <p>
								    <label>
								      <input class="with-gap" name="city" type="radio" value="genova" disabled/>
								      <span>See Genova indicators</span>
								    </label>
								  </p>
						        </div>
						      </div>
						    </div>
						    				    
						    <div class="col s12 m4">
						    <a href="/cpm/GetCommonKPIs?city=2"> 
						      <div class="card" >
						        <div class="card-image">
						          <img src="img/Tampere.jpg">
						          <span class="card-title">Tampere</span>
						        </div>
						        <div class="card-content">
						          <p>
								    <label>
								      <input class="with-gap" name="city" type="radio" value="tampere" disabled/>
								      <span>See Tampere indicators</span>
								    </label>
								  </p>
						        </div>
						      </div>
						    </div>
						    
						 </div>
				 
				 
				 <div class="row center-align">
			
<!-- 			<button type="submit" id="submit" name="submit" -->
<!-- 						class="waves-effect waves-light btn hoverable unalabGreen"> -->
<!-- 						View KPIs -->
<!-- 				</button> -->
			</div>
<!-- 			   </form> -->
			</div>
			
			
			<div class="spacer"></div>
		</div>

<!-- 	<footer class="valign-wrapper primary" -->
<!-- 		style="position:fixed;bottom:0;left:0;width:100%;"> -->
<!-- 	<div style="background-color: black; width: 100%; height: 25px;" -->
<!-- 		class="center white-text">&copy; UNALAB project</div> -->
<!-- 	</footer> -->

<footer class="valign-wrapper primary"
		style="position:fixed;bottom:0;left:0;width:100%;">
		<div class="footer-section section-left white-text" style="background-color: black; width: 100%; height: 35px;">&nbsp;</div>
	<div class="footer-section section-center white-text" style="background-color: black; width: 100%; height: 35px;">&copy; UNALAB project</div>
		
		<div class="footer-section section-right white-text" style="background-color: black; width: 100%; height: 35px;"><span>Powered by </span><img class="footer-logo" src="img/digital-enabler-white.png"/></div>
	</footer>


<script>

$(document).ready(function(){
	
    $("#cityForm").on("change", "input:radio", function(){
    	console.log("sono dentro");
        $("#cityForm").submit();
    });
});

</script>

</body>

</html>

