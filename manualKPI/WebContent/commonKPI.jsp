<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="main.java.KPIBean"%>
<%@page import="main.bean.LoginBean"%>
<%@page import="java.util.ArrayList"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<% ArrayList<KPIBean> kpiList = new ArrayList<KPIBean>();
  kpiList = (ArrayList<KPIBean>) request.getAttribute("kpiList");
%>


<%
String role = (String) request.getSession().getAttribute("user");

%>
<!-- Page Layout here -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta charset="ISO-8859-1">
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>City Performance Monitor</title> 
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet" />


<link type="text/css" rel="stylesheet" href="css/custom.css"
	media="screen,projection" />
<link type="text/css" rel="stylesheet" href="css/common.css"
	media="screen,projection" />
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/css/materialize.min.css" />
<!-- <link type="text/css" rel="stylesheet" href="css/materialize.css" media="screen,projection" />  -->
<script type="text/javascript"
	src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/js/materialize.min.js"></script>

<script type="text/javascript" src="js/pagination.js"></script>
<script type="text/javascript" src="js/table-sorter.js"></script>
<script type="text/javascript" src="js/common.js"></script>

<script type="text/javascript" src="js/paginathing.js"></script>
</head>

<body>

<c:set var="city" value="${param.city}" />
<c:set var="cat" value="${param.categories}" />

<% if(role != null) {
	%>
<nav id="header">
	<div class="nav-wrapper white">
		<a href="index.jsp" class="brand-logo"><img
			src="img/logo_alpha_270x.png" class="logo" alt="UNaLab logo"
			style="width: 42%"></a> <span class="brand-logo secondary-text"
			style="margin-left: 130px; margin-top: 12px; color: #77943E; font-size: 2.1rem !important;">City Perfomance Monitor</span>



		<ul class="right">
		<% if (role == "admin"){%>
			<li><a href="/cpm/dashboard.jsp?city=Eindhoven" class="grey-text text-darken-4">City Dashboard</a></li>
			<li><a href="/cpm/GetAdminKPIList?city=3&level=task&category=all" class="grey-text text-darken-4">Expert View</a></li>
			
		<%} else if (role == "eindhoven_city_manager"){%>		
			<li><a href="/cpm/dashboard.jsp?city=Eindhoven" class="grey-text text-darken-4">City Dashboard</a></li>
			<li><a href="/cpm/GetKPIList?city=3&level=task&category=all" class="grey-text text-darken-4">Expert View</a></li>
		<%} else if (role == "genova_city_manager") {%>
			<li><a href="/cpm/dashboard.jsp?city=Genova" class="grey-text text-darken-4">City Dashboard</a></li>
			<li><a href="/cpm/GetKPIList?city=1&level=task&category=all" class="grey-text text-darken-4">Expert View</a></li>
		<%} else if (role == "tampere_city_manager") { %>
			<li><a href="/cpm/dashboard.jsp?city=Tampere" class="grey-text text-darken-4">City Dashboard</a></li>
			<li><a href="/cpm/GetKPIList?city=2&level=task&category=all" class="grey-text text-darken-4">Expert View</a></li>
		<% } %>
			<li class="right">
			<a class="username dropdown-button dropdown-user-big" href="#" data-target="userdropdown_big">
			<i class="material-icons left">perm_identity</i>
			<%if (role == "eindhoven_city_manager"){%>
				<span class="val">eindhoven-admin</span> 
			<% } else if (role == "genova_city_manager") { %>
				<span class="val">genova-admin</span> 
			<% } else if(role == "tampere_city_manager") {%>
				<span class="val">tampere-admin</span> 
			<%} else if(role == "admin") {%>
				<span class="val">cpm-admin</span>
			<%} %>			
			<i class="material-icons right">arrow_drop_down</i>
			</a>
			<ul id="userdropdown_big"  class='dropdown-content'>

				<li><a href="/cpm/LogoutServlet">Logout</a></li>

			</ul>
			</li>
		</ul>
	</div>
	</nav>
<% } else{ %>

<nav id="header">
	<div class="nav-wrapper white">
		<a href="index.jsp" class="brand-logo"><img
			src="img/logo_alpha_270x.png" class="logo" alt="UNaLab logo"
			style="width: 42%"></a> <span class="brand-logo secondary-text"
			style="margin-left: 130px; margin-top: 12px; color: #77943E">City Perfomance Monitor</span>



		<ul class="right">
		<li><a href="/cpm/dashboard.jsp?city=${param.city}" class="grey-text text-darken-4">City Dashboard</a></li>
		<li><a href="/cpm/GetGuestKPIList?city=${param.city}&&level=task&category=all" class="grey-text text-darken-4">Expert View</a></li>
		
			
		<li class="right"><a class="username dropdown-button dropdown-user-big" href="/cpm/login.jsp"><i
					class="material-icons left">input</i><span class="val">Login</span> </a></li>
		</ul>
	</div>
	</nav>
	
	<%} %>

<div id="container">

 <div id="transitCard" class="hide"></div>
 
  <div class="row">
    <div class="col s12 m12 l3">
      
        <div class="nav-wrapper">
          <form>
           
             <input class="unalabInput" type="text" id="search" placeholder="Type to search..." />
            
          </form>
        </div>
      
      
       
      
<!--       <form action="javascript:pippo();" id="filterForm"> -->
     
     
		<input type="hidden" name="city" class="city" value="${param.city}"></input>     
       <ul class="collapsible" data-collapsible="expandable"> 

        <li>
          <div class="collapsible-header active"><i class="material-icons">label</i>Indicators category</div>
          <div class="collapsible-body filter-container">

            <ul>
              <li>
                <input type="checkbox" name="event-type-filter" class="event-type-filter filled-in" checked="checked" id="cloud_queue" value="climate" />
                <label for="cloud_queue"> <i class="material-icons small left" style="font-size:1.5rem">cloud_queue</i> Climate change adaptation and mitigation</label>
              </li>
              <li>
                <input type="checkbox" name="event-type-filter" class="event-type-filter  filled-in" checked="checked" id="warning" value="airquality"  />
                <label for="warning"> <i class="material-icons small left" style="font-size:1.5rem">warning</i>Air Quality</label>
              </li>
              <li>
                <input type="checkbox" name="event-type-filter" class="event-type-filter filled-in" checked="checked" id="euro_symbol" value="economic"/>
                <label for="euro_symbol"><i class="material-icons small left" style="font-size:1.5rem">euro_symbol</i> Economic opportunities and green jobs</label>
              </li>
              <li>
                <input type="checkbox" name="event-type-filter" class="event-type-filter filled-in" checked="checked" id="waves" value="water" />
                <label for="waves"><i class="material-icons small left" style="font-size:1.5rem">waves</i>Water management</label>
              </li>
              <li>
                <input type="checkbox" name="event-type-filter" class="event-type-filter filled-in" checked="checked" id="nature_people" value="green" />
                <label for="nature_people"><i class="material-icons small left" style="font-size:1.5rem">nature_people</i>Green space management</label>
              </li>
<!--               <li> -->
<!--               <button type='submit' id="filter" name='btn_login' class='btn btn-small waves-effect unalabGreen'>Filter indicators</button> -->
<!--               </li> -->
            </ul>
            
            
            
         

          </div>
        </li>
      </ul>
<!-- </form> -->
    </div>

    <!-- Cards container -->
    

<!--       <h4 class="center">Citizen View</h4> -->
      
   
        <div id="pag-card" class="col s12 m12 l9">
        
      <c:set var="mykpi" value="${requestScope.kpiList}" /> 

	<c:forEach var="kpi" items= "${mykpi}" varStatus="i">
		<c:set var="isManual" value="${kpi.getIsManual()}"></c:set>
        <div class="col s12 m6 l3">
          <!-- Card 1 -->
          <div  class="card small ${kpi.getIcon()}Class">
            <div class="card-content white-text">
            
            <c:choose>
			<c:when test = "${isManual == 'manual'}">
            <div class="card__date">
                <span class="card__date__day"><a style="padding:0 !important" onclick="dashboardParams('${kpi.getIdKPI()}','${kpi.getIdCity()}')" href="#cont" 
							class="tooltipped btn-flat unalabText view" data-position="top" data-tooltip="View indicator" ><i class="material-icons eyes" id="icon_${kpi.getIdKPI()}">remove_red_eye</i></a></span>
                
              </div>
             
            </c:when>
            
            <c:when test = "${isManual == 'citymanual'}">
            <div class="card__date">
                <span class="card__date__day"><a style="padding:0 !important" onclick="dashboardCityParams('${kpi.getIdKPI()}','${kpi.getIdCity()}')" href="#cont" 
							class="tooltipped btn-flat unalabText view" data-position="top" data-tooltip="View indicator" ><i class="material-icons eyes" id="icon_${kpi.getIdKPI()}">remove_red_eye</i></a></span>
                
              </div>
             
            </c:when>
            <c:otherwise>
            
            
            <div class="card__date">
                <span class="card__date__day"><a style="padding:0 !important" onclick="dashboardParams2('${kpi.getIdKPI()}','${kpi.getIdCity()}','${kpi.getUnit()}')" href="#cont" 
							class="tooltipped btn-flat unalabText view" data-position="top" data-tooltip="View indicator" ><i class="material-icons eyes" id="icon_${kpi.getIdKPI()}">remove_red_eye</i></a></span>
                
              </div>
             
            
            </c:otherwise>
            </c:choose>    
              <div class="icon-block ">
              <h4 class="unalabText"><i class="material-icons medium left">${kpi.getIcon()}</i></h4>
              </div>
         	  <span class="unalabOrange card-info-custom" style="display:inline; font-size:32px; line-height:2">	${kpi.getValue()}</span>
              <span class="unalabOrange card-title" style="font-size: 14px !important">&nbsp;${kpi.getUnit()}</span>
              <span class="card-title unalabText" id="title" style="font-size: 15px !important">${kpi.getName()} <a style="padding:0 !important" 
							class="tooltipped btn-flat unalabText view" data-position="top" data-tooltip="${kpi.getDescription()}" ><i class="material-icons">info</i></a></span>

<%--               <p class="card-subtitle grey-text text-darken-2 ">${kpi.getDescription()}</p> --%>
              
              
              
            </div>
           
          </div>
          <!-- End of card -->
        </div>
         </c:forEach> 
         
         </div>
        
        
        
        <div id="cont" style="display:none;">
        <iframe id="cockpit" width="100%" height="600" src="test" frameborder="0"></iframe>
       </div>

     
     
    </div>
   </div>
    
<!--     <footer class="valign-wrapper primary" -->
<!-- 		style="position:fixed;bottom:0;left:0;width:100%;"> -->
<!-- 	<div style="background-color: black; width: 100%; height: 25px;" -->
<!-- 		class="center white-text">&copy; UNALAB project</div> -->
<!-- 	</footer> -->

<footer class="valign-wrapper primary"
		style="position:fixed;bottom:0;left:0;width:100%;">
		<div class="footer-section section-left white-text" style="background-color: black; width: 100%; height: 35px;">&nbsp;</div>
	<div class="footer-section section-center white-text" style="background-color: black; width: 100%; height: 35px;">&copy; UNALAB project</div>
		
		<div class="footer-section section-right white-text" style="background-color: black; width: 100%; height: 35px;"><span>Powered by </span><img class="footer-logo" src="img/digital-enabler-white.png"/></div>
	</footer>
    
<!--     <div class="container"> -->
<!--         <div id="pag-card"></div> -->
<!--         <div id="pagination-short"></div> -->
<!--     </div> -->
  
  
  <script>
  $(document).ready(function(){
          $("#pag-card").paginathing({
              
              perPage: 12
              
          });

	

      });
  
  
  </script>
  
  <script>
function dashboardParams(idKpi, city){
	var id = idKpi;
	console.log(id);
	var c = city;
	var dashboard = "http://unalab.eng.it:80/knowage/public/servlet/AdapterHTTP?ACTION_NAME=EXECUTE_DOCUMENT_ACTION&OBJECT_LABEL=manualKPI2&TOOLBAR_VISIBLE=true&ORGANIZATION=DEFAULT_TENANT&NEW_SESSION=true&PARAMETERS=kpi="+id+"%26city="+c;
	
	console.log(dashboard);
	$("#cockpit").attr("src",dashboard);
	console.log($("#cockpit").attr("src",dashboard));
	if($("#icon_"+id).text()==="remove_red_eye"){
		$(".eyes").text("remove_red_eye");
		$("#icon_"+id).text("visibility_off");
		$("#cont").show();
		$("#pag").hide();
	}
	else{
		$("#icon_"+id).text("remove_red_eye");
		$("#cont").hide();
		$("#pag").show();
	}
	
	
	
}


function dashboardCityParams(idKpi, city){
	var id = idKpi;
	console.log(id);
	console.log("CITY FROM MANUAL CITYMANAGER" + city );
	var c = city;
	var dashboard="";
	switch(c){
	case "1":
		dashboard="";
		console.log(dashboard);
		break;
	case "2":
		dashboard="";
		console.log(dashboard);
		break;
	case "3":
		dashboard = "http://unalab.eng.it:80/knowage/public/servlet/AdapterHTTP?ACTION_NAME=EXECUTE_DOCUMENT_ACTION&OBJECT_LABEL=EINmanualKPI&TOOLBAR_VISIBLE=true&ORGANIZATION=DEFAULT_TENANT&NEW_SESSION=TRUE&PARAMETERS=kpi="+id;
		console.log(dashboard);
		break;
	}
	
	
	console.log(dashboard);
	$("#cockpit").attr("src",dashboard);
	console.log($("#cockpit").attr("src",dashboard));
	if($("#icon_"+id).text()==="remove_red_eye"){
		$(".eyes").text("remove_red_eye");
		$("#icon_"+id).text("visibility_off");
		$("#cont").show();
		$("#pag").hide();
	}
	else{
		$("#icon_"+id).text("remove_red_eye");
		$("#cont").hide();
		$("#pag").show();
	}
	
}

function getCityName(id_city){
	
	switch(id_city){
	case 1:
		cityString = "Genova";
		break;
	case 2:
		cityString = "Tampere";
		break;
	case 3:
		default:
		cityString = "Eindhoven";
		break;
		
	}
}

function dashboardParams2(idKpi, city, unit){
	var id = idKpi;
	var c = city;
	//var url = new URL(window.location.href);
	//var url_params = new URLSearchParams(url.search);
	//var city2 = url_params.get('city');
	
	console.log("CITY FROM: " + c);
	console.log("ID KPI: " +id);
	
	console.log("CITY ID: "+ c);
	var input = new Object();
	input['idkpi'] = id;
	input['city'] = c;
	
	
	
	
	$.ajax({
		async: false,
        type: "POST",
        url: "GetIdKnowage",
        
        data: input,
        
        success: function(id){ 
        	    console.log("ID ID ID " + id);
        		var idKnowage = id;
        		console.log("idKnowage:" + idKnowage);
        		
        		console.log("CITY " + c);
        		$.ajax({
    				async: false,
    				type: "POST",
    				url: "GetIdCityKnowage",
    				data: input,
    				
    				success: function(city_category){
    					var cityKnowage =  city_category;
    					console.log("city knowage: " + cityKnowage);
    					getCityName(c);
    					console.log("CITTA TROVATA: " + cityString);
    					http://unalab.eng.it/knowage/public/servlet/AdapterHTTP?ACTION_NAME=EXECUTE_DOCUMENT_ACTION&OBJECT_LABEL=ComputedKPI&TOOLBAR_VISIBLE=true&ORGANIZATION=DEFAULT_TENANT&PARAMETERS=kpi=8&kpi_field_visible_description=8&city=251&city_field_visible_description=251&cityname=Eindhoven&cityname_field_visible_description=Eindhoven&unitm=ppm&unitm_field_visible_description=ppm
    					var dashboard = "http://unalab.eng.it:80/knowage/public/servlet/AdapterHTTP?ACTION_NAME=EXECUTE_DOCUMENT_ACTION&OBJECT_LABEL=ComputedKPI&TOOLBAR_VISIBLE=true&ORGANIZATION=DEFAULT_TENANT&SBI_LANGUAGE=en&SBI_COUNTRY=US&NEW_SESSION=true&PARAMETERS=kpi="+idKnowage+"%26city="+cityKnowage + "%26cityname="+cityString+"%26unitm="+unit;
    					console.log("DASHBOARD: " + dashboard);
    					
    					$("#cockpit").attr("src",dashboard);
    					console.log($("#cockpit").attr("src",dashboard));
    					if($("#icon_"+id).text()==="remove_red_eye"){
    						$(".eyes").text("remove_red_eye");
    						$("#icon_"+id).text("visibility_off");
    						$("#cont").show();
    						$("#pag").hide();
    					}
    					else{
    						$("#icon_"+id).text("remove_red_eye");
    						$("#cont").hide();
    						$("#pag").show();
    					}
    					}
    				});

        }
        
	});

	
	
}


</script>
  
  <script>
 
  $('.event-type-filter').on("change", function(){
	  
	  $("#pag-card").paginathing({
          perPage: 99999999999
      });
   	  $('.pagination').hide();
	  
	  var id= $(this).prop('id');
	  console.log(id+" "+$(this).prop("checked"));
	  
	  
		if($(this).prop("checked")){
			console.log("IF")
			
			var cardCol = $("#transitCard ."+id+"Class").parent().detach();
			cardCol.prependTo("#pag-card");
			
			console.log("CARD COL "+cardCol);
			console.log(cardCol);
			console.log("IF end")
		}  
		else{
			console.log("else")
// 			$("."+id+"Class").parent().css("display", "none");
			var cardCol = $("."+id+"Class").parent().detach();
			cardCol.appendTo("#transitCard");
			
			console.log("else end")
		}
		
// 		$('#pag-card').empty();
		$("#pag-card").paginathing({
            perPage: 12
        });
		//$('.pagination').show();
  });
  
  
//   function pippo(){
	  

// 	  console.log("FILTER FORM");
// 	  	  var inputFilter =  $('.event-type-filter');
// 	  	  var filters = [];
// 	  	  var filter;
// 	  	inputFilter.each(function( index, element ) {
	  		
// 	  		if($(this).prop('checked')){
// 	  			console.log("--- "+$(this).val());
// 	  			filter = $(this).val();
// 	  			filters.push(filter);
	  			
// 	  		}
	  		
// 	  	});
// 	  	console.log("FILTERS: " + filters);
// 	  	filter = JSON.stringify(filters);
// 	  	console.log("STRINGIFY ARRAY: "+ filter);
// 	  	var input = new Object();
// 	  	input['categories'] = filter;
// 	  	input['city'] = $('.city').val();
// 	  	console.log("INPUT categories: " + input['categories']);
// 	  	console.log("INPUT city: " + input['city']);
	  	
// 	  	$.ajax({
//          type: 'post',
//          url: 'GetFilteredIndicators',
//          dataType: 'json',
//          data: input,//this is my servlet
         
//          success: function(json){      
//        	  //alert('successful');
//        	  console.log(json);
//        	  //$('#cardbuild').empty();
//        	$('#pag-card').empty();
//        	$('.pagination').hide();
//        	//$('#pag-card').hide();
//         	json.forEach(function(x, i) {
        		
        		
//             var stat = json[i];
            
           
           
            
//             var div1 = $('<div class="col s12 m6 l3">').appendTo($('#pag-card'));
            
//             div1 = $('<div  class="card small">').appendTo(div1);
//             div1 = $('<div class="card-content white-text">').appendTo(div1);
//             var div31 = $('<div class="card__date"></div>').appendTo(div1);
//             if(stat.isManual === "manual"){
//             var span22=$('<span class="card__date__day"><a style="padding:0 !important" id="#action" href="#cont" onclick="dashboardParams('+stat.idKPI+','+stat.idCity+')" class="tooltipped btn-flat unalabText view" data-position="top" data-tooltip="View indicator"><i class="material-icons">remove_red_eye</i></a></span>').appendTo(div31);
//             }
//             else if(stat.isManual=="automatic"){
//             	var span22=$('<span class="card__date__day"><a style="padding:0 !important" id="#action" href="#cont" onclick="dashboardParams2('+stat.idKPI+','+stat.idCity+')" class="tooltipped btn-flat unalabText view" data-position="top" data-tooltip="View indicator"><i class="material-icons">remove_red_eye</i></a></span>').appendTo(div31);
//             }
//             else{
//             	var span22=$('<span class="card__date__day"><a style="padding:0 !important" id="#action" href="#cont" onclick="dashboardCityParams('+stat.idKPI+','+stat.idCity+')" class="tooltipped btn-flat unalabText view" data-position="top" data-tooltip="View indicator"><i class="material-icons">remove_red_eye</i></a></span>').appendTo(div31);
//             }
            
//             var div2 = $('<div class="icon-block ">').appendTo(div1);
//             var div3 = $('<h4 class="unalabText"></h4>').appendTo(div2);
//             var i=$('<i class="material-icons medium left"></i>').appendTo(div3);
//             i.text(stat.icon);
            
//             div3 = $('</div>').appendTo(div3);
            
//             var span2 = $('<span class="unalabOrange card-info-custom" style="display:inline; font-size:32px; line-height:2" &nbsp>').appendTo(div1);
//             span2.text(" " + stat.value);
//             //span2.text(stat.unit);
//             span2 = $('</span>').appendTo(span2);
            
//             var span8 = $('<span class="unalabOrange card-title" style="font-size: 14px !important">').appendTo(div1);
//             span8.text("  " + stat.unit);
//             //span2.text(stat.unit);
//             span8 = $('</span>').appendTo(span8);
            
            
            
//             var span3 = $('<span class="card-title unalabText" style="font-size: 15px !important"></span>').appendTo(div1);
//             span3.text(stat.name);
//             var spanAction = $('<a style="padding:0 !important" href="#cont" class="tooltipped btn-flat unalabText view" data-position="top"></a>').appendTo(span3);
//             spanAction.prop('data-tooltip', stat.description);
//             var spanna = $('<i class="material-icons">info</i>').appendTo(spanAction);
            
// //             $('#action').delegated("click", function() {
// //     			dashboardParams(stat.idKPI,stat.idCity);
// //     			console.log("mostrati");
// //     		});
            
//             //span3 = $('</span>').appendTo(span3);
            
            
//             div2 = $('</div>').appendTo(div2);
//             div1 = $('</div>').appendTo(div1);
//             div1 = $('</div>').appendTo(div1);

//         });
//         	$("#pag-card").paginathing({
                
//                 perPage: 12
                
//             });
        	
//         	$(document).ready(function(){
//         	    $('.tooltipped').tooltip();
//         	  });
        	   
        	
//     }
         
//      });
//      return false;

	  	
// 	  	};
  

</script>
  
  <script>
$(document).ready(function(){
$("#search").on("keyup", function() {
    var value = $(this).val();
//     if(value!=""){
//     	console.log("page hide")
//     	$('#pag').hide();
//     }
//     else{
//     	console.log("page show")
//     	$('#pag').empty();
//     	$('#pag').show();
//     }
    console.log("VALUE: " + value);
    
    var set =  $(".card");
    var length = set.length;
    //console.log("ciao");
    set.each(function(index){
        if (index != -1 && index < length) {

		   var id = $(this).find("#title").text();
    		
    		
    		
            
           if(value!=""){
        	   $('#pag').hide();
        	   if (id.toLowerCase().indexOf(value.toLowerCase()) == -1) {
           		$(this).parent().hide();
           		} 
           		else {
           			console.log(id);
            		
           		$(this).parent().show();
           		}
           }
           else if (value=="") {
        	  		 $('#pag').empty();
           			$('#pag').show();
        			
        			$("#pag-card").paginathing({
        	              
        	              perPage: 12
        	              
        	          });
        }
        
        
    }
});

});
});



</script>
  
  </body>
</html>