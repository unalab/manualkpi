var idKnowage;
var jsonCalculated;
var value;
var value2;
var idKnowage2;
var city;
var cityString = "";

function dashboardParams(idKpi, city){
	var id = idKpi;
	console.log(id);
	console.log("CITY FROM MANUAL " + city );
	var c = city;
	var dashboard = "http://unalab.eng.it:80/knowage/public/servlet/AdapterHTTP?ACTION_NAME=EXECUTE_DOCUMENT_ACTION&OBJECT_LABEL=manualKPI2&TOOLBAR_VISIBLE=true&ORGANIZATION=DEFAULT_TENANT&SBI_LANGUAGE=en&SBI_COUNTRY=US&NEW_SESSION=true&PARAMETERS=kpi="+id+"%26city="+c;
	
	console.log(dashboard);
	$("#cockpit").attr("src",dashboard);
	console.log($("#cockpit").attr("src",dashboard));
	if($("#icon_"+id).text()==="remove_red_eye"){
		$(".eyes").text("remove_red_eye");
		$("#icon_"+id).text("visibility_off");
		$("#cont").show();
		$("#pag").hide();
	}
	else{
		$("#icon_"+id).text("remove_red_eye");
		$("#cont").hide();
		$("#pag").show();
	}
	
}


function dashboardCityParams(idKpi, city){
	var id = idKpi;
	console.log(id);
	console.log("CITY FROM MANUAL CITYMANAGER" + city );
	var c = city;
	var dashboard="";
	switch(c){
	case "1":
		dashboard="http://unalab.eng.it:80/knowage/public/servlet/AdapterHTTP?ACTION_NAME=EXECUTE_DOCUMENT_ACTION&OBJECT_LABEL=GENmanualKPI&TOOLBAR_VISIBLE=true&ORGANIZATION=DEFAULT_TENANT&NEW_SESSION=TRUE&PARAMETERS=kpi="+id;
		console.log(dashboard);
		break;
	case "2":
		dashboard="http://unalab.eng.it:80/knowage/public/servlet/AdapterHTTP?ACTION_NAME=EXECUTE_DOCUMENT_ACTION&OBJECT_LABEL=TAMmanualKPI&TOOLBAR_VISIBLE=true&ORGANIZATION=DEFAULT_TENANT&NEW_SESSION=TRUE&PARAMETERS=kpi="+id;
		console.log(dashboard);
		break;
	case "3":
		dashboard = "http://unalab.eng.it:80/knowage/public/servlet/AdapterHTTP?ACTION_NAME=EXECUTE_DOCUMENT_ACTION&OBJECT_LABEL=EINmanualKPI&TOOLBAR_VISIBLE=true&ORGANIZATION=DEFAULT_TENANT&NEW_SESSION=TRUE&PARAMETERS=kpi="+id;
		console.log(dashboard);
		break;
	}
	
	
	console.log(dashboard);
	$("#cockpit").attr("src",dashboard);
	console.log($("#cockpit").attr("src",dashboard));
	if($("#icon_"+id).text()==="remove_red_eye"){
		$(".eyes").text("remove_red_eye");
		$("#icon_"+id).text("visibility_off");
		$("#cont").show();
		$("#pag").hide();
	}
	else{
		$("#icon_"+id).text("remove_red_eye");
		$("#cont").hide();
		$("#pag").show();
	}
	
}


function getCityName(id_city){
	
	switch(id_city){
	case 1:
		cityString = "Genova";
		break;
	case 2:
		cityString = "Tampere";
		break;
	case 3:
		default:
		cityString = "Eindhoven";
		break;
		
	}
}

function dashboardParams2(idKpi, city, unit){
	var id = idKpi;
	var c = city;
	//var url = new URL(window.location.href);
	//var url_params = new URLSearchParams(url.search);
	//var city2 = url_params.get('city');
	
	console.log("CITY FROM: " + c);
	console.log("ID KPI: " +id);
	
	console.log("CITY ID: "+ c);
	var input = new Object();
	input['idkpi'] = id;
	input['city'] = c;
	
	
	
	
	$.ajax({
		async: false,
        type: "POST",
        url: "GetIdKnowage",
        
        data: input,
        
        success: function(id){ 
        	    console.log("ID ID ID " + id);
        		var idKnowage = id;
        		console.log("idKnowage:" + idKnowage);
        		
        		console.log("CITY " + c);
        		$.ajax({
    				async: false,
    				type: "POST",
    				url: "GetIdCityKnowage",
    				data: input,
    				
    				success: function(city_category){
    					var cityKnowage =  city_category;
    					console.log("city knowage: " + cityKnowage);
    					getCityName(c);
    					console.log("CITTA TROVATA: " + cityString);
    					http://unalab.eng.it/knowage/public/servlet/AdapterHTTP?ACTION_NAME=EXECUTE_DOCUMENT_ACTION&OBJECT_LABEL=ComputedKPI&TOOLBAR_VISIBLE=true&ORGANIZATION=DEFAULT_TENANT&PARAMETERS=kpi=8&kpi_field_visible_description=8&city=251&city_field_visible_description=251&cityname=Eindhoven&cityname_field_visible_description=Eindhoven&unitm=ppm&unitm_field_visible_description=ppm
    					var dashboard = "http://unalab.eng.it:80/knowage/public/servlet/AdapterHTTP?ACTION_NAME=EXECUTE_DOCUMENT_ACTION&OBJECT_LABEL=ComputedKPI&TOOLBAR_VISIBLE=true&ORGANIZATION=DEFAULT_TENANT&SBI_LANGUAGE=en&SBI_COUNTRY=US&NEW_SESSION=true&PARAMETERS=kpi="+idKnowage+"%26city="+cityKnowage + "%26cityname="+cityString+"%26unitm="+unit;
    					console.log("DASHBOARD: " + dashboard);
    					
    					$("#cockpit").attr("src",dashboard);
    					console.log($("#cockpit").attr("src",dashboard));
    					if($("#icon_"+id).text()==="remove_red_eye"){
    						$(".eyes").text("remove_red_eye");
    						$("#icon_"+id).text("visibility_off");
    						$("#cont").show();
    						$("#pag").hide();
    					}
    					else{
    						$("#icon_"+id).text("remove_red_eye");
    						$("#cont").hide();
    						$("#pag").show();
    					}
    					}
    				});

        }
        
	});

	
	
}

function isEmpty(obj) {
	    for(var key in obj) {
	        if(obj.hasOwnProperty(key))
	            return false;
	    }
	    return true;
	}

function getSelectValues(city){

	var c = city;
	console.log(c);
	var input = new Object();
	
	input['city'] = c;
	
	$.ajax({
		async: false,
        type: "POST",
        url: "GetCalculatedIndicators",
        
        data: input,
        dataType: "json",
        success: function(json){ 
        	console.log("JSON INDICATORI: " + json);
        	    
        	    	    
        	    
        	    
        	    		jsonCalculated = json;
        	    		if(jsonCalculated)
        	    			$.each(json, function(i, item) {
        	    				console.log(item.NAME);
        	    				var li = $('<li></li>').appendTo($('#compKPI'));
        	    				var a = $('<a href="#!"></a>').appendTo(li);
        	    				var span = $('<span></span>').text(item.NAME).appendTo(a);
		       
		    });
        	    },
        	    error: function(data){
        	    	 $('#indicatorValue').text("No indicators to be added for this pilot");
        	    }
        
        
	});
        
}


function getSelectValues2(city){

	var c = city;
	console.log(c);
	var input = new Object();
	
	input['city'] = c;
	
	$.ajax({
		async: false,
        type: "POST",
        url: "GetCalculatedIndicators",
        
        data: input,
        dataType: "json",
        success: function(json){ 
        		jsonCalculated = json;
        		$('#compKPI2').empty();
        		$.each(json, function(i, item) {
        		   console.log(item.NAME);
        		   
		        var li = $('<li></li>').appendTo($('#compKPI2'));
		        var a = $('<a href="#!"></a>').appendTo(li);
		        var span = $('<span></span>').text(item.NAME).appendTo(a);
		        //var p = $('<p id="scenarioDescription"></p>').text(item.SCENARIODESCRIPTION).appendTo(a);
		        
		       
		        
		    });

        }
        
	});
        
}

function getValue(id){
	var tableId = id;
	console.log("TABLE ID 2: " + tableId);
	var input = new Object();
	input['id'] = tableId;
	console.log("TABLE ID 3: " + input['id']);
	$.ajax({
		async: false,
        type: "POST",
        url: "GetValue",
        
        data: input,
        
        success: function(indicatorName){ 
        		
            $('#indicatorValue2').text(indicatorName); 
		       
        },
        
	});
 
}





function myParam (kpi,level,category,city,title,tableId){
	
	
	var kpiId = kpi;
	var l= level;
	var category2 = category;
	var ct = city;
	console.log(category2);
	var title = title;
	var id = tableId;
	$(".title").text("Insert a new value for the indicator: " + title + " with id " + id);
	var oModal = $('#modal2');	
	oModal.find('input[name="KPI_id"]').val(kpiId);
	oModal.find('input[name="level"]').val(l);
	oModal.find('input[name="category"]').val(category2);
	oModal.find('input[name="city"]').val(ct);
	oModal.find('input[name="title"]').val(title);
	oModal.find('input[name="tableId"]').val(id);
    oModal.modal();
}
		
function myCityParam (kpi,level,category,city,title,tableId){
	
	
	var kpiId = kpi;
	var l= level;
	var category2 = category;
	var ct = city;
	console.log(category2);
	var title = title;
	var id = tableId;
	$(".title").text("Insert a new value for the indicator: " + title + " with id " + id);
	var oModal = $('#modalcity');	
	oModal.find('input[name="KPI_id"]').val(kpiId);
	oModal.find('input[name="level"]').val(l);
	oModal.find('input[name="category"]').val(category2);
	oModal.find('input[name="city"]').val(ct);
	oModal.find('input[name="title"]').val(title);
	oModal.find('input[name="tableId"]').val(id);
    oModal.modal();
}


$(document).ready(function(){
	$('.dropdown-trigger').dropdown();	
	
	
	

$('#myTable').pageMe({
    pagerSelector:'#myPager',
    activeColor: 'unalabGreen',
//     prevText:'Prev',
//     nextText:'Next',
    showPrevNext:true,
    hidePageNumbers:false,
    perPage:10
  });
$('.tooltipped').tooltip();
$('.modal').modal();
$('select').formSelect();
//$('#tableId').updateTextFields();
$('#computedForm').hide();

$('#manualComputedSwitch').change(function(){
	if($(this).is(":checked")){
		$('#manualForm').hide();
		$('#computedForm').show();
	}
	else if($(this).not(":checked")){
		$('#computedForm').hide();
		$('#manualForm').show();	
	}
});


if ($("#one").val() == "task"){
	$("#two").parent().show();
	$("#third").parent().hide();
}
else if ($("#one").val() == "city" || $("#one").val() == "project") {
	$("#two").parent().hide();
	$("#third").parent().show();
}


$("#one").change(function() {
	
	if($(this).val() == "task") {
		console.log("ciao");
		$("#two").parent().show();
		$("#third").parent().hide();
		
	}
	else if($(this).val() == "city" || $(this).val() == "project") {
		
		$("#two").parent().hide();
		$("#third").parent().show();
		
	}
	
});

if ($("#level").val() == "task"){
	$("#cat1").parent().show();
	$("#cat2").parent().hide();
}
else if ($("#level").val() == "city" || $("#level").val() == "project") {
	$("#cat1").parent().hide();
	$("#cat2").parent().show();
}


$("#level").change(function() {
	
	if($(this).val() == "task") {
		console.log("ciao");
		$("#cat1").parent().show();
		$("#cat2").parent().hide();
		
	}
	else if($(this).val() == "city" || $(this).val() == "project") {
		
		$("#cat1").parent().hide();
		$("#cat2").parent().show();
		
	}
	
});


if ($("#computedLevel").val() == "task"){
	$("#computedCat1").parent().show();
	$("#computedCat2").parent().hide();
}
else if ($("#computedLevel").val() == "city" || $("#computedLevel").val() == "project") {
	$("#computedCat1").parent().hide();
	$("#computedCat2").parent().show();
}


$("#computedLevel").change(function() {
	
	if($(this).val() == "task") {
		console.log("ciao");
		$("#computedCat1").parent().show();
		$("#computedCat2").parent().hide();
		
	}
	else if($(this).val() == "city" || $(this).val() == "project") {
		
		$("#computedCat1").parent().hide();
		$("#computedCat2").parent().show();
		
	}
	
});

var url = new URL(window.location.href);
console.log("URL: " + url);

var url_params = new URLSearchParams(url.search);
city = url_params.get('city');
console.log("CITY: "+ city);
if(city==3){ //eindhoven
	$('#cityDashboard').attr('src', eindhoven_dashboard);
}
else if(city == 1){
	$('#cityDashboard').attr('src', genova_dashboard);
}
else if(city==2){
	$('#cityDashboard').attr('src', tampere_dashboard);
}
getSelectValues(city);
getSelectValues2(city);

$('.dropdown-button').dropdown(); 

$("#compKPI li a").click(function(el){
	
	var $this = $(this);
	
	if($this.find("span").length > 0){
		$val = $this.find("span").text();
	}else{
		$val = $this.text();
	}
	console.log("SELECTED: "+ $val);
	$('#indicatorValue').text($val);
	
	$.each(jsonCalculated, function(i, item){
		if($('#indicatorValue').text()===item.NAME){
			console.log("INDICATOR VALUE: " + $('#indicatorValue').text());
			console.log("ITEM.NAME: " + item.NAME);
			console.log("ITEM ID: " + item.ID);
			idKnowage = item.ID;
			value = $('#indicatorValue').text()
			
			console.log("IDKNOWAGE: "+ idKnowage);
			
		}
		
		
		
		
	});
	
	console.log("ID: " + idKnowage);
	$('<input name="id_knowage" type="hidden" value="' + idKnowage + '"></input>').appendTo($('#compForm'));
	$('<input name="name" type="hidden" value="' + value + '"></input>').appendTo($('#compForm'));
	
});





$('#checkCityComputed').change(function(){
	console.log("ooooh");
	if($(this).is(":checked")){
		$("#shown").removeClass("hide");
		
	}
	else if($(this).not(":checked")){
		$("#shown").addClass("hide");
	}
	
	
	
	
})


$("#compKPI2 li a").click(function(el){
	
	var $this = $(this);
	
	if($this.find("span").length > 0){
		$val = $this.find("span").text();
	}else{
		$val = $this.text();
	}
	console.log("SELECTED: "+ $val);
	
	$('#indicatorValue2').text($val);
	
	$.each(jsonCalculated, function(i, item){
		
		
		if($('#indicatorValue2').text()===item.NAME){
			console.log("INDICATOR VALUE: " + $('#indicatorValue2').text());
			console.log("ITEM.NAME: " + item.NAME);
			console.log("ITEM ID: " + item.ID);
			idKnowage2 = item.ID;
			value2 = $('#indicatorValue2').text()
			
			console.log("IDKNOWAGE: "+ idKnowage2);
			
		}
		
	});
	
	console.log("ID: " + idKnowage2);
	
	$('<input type="hidden" name="id_knowage" value="' + idKnowage2 + '"></input>').appendTo($('#shown'));
	
});


$("#search").on("keyup", function() {
var value = $(this).val();
if(value!=""){
	console.log("page hide")
	$('#pag').hide();
}
else{
	console.log("page show")
	$('#pag #myPager').empty();
	$('#pag').show();
}
console.log("VALUE: " + value);
var set =  $(".tableContent table tr");
var length = set.length;

set.each(function(index){
    if (index != 0 && index < length) {

    	$row = $(this);
		
        var id = $row.find("td").text();
       
        if (id.toLowerCase().indexOf(value.toLowerCase()) == -1) {
            $row.hide();
        } 
        else if (value!=""){
        		$row.show();
        }
    } else if (value=="") {
          $('#myTable').pageMe({
  		    pagerSelector:'#myPager',
  		    activeColor: 'unalabGreen',
  		    showPrevNext:true,
  		    hidePageNumbers:false,
  		    perPage: 10
  		  });
    }
    
});
});



$('#submit').attr('disabled', true);
$('input[type="text"], input[type="number"], textarea').on('keyup',function() {
    var id = $("#id").val();
    var name = $("#name").val();
    var desc = $("#description").val();
    var unit = $("#unit").val();
    
    if(id != '' && name != '' && desc != '' && unit != '') {
        $('#submit').attr('disabled', false);
    } else {
        $('#submit').attr('disabled', true);
    }
});




$('#submitAdminComputed').attr('disabled', true);
$('input[type="text"], input[type="number"], textarea').on('keyup',function() {
    var id = $("#idComputed").val();
    var name = $("#nameComputed").val();
    var desc = $("#descriptionComputed").val();
    var unit = $("#unitComputed").val();
    var ind = $('#indicatorValue').val();
    
    if(id != '' && name != '' && desc != '' && unit != '' &&  ind!= 'none') {
        $('#submitComputed').attr('disabled', false);
    } else {
        $('#submitComputed').attr('disabled', true);
    }
});



$('#submitEdit').attr('disabled', true);
$('#submitAdminEdit').attr('disabled', true);
$('input[type="text"], textarea').on('keyup',function() {
	var nameAdmin = $("#nameAdminEdit").val();
	var descAdmin = $("#descAdminEdit").val();
	var unitAdmin = $("#unitAdminEdit").val();
	if(nameAdmin != '' && descAdmin != '' && unitAdmin != '') {
		$('#submitAdminEdit').attr('disabled', false);
	} else {
		$('#submitAdminEdit').attr('disabled', true);
	}
});

$('#checkCityComputed').change(function(){ 
	if($(this).is(":checked")){
		console.log("CECCATO");
		$('#submitEdit').attr('disabled', true);
		var ind = $('#indicatorValue2').val();
		 var name = $("#nameEdit").val();
		 var desc = $("#descEdit").val();
		 var unit = $("#unitEdit").val();
		if(name != '' && desc != '' && unit != '' &&  ind!= 'none') {
			$('#submitEdit').attr('disabled', false);
		} else {
			$('#submitEdit').attr('disabled', true);
	
		}
	}
	else if ($(this).not(":checked")){
		console.log(" NON CECCATO");
		var name = $("#nameEdit").val();
		 var desc = $("#descEdit").val();
		 var unit = $("#unitEdit").val();
		if(name != '' && desc != '' && unit != '') {
			$('#submitEdit').attr('disabled', false);
		} else {
			$('#submitEdit').attr('disabled', true);
		}
	}
});

if($('#shown').hasClass(':not(.hide)')){
	$('input[type="text"], input[type="number"], textarea').on('keyup',function() {
    
    var name = $("#nameEdit").val();
    var desc = $("#descEdit").val();
    var unit = $("#unitEdit").val();
    
    	console.log("NON SONO NASCOSTO");
    		var ind = $('#indicatorValue2').val();
    		if(name != '' && desc != '' && unit != '' &&  ind!= 'none') {
    	        $('#submitEdit').attr('disabled', false);
    	    } else {
    	        $('#submitEdit').attr('disabled', true);
    	    }
    		
 });
	
	
	
}
    
   else if ($('#shown').hasClass('hide')){
    		$('input[type="text"], textarea').on('keyup',function() {
    	    
    	    var name = $("#nameEdit").val();
    	    var desc = $("#descEdit").val();
    	    var unit = $("#unitEdit").val();
    	    
    		console.log("SONO NASCOSTO");
    		
    		if(name != '' && desc != '' && unit != '') {
	        $('#submitEdit').attr('disabled', false);
	    } else {
	        $('#submitEdit').attr('disabled', true);
	    }
    		
    		
    		
    });
    		
    		
    		
    }
    

    



});


function myParamCityInfo (kpi,level,category,city2,name,tableId,desc,unit,isManual){
	
//	var url = new URL(window.location.href);
//	console.log("URL: " + url);
//
//	var url_params = new URLSearchParams(url.search);
	//city = url_params.get('city');
	console.log("CITY: "+ city);
	var kpiId = kpi;
	var l= level;
	var c = category;
	var ct = city;
	console.log(c);
	var name = name;
	var id = tableId;
	var description= desc;
	var unitM = unit;
	var manual="citymanual";
	console.log("MANUAL " + manual);
	if(manual === "citymanual"){
		console.log("kpi manuale");
		$('#checkCityComputed')[0].checked = false;
		$('#shown').addClass("hide");
		
	}
	else {
		console.log("kpi automatico");
		$('#checkCityComputed')[0].checked= true;
		$('#shown').removeClass("hide");
		console.log("TABLE ID : "+ tableId);
		getValue(tableId);
	}
	
	$(".title").text("Edit information for the indicator: " + name + " with id " + id);
	var oModal = $('#modal3');	
	oModal.find('input[name="KPI_id"]').val(kpiId);
	oModal.find('input[name="level"]').val(l);
	oModal.find('input[name="category"]').val(c);
	oModal.find('input[name="city"]').val(ct);
	oModal.find('textarea[name="name"]').val(name);
	oModal.find('input[name="tableId"]').val(id);
	oModal.find('textarea[name="desc"]').val(description);
	oModal.find('input[name="unit"]').val(unitM);
	//oModal.find('label').class('active');
	oModal.modal();
	//M.updateTextFields();
	
}

function myParamCityCalculatedInfo (kpi,level,idKnowage,category,city2,name,tableId,desc,unit,isManual){
	
//	var url = new URL(window.location.href);
//	console.log("URL: " + url);
//
//	var url_params = new URLSearchParams(url.search);
	//city = url_params.get('city');
	console.log("CITY: "+ city);
	
	var kpiId = kpi;
	var l= level;
	var c = category;
	var ct = city;
	console.log(c);
	var name = name;
	var id = tableId;
	var description= desc;
	var unitM = unit;
	var manual="automatic";
	console.log("MANUAL " + manual);
	if(manual === "citymanual"){
		console.log("kpi manuale");
		$('#checkCityComputed')[0].checked = false;
		$('#shown').addClass("hide");
		
	}
	else {
		console.log("kpi automatico");
		$('#checkCityComputed')[0].checked= true;
		$('#shown').removeClass("hide");
		console.log("TABLE ID : "+ tableId);
		getValue(tableId);
	}
	
	$(".title").text("Edit information for the indicator: " + name + " with id " + id);
	var oModal = $('#modal3');	
	oModal.find('input[name="KPI_id"]').val(kpiId);
	oModal.find('input[name="level"]').val(l);
	oModal.find('input[name="category"]').val(c);
	oModal.find('input[name="city"]').val(ct);
	oModal.find('textarea[name="name"]').val(name);
	oModal.find('input[name="tableId"]').val(id);
	oModal.find('textarea[name="desc"]').val(description);
	oModal.find('input[name="unit"]').val(unitM);
	oModal.find('input[name="id_knowage"]').val(idKnowage);
	//oModal.find('label').class('active');
	oModal.modal();
	//M.updateTextFields();
	
}

function myParamAdminInfo(kpi,level,category,city2,name,tableId,desc,unit,isManual){
	
//	var url = new URL(window.location.href);
//	console.log("URL: " + url);
//
//	var url_params = new URLSearchParams(url.search);
	//city = url_params.get('city');
	console.log("CITY: "+ 3);
	var kpiId = kpi;
	var l= level;
	var c = category;
	var ct = 3;
	console.log(c);
	var name = name;
	var id = tableId;
	var description= desc;
	var unitM = unit;
	var manual=isManual;
	console.log("MANUAL " + manual);
//	if(manual === "manual"){
//		console.log("kpi manuale");
//		$('#checkComputed')[0].checked = false;
//		$('#shown').addClass("hide");
//		
//	}
//	else {
//		console.log("kpi automatico");
//		$('#checkComputed')[0].checked= true;
//		$('#shown').removeClass("hide");
//		console.log("TABLE ID : "+ tableId);
//		getValue(tableId);
//	}
	
	$(".title").text("Edit information for the indicator: " + name + " with id " + id);
	var oModal = $('#modal3');	
	oModal.find('input[name="KPI_id"]').val(kpiId);
	oModal.find('input[name="level"]').val(l);
	oModal.find('input[name="category"]').val(c);
	oModal.find('input[name="city"]').val(ct);
	oModal.find('textarea[name="name"]').val(name);
	oModal.find('input[name="tableId"]').val(id);
	oModal.find('textarea[name="desc"]').val(description);
	oModal.find('input[name="unit"]').val(unitM);
	//oModal.find('label').class('active');
	oModal.modal();
	//M.updateTextFields();
	
}

